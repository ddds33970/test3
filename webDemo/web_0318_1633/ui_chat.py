import streamlit as st
from util.askChat_reRank import ask_reRank
from util.askChat_no_reRank import ask_no_reRank
from util.connector.docker_connector import send_chatHistory_to_docker
from util.memory import memory
# ส่วน chat
def st_chat(username, userId, companyId, role):
    
    st.title("Echo Bot")
    # ปุ่มเปิด-ปิด re-rank
    on = st.toggle('"re-rank" feature')

    # clear_memory_button = st.button("Clear memory")
    # if clear_memory_button:
    #     memory.clear()
    # else:
    #     pass
    
    # ปุ่ม reset chat history ทั้งบนหน้าเว็บ และใน memory
    reset_button_key = "reset_button"
    reset_button = st.button("Reset Chat",key=reset_button_key)
    if reset_button:
        # ลบข้อมูลทั้งหมดใน memory
        memory.clear()
        # reset chat history ที่แสดงบนหน้าเว็บ
        st.session_state.messages = []

    # Initialize chat history
    if "messages" not in st.session_state:
        st.session_state.messages = []

    # Display chat messages from history on app rerun
    for message in st.session_state.messages:
        with st.chat_message(message["role"]):
            st.markdown(message["content"])

    # React to user input
    if question := st.chat_input("What is up?"):
        # Display user message in chat message container
        st.chat_message("user").markdown(question)
        # Add user message to chat history
        st.session_state.messages.append({"role": "user", "content": question})
        
        # ตรงนี้ถ้าเปิด re-rank ให้เรียกใช้ ask_reRank แต่ถ้าปิด re-rank ให้เรียกใช้ ask_no_reRank
        if on:
            result = ask_reRank(question, username)
        else:
            result = ask_no_reRank(question, username)
        
        # response เอามาจาก result ที่ได้จากการเรียกใช้ ask_reRank หรือ ask_no_reRank
        response = f"Echo: {result['answer'].content}"
        # เก็บข้อมูลลงใน memory
        memory.save_context({"question": question}, {"answer": result['answer'].content}) # save memory
        # ดึงข้อมูลล่าสุดจาก memory
        latestMemory = str(memory.load_memory_variables({})['history'][:])

        # ส่งข้อมูลไปยัง docker ของพี่ STAR
        send_chatHistory_to_docker(userId, companyId, role, question, result['answer'].content, latestMemory)
        
        # Display assistant response in chat message container
        with st.chat_message("assistant"):
            st.markdown(response)
        # Add assistant response to chat history
        st.session_state.messages.append({"role": "assistant", "content": response})
    
    
