import os
import uuid
from langchain_openai import OpenAIEmbeddings
from langchain.text_splitter import RecursiveCharacterTextSplitter
from langchain_community.document_loaders import CSVLoader, PyPDFLoader, TextLoader
from langchain_community.document_loaders import UnstructuredWordDocumentLoader
import datetime
from util.delete_in_local import delete_file_duplicate
from util.connector.pinecone_connector import pinecone_connect
from util.connector.mongodb_connector import mongo_connect_fileOfUser_collection, mongo_connect_latest_upload_collection, mongo_connect_pinecone_collection, mongo_connect_vectorDBOfUser_collection
from util.mongodb.mongo_userdata import mongo_find_one_userdata

def upsert_to_pinecone(embeddings_model, document, username, fileId):
    folder = username
    userId = username
    # ที่มี _ คือเป็นการดึงค่าที่ไม่ได้ใช้ แต่ต้องดึงมาเพื่อให้รันได้
    _, _, role, _, _, _, _ = mongo_find_one_userdata(username)

    # Pinecone connect
    index = pinecone_connect()
    
    # loop ทีละ document แล้ว upsert ข้อมูลไปใน Pinecone ซึ่ง document คือ list ของ document ที่ได้จากการ split แล้ว
    for i in range(len(document)):
        # ดึงค่ามาจาก all_file_loader ทีละตัว
        page_content = document[i].page_content
        fileName = document[i].metadata['source'].split('/')[-1]
        
        # สร้าง item_id เพื่อกำหนด id ของ document ที่จะ upsert ไปใน Pinecone
        item_id = f"{userId}_{fileName}_doc_{i+1}"
        # สร้าง item_data เพื่อกำหนด metadata ของ document ที่จะ upsert ไปใน Pinecone
        item_data = {
            "fileName": fileName,
            "text": page_content
        }
        
        # เรียกใช้ฟังก์ชัน embed_documents ใน ver05/process/all_file_loader.py เพื่อแปลง page_content ให้เป็น vector ทีละตัว
        embeddings_values = embeddings_model.embed_documents(document[i].page_content)
        
        # upsert ข้อมูลไปใน Pinecone
        index.upsert(
            vectors=[
                {
                    "id": item_id,
                    # upsert ข้อมูลไปใน Pinecone ในรูปแบบ vector ทีละตัว โดยเริ่มที่ชุดที่ 0
                    "values": embeddings_values[0][:],
                    "metadata": item_data
                }
            ],
            # ให้ upsert ไปใน namespace ของ user คนนั้น
            namespace=folder
        )
        # อันนี้เป็น collection ของ MongoDB ที่เก็บ log การทำงานของ Pinecone (ของทุกคนรวมกันในนี้)
        mongo_pinecone_collection = mongo_connect_pinecone_collection()
        # กำหนดข้อมูลที่จะ insert ไปใน MongoDB
        log_upsert_data = {
            "timestamp": datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            "user": userId,
            "role": role,
            "action": "add documents",
            "item_id": item_id,
            "folder": folder
        }
        mongo_pinecone_collection.insert_one(log_upsert_data)
        
        mongo_vectorDBOfUser_collection = mongo_connect_vectorDBOfUser_collection(username)
        
        vectorDB = {
            "collectionId": str(uuid.uuid4()),
            "fileId": fileId,
            "embedding": embeddings_values[0][:],
            "document": page_content,
            "metadata": item_data
        }
        
        mongo_vectorDBOfUser_collection.insert_one(vectorDB)

    # clear document = [] เมื่อจบการทำงานของฟังก์ชันนี้
    document.clear()
# ฟังก์ชันนี้ จะทำการลบไฟล์ที่อัปโหลดซ้ำออกจาก local directory และเอาเฉพาะไฟล์ที่ชื่อไม่ซ้ำมา split และ upsert ไปใน Pinecone
def file_loader(username, role, filename, directory):
    # mongo connect "latest_upload" collection ของ user คนนั้น
    mongo_latest_upload_collection = mongo_connect_latest_upload_collection(username)
    mongo_fileOfUser_collection = mongo_connect_fileOfUser_collection(username)
    # อันนี้เป็น model ที่ใช้ในการแปลง text ให้เป็น vector
    embeddings_model = OpenAIEmbeddings()
    # อันนี้เป็นฟังก์ชันที่ใช้ในการ split text ให้เป็นหลายๆ ชุด โดยใช้ RecursiveCharacterTextSplitter
    text_splitter = RecursiveCharacterTextSplitter(chunk_size=1000, chunk_overlap=200)
    # ตรงนี้จะเช็คว่าไฟล์ที่อัปโหลดเป็นไฟล์ประเภทอะไร แล้วทำการ split และ upsert ไปใน Pinecone หลังจากนั้นจะลบไฟล์ที่อัปโหลดออกจาก local directory
    
    fileId = str(uuid.uuid4())
    
    if filename.endswith(".csv"):
        document = []
        type = "csv"
        loader = CSVLoader(os.path.join(directory, filename))
        document.extend(text_splitter.split_documents(loader.load()))
        length = len(document)
        upsert_to_pinecone(embeddings_model, document, username, fileId)
        delete_file_duplicate(filename, directory)
    if filename.endswith(".txt"):
        document = []
        type = "txt"
        loader = TextLoader(os.path.join(directory, filename))
        document.extend(text_splitter.split_documents(loader.load()))
        length = len(document)
        upsert_to_pinecone(embeddings_model, document, username, fileId)
        delete_file_duplicate(filename, directory)
    if filename.endswith(".pdf"):
        document = []
        type = "pdf"
        loader = PyPDFLoader(os.path.join(directory, filename))
        document.extend(text_splitter.split_documents(loader.load()))
        length = len(document)
        upsert_to_pinecone(embeddings_model, document, username, fileId)
        delete_file_duplicate(filename, directory)
    if filename.endswith(".docx"):
        document = []
        type = "docx"
        loader = UnstructuredWordDocumentLoader(os.path.join(directory, filename))
        document.extend(text_splitter.split_documents(loader.load()))
        length = len(document)
        upsert_to_pinecone(embeddings_model, document, username, fileId)
        delete_file_duplicate(filename, directory)
    
    # mongo insert filename
    upsert_filename = {
        "timestamp": datetime.datetime.now(datetime.UTC),
        "filename": filename,
        "user": username
    }
    mongo_latest_upload_collection.insert_one(upsert_filename)
    
    
    
    upload_file = {
        "fileId": fileId,
        "filename": filename,
        "type": type,
        "userUpload": username,
        "status": "uploaded",
        "credentialRole": role,
        "startUpdate": datetime.datetime.now(datetime.UTC),
        "latestUpdate": datetime.datetime.now(datetime.UTC)
    }
    
    mongo_fileOfUser_collection.insert_one(upload_file)
    return length
    