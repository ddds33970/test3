import streamlit as st
from util.askChat_reRank import ask_reRank
from util.connector.docker_connector import send_chatHistory_to_docker
from util.memory import memory
from util.connector.docker_connector import get_queryCredits_queryQuota
from util.connector.docker_connector import send_querycreditUpdate_to_docker

# ส่วน chat
def st_chat(username, userId, companyId, role):
    
    st.title("Echo Bot")
    
    queryCredits, queryQuota = get_queryCredits_queryQuota(userId)
    
    col1, col2 = st.columns(2)
            
    col1.metric(label="Query Credits", value=str(queryCredits))
    col2.metric(label="Query Quota used", value=str(queryQuota))
    
    reRankToggle, paraphaseToggle = st.columns(2)
    # ปุ่มเปิด-ปิด re-rank
    reRankOnOff = reRankToggle.toggle('"re-rank" feature')
    # ปุ่มเปิด-ปิด paraphase
    paraphase = paraphaseToggle.toggle('paraphase')
        
    kFromIUchat = st.number_input("retrive documents", min_value=1, max_value=10, value=5)
    kFromUser = int(kFromIUchat)
    # clear_memory_button = st.button("Clear memory")
    # if clear_memory_button:
    #     memory.clear()
    # else:
    #     pass
    
    # ปุ่ม reset chat history ทั้งบนหน้าเว็บ และใน memory
    reset_button_key = "reset_button"
    reset_button = st.button("Reset Chat", key=reset_button_key)
    if reset_button:
        # ลบข้อมูลทั้งหมดใน memory
        memory.clear()
        # reset chat history ที่แสดงบนหน้าเว็บ
        st.session_state.messages = []
    
    question = st.chat_input("What is up?")
        
    with st.container(height=500):
        # Initialize chat history
        if "messages" not in st.session_state:
            st.session_state.messages = []

        # Display chat messages from history on app rerun
        for message in st.session_state.messages:
            with st.chat_message(message["role"]):
                st.markdown(message["content"])

        # React to user input
        if question :
            # Display user message in chat message container
            st.chat_message("user").markdown(question)
            # Add user message to chat history
            st.session_state.messages.append({"role": "user", "content": question})
            
            
            if queryCredits >= 1:
                queryCredits_update = queryCredits - 1
                queryQuota_update = queryQuota + 1
                status = "Success"
                description = "queryCredits and queryQuota updated"
                send_querycreditUpdate_to_docker(userId, companyId, queryQuota_update, queryCredits_update, status, description)
                result = ask_reRank(question, username, kFromUser, reRankOnOff, paraphase)
                
                # response เอามาจาก result ที่ได้จากการเรียกใช้ ask_reRank หรือ ask_no_reRank
                response = f"Echo: {result['answer'].content}"
                # เก็บข้อมูลลงใน memory
                memory.save_context({"question": question}, {"answer": result['answer'].content}) # save memory
                # ดึงข้อมูลล่าสุดจาก memory
                latestMemory = str(memory.load_memory_variables({})['history'][:])

                # ส่งข้อมูลไปยัง docker ของพี่ STAR
                send_chatHistory_to_docker(userId, companyId, role, question, result['answer'].content, latestMemory)
                
                # Display assistant response in chat message container
                with st.chat_message("assistant"):
                    st.markdown(response)
                # Add assistant response to chat history
                st.session_state.messages.append({"role": "assistant", "content": response})
            
            else:
                status = "Failed"
                description = "queryCredits not enough"
                send_querycreditUpdate_to_docker(userId, companyId, queryQuota, queryCredits, status, description)
                st.warning("You don't have enough queryCredits to chat.")
        
        
