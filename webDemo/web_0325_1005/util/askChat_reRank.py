from langchain_core.runnables import RunnableLambda, RunnablePassthrough
from operator import itemgetter
from langchain_openai import ChatOpenAI
from langchain_core.output_parsers import StrOutputParser
from langchain_core.messages import get_buffer_string
from util.pinecone.doc_search_and_retriever import doc_search_and_retriever
from util.prompt_rephrase import CONDENSE_QUESTION_PROMPT
from util.prompt_retrieved import RETRIEVED_CONDENSE_QUESTION_PROMPT
from util._combine_documents import _combine_and_remove_redundancy_documents
from util.prompt_answer import ANSWER_PROMPT
from util.cohere_reRank import CompressionRetriever
from util.memory import memory
from langchain_google_genai import ChatGoogleGenerativeAI
import os
from util.prompt_question import FINAL_QUESTION_PROMPT

GOOGLE_API_KEY = os.getenv("GEMINI_API_KEY")

llm_gimini = ChatGoogleGenerativeAI(model="gemini-pro", convert_system_message_to_human=True, temperature=0) # google_api_key="AI--A") # bun
# ไม่มี call back for count token เหมือน OpenAI

# ฟังก์ชันที่จะใช้ในส่วนของ "Chat"
llm_for_calling = ChatOpenAI(temperature=0, model="gpt-3.5-turbo-1106")

def AskChatModels_main_chain_reRank(username, kFromUser, ReRankOnOff, paraphase):
    compression_retriever = CompressionRetriever(username, kFromUser)
    retriever_pinecone = doc_search_and_retriever(username, kFromUser)
    # First we add a step to load memory
    # This adds a "memory" key to the input object
    loaded_memory = RunnablePassthrough.assign(
        chat_history=RunnableLambda(memory.load_memory_variables) | itemgetter("history"),
        language= itemgetter("language"), # ดึงจาก invoke
        question_original = itemgetter("question"),
    )
    # Now we calculate the standalone question
    standalone_question = {
        "standalone_question": {
            "question": itemgetter("question_original"),
            "chat_history": lambda x: get_buffer_string(x["chat_history"]), # ใช้ lambda สำหรับจะนำแต่ละค่าไปเข้า fn อื่นๆต่อ
            "language" : itemgetter("language")
        }
        | CONDENSE_QUESTION_PROMPT
        | llm_gimini #llm_for_calling#
        | StrOutputParser(),
        "question_original" : itemgetter("question_original"), # ดึง key "question" จาก chain ก่อนหน้า (chain assign)
        "language" : itemgetter("language"),
        "chat_history" : itemgetter("chat_history"),
    }

    standalone_question_search = {
        "standalone_question_search": {
            "question": itemgetter("question_original"), # ดึง key "question_original" จาก dictionary "standalone_question"
            "chat_history": lambda x: get_buffer_string(x["chat_history"]),
        }
        | RETRIEVED_CONDENSE_QUESTION_PROMPT
        | llm_for_calling# llm_gimini #
        | StrOutputParser(),
        "language" : itemgetter("language"),
        "question_original" : itemgetter("question_original"),
        "standalone_question" : itemgetter("standalone_question"),
    }

    standalone_final = {
        "standalone_final": {
            "question_reqphrase": itemgetter("standalone_question"), # ดึง key "question_original" จาก dictionary "standalone_question"
            "retrieved_keyword": itemgetter("standalone_question_search"),
            "language" : itemgetter("language"),
        }
        | FINAL_QUESTION_PROMPT
        | llm_for_calling # llm_gimini #
        | StrOutputParser(),
        "language" : itemgetter("language"),
        "standalone_question" : itemgetter("standalone_question"),
        "standalone_question_search" : itemgetter("standalone_question_search"),
    }

    # Now we retrieve the documents
    retrieved_one_documents = {
        "question_to_search_doc": itemgetter("standalone_question_search"),
        "docs1" : itemgetter("standalone_question_search") | compression_retriever if ReRankOnOff == True else itemgetter("standalone_question_search") | retriever_pinecone, # | compression_retriever, # คือเอาคำรีสำหรับ search ไป retreival
        "language" : itemgetter("language"),
        "standalone_question" : itemgetter("standalone_question") ,
    }

    retrieved_two_documents = {
        "sq_to_search_doc": itemgetter("standalone_question_search"),
        "fq_to_search_doc": itemgetter("standalone_final"),
        "docs1" : itemgetter("standalone_question_search") | compression_retriever if ReRankOnOff == True else itemgetter("standalone_question_search") | retriever_pinecone, # | compression_retriever, # คือเอาคำรีสำหรับ search ไป retreival
        "docs2" : itemgetter("standalone_final") | compression_retriever if ReRankOnOff == True else itemgetter("standalone_final") | retriever_pinecone,
        "language" : itemgetter("language"),
        "standalone_question" : itemgetter("standalone_question") ,
    }

    final_inputs_one = {
        "context1": lambda x: _combine_and_remove_redundancy_documents(
            *[doc for doc in x["docs1"]],  # Pass all elements from "docs1"
        ),
        "question":  itemgetter("standalone_question"),  #itemgetter("standalone_final"), # ใช้ qusetion ใหม่ + history เก่า + keyword เป็น final prompt
        "language" : itemgetter("language"),
    }

    final_inputs_two = {
        "context1": lambda x: _combine_and_remove_redundancy_documents(
            *[doc for doc in x["docs1"]],  # Pass all elements from "docs1"
            *[doc for doc in x["docs2"]],  # Pass all elements from "docs2"
        ),
        "question":  itemgetter("standalone_question"),  #itemgetter("standalone_final"), # ใช้ qusetion ใหม่ + history เก่า + keyword เป็น final prompt
        "language" : itemgetter("language"),
    }

    answer_one = {
        "answer": final_inputs_one | ANSWER_PROMPT | llm_gimini, #llm_for_calling
        "docs1": itemgetter("docs1"),
    }

    answer_two = {
        "answer": final_inputs_two | ANSWER_PROMPT | llm_gimini, #llm_for_calling
        "docs1": itemgetter("docs1"),
        "docs2": itemgetter("docs2"),
    }
    # And now we put it all together!
    if paraphase == False:
        ## if jsut minimum
        final_chain = loaded_memory | standalone_question | standalone_question_search | retrieved_one_documents | answer_one
    else:
        ## if add this option full
        final_chain = loaded_memory | standalone_question | standalone_question_search | standalone_final | retrieved_two_documents | answer_two

    return final_chain

def ask_reRank(question, username, kFromUser, ReRankOnOff, paraphase, language="thai"):
    
    final_chain = AskChatModels_main_chain_reRank(username, kFromUser, ReRankOnOff, paraphase)
    
    question_dict = {
        "question": question,
        "language": language,
    }
    
    # print("\n\n\n\n\nfinal_chain: ", question_dict)
    result = final_chain.invoke(question_dict)
    # print("\n\n\n\n\nresult: ", result)
    return result