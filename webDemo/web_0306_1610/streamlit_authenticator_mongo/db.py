from util.connector.mongodb_connector import mongo_connect_userdata_collection
# Connect to the MongoDB database

try:
    # Create a new client and connect to the server
    # mongo connect "user_data" collection
    mongo_userdata_collection = mongo_connect_userdata_collection()
except Exception as e:
    print(e)
