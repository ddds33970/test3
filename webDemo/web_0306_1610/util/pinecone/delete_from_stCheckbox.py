from pinecone import Pinecone
import other.settings as ENV
from util.connector.pinecone_connector import pinecone_connect

def pinecone_delete_from_checkbox(name_new, filename):
    # Pinecone connect
    index = pinecone_connect()

    prefix_for_delete = name_new + "_" + filename

    for ids in index.list(prefix=prefix_for_delete, namespace=name_new):
        index.delete(ids,namespace=name_new)