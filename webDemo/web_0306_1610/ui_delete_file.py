import streamlit as st
from util.pinecone.delete_from_pinecone import delete_namespace_pinecone
from util.mongodb.mongo_latest_upload import mongo_query_all_latest_upload

def st_delete_namespace(username_new):
    filenames = mongo_query_all_latest_upload(username_new)
    if filenames:
        st.header("Delete All File on Pinecone")
        st.button('Delete All File on Pinecone', on_click=delete_namespace_pinecone, args=(username_new,))
    else:
        pass