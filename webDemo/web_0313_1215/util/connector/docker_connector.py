import json
import requests

def send_chatHistory_to_docker(userId, companyId, role, question, result, latestMemory):
    data = {
        "userId": userId, # ดึงมาจาก MongoDB req
        "companyId": companyId, # ดึงมาจาก MongoDB
        "role": role,
        "question": question,
        "answer": result,
        "chat_history": latestMemory
    }
    # print(data)
    response = requests.post('https://cqc-bn-api-product-pdk5jzx2pq-uc.a.run.app/credit_query_checker', json=data)
    # เรียกใช้งาน API ด้วยคำสั่ง requests.post
    response

    # ตรวจสอบว่าคำขอสำเร็จหรือไม่
    # if response.status_code == 200:
    #     # ถ้าสำเร็จ พิมพ์ข้อมูลที่ได้รับกลับมา
    #     print(response.json())
    # else:
    #     # ถ้าไม่สำเร็จ พิมพ์ข้อความแจ้งเตือน
    #     print("Failed to send request:", response.status_code)
    
def send_fileDocument_to_docker(userId, companyId, role, documents):
    data = {
        "userId": userId, 
        "companyId": companyId,
        "role": role,
        "documents": documents}
    # print(data)
    response = requests.post('https://cec-bn-api-product-pdk5jzx2pq-uc.a.run.app/credit_embedded_checker', json=data)
    # เรียกใช้งาน API ด้วยคำสั่ง requests.post
    response
    
def get_status_CreditEmbeddedChecker(userId):
    result = requests.get('https://cec-bn-api-product-pdk5jzx2pq-uc.a.run.app/get_status?userId=' + userId)
    result_json = result.json()
    status = result_json['status']
    description = result_json['description']
    return status, description