from util.connector.mongodb_connector import mongo_connect_latest_upload_collection

# ฟังก์ชันนี้ใช้สำหรับ insert ชื่อไฟล์ล่าสุดที่อัพโหลดขึ้น MongoDB
def mongo_insert_latest_upload(username, fileName):
    # mongo connect "latest_upload" collection ของ user คนนั้น
    mongo_latest_upload_collection = mongo_connect_latest_upload_collection(username)

    mongo_latest_upload_collection.insert_one({"fileName": fileName})

# ฟังก์ชันนี้ใช้สำหรับดึงชื่อไฟล์ทีละอันจากไฟล์ที่เคยอัพโหลดขึ้น Pinecone ของคนนั้น ซึ่งเรามีเก็บชื่อไฟล์ไว้ใน MongoDB collection ของ user คนนั้น
def mongo_find_one_latest_upload(username, uploaded_filename):
    # mongo connect "latest_upload" collection ของ user คนนั้น
    mongo_latest_upload_collection = mongo_connect_latest_upload_collection(username)

    document = mongo_latest_upload_collection.find_one({"filename": uploaded_filename})
    # ถ้ามีข้อมูลของ user คนนั้น ให้ดึงข้อมูลออกมาแล้ว return ออกไป
    if document is not None:
        filename_from_mongo = document["filename"]
        return filename_from_mongo
    else:
        filename_from_mongo = None
        return filename_from_mongo

# เรียกชื่อไฟล์ทั้งหมดที่ user เคย upload ขึ้น Pinecone และยังไม่ได้ลบ มาแสดง
def mongo_query_all_latest_upload(username):
    # mongo connect "latest_upload" collection ของ user คนนั้น
    mongo_latest_upload_collection = mongo_connect_latest_upload_collection(username)
    # เก็บข้อมูลที่ได้จากการ query ลงใน list
    filenames_list = [document["filename"] for document in mongo_latest_upload_collection.find({})]
    # แปลง list เป็น set เพื่อลบข้อมูลที่ซ้ำกัน
    filenames = set(filenames_list)
    # เรียงชื่อไฟล์ตามตัวอักษร
    sorted_filenames = sorted(list(filenames))
    return sorted_filenames

# ฟังก์ชันนี้ใช้สำหรับลบชื่อไฟล์ที่เลือกออกจาก MongoDB collection latest_upload (ของ user คนนั้น)
def mongo_delete_selected_latest_upload(username, filename):
    # mongo connect "latest_upload" collection ของ user คนนั้น
    mongo_latest_upload_collection = mongo_connect_latest_upload_collection(username)
    # ไปหา document ที่มีชื่อไฟล์ที่เราเลือก
    document = mongo_latest_upload_collection.find_one({"filename": filename})
    # ลบ document ที่เราหาเจอเมื่อกี้
    mongo_latest_upload_collection.delete_one(document)

# ฟังก์ชันนี้ใช้สำหรับลบชื่อไฟล์ทั้งหมดของ user คนนั้นออกจาก MongoDB collection latest_upload ของ user คนนั้น
def mongo_delete_all_latest_upload(username):
    # mongo connect "latest_upload" collection ของ user คนนั้น
    mongo_latest_upload_collection = mongo_connect_latest_upload_collection(username)
    # ลบ document ทั้งหมดที่อยู่ใน collection นั้น โดยไม่กำหนดเงื่อนไข
    mongo_latest_upload_collection.delete_many({})