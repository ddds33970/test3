import os

def delete_files_in_folder(folder_path):
    try:
        # รับ list ของไฟล์ในโฟลเดอร์
        file_list = os.listdir(folder_path)

        # ลูปเพื่อลบทุกไฟล์ในโฟลเดอร์
        for file_name in file_list:
            file_path = os.path.join(folder_path, file_name)
            os.remove(file_path)
            # print(f'ลบไฟล์ {file_path} แล้ว')

        # print(f'ลบไฟล์ทั้งหมดในโฟลเดอร์ {folder_path} เรียบร้อยแล้ว')
    except Exception as e:
        pass
        # print(f'เกิดข้อผิดพลาด: {e}')

def delete_file_duplicate(uploaded_filename, folder_path):
    try:
        file_path = os.path.join(folder_path, uploaded_filename)
        os.remove(file_path)
    except Exception as e:
        pass
        # print(f'เกิดข้อผิดพลาด: {e}')

