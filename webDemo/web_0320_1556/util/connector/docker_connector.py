import json
import requests


def send_chatHistory_to_docker(userId, companyId, role, question, result, latestMemory):
    data = {
        "userId": userId, # ดึงมาจาก MongoDB req
        "companyId": companyId, # ดึงมาจาก MongoDB
        "role": role,
        "question": question,
        "answer": result,
        "chat_history": latestMemory
    }
    # print(data)
    response = requests.post('https://cqc-bn-api-product-pdk5jzx2pq-uc.a.run.app/credit_query_checker', json=data)
    # เรียกใช้งาน API ด้วยคำสั่ง requests.post
    response

    # ตรวจสอบว่าคำขอสำเร็จหรือไม่
    # if response.status_code == 200:
    #     # ถ้าสำเร็จ พิมพ์ข้อมูลที่ได้รับกลับมา
    #     print(response.json())
    # else:
    #     # ถ้าไม่สำเร็จ พิมพ์ข้อความแจ้งเตือน
    #     print("Failed to send request:", response.status_code)
    
def send_fileDocument_to_docker(userId, companyId, role, documents):
    data = {
        "userId": userId, 
        "companyId": companyId,
        "role": role,
        "documents": documents
    }
    # print(data)
    response = requests.post('https://cec-bn-api-product-pdk5jzx2pq-uc.a.run.app/credit_embedded_checker', json=data)
    # เรียกใช้งาน API ด้วยคำสั่ง requests.post
    response
    
# ตอนนี้ใช้ get uploadCredits กับ get uploadQuota แยกกัน แต่เราสามารถรวมเป็นฟังก์ชันเดียวกันได้ '''เอาไว้ก่อน'''
def get_uploadCredits_uploadQuota(userId):
    result = requests.get('https://cec-bn-api-product-pdk5jzx2pq-uc.a.run.app/get_upload?user_id=' + userId)
    result_json = result.json()
    uploadCredits = result_json['uploadCredits']
    uploadQuota = result_json['uploadQuota']
    return uploadCredits, uploadQuota


def send_uploadcreditUpdate_to_docker(userId, uploadQuota_update, uploadCredits_update, status, description):
    data = {
        "userId": userId, 
        "uploadQuota": uploadQuota_update,
        "uploadCredits": uploadCredits_update,
        "status": status,
        "description": description
    }
    # print(data)
    response = requests.post('https://cec-bn-api-product-pdk5jzx2pq-uc.a.run.app/credit_update_embedded', json=data)
    # เรียกใช้งาน API ด้วยคำสั่ง requests.post
    response
    
def get_queryCredits_queryQuota(userId):
    result = requests.get('https://cqc-bn-api-product-pdk5jzx2pq-uc.a.run.app/get_query?user_id=' + userId)
    result_json = result.json()
    queryCredits = result_json['queryCredits']
    queryQuota = result_json['queryQuota']
    return queryCredits, queryQuota

def send_querycreditUpdate_to_docker(userId, companyId, queryQuota_update, queryCredits_update, status, description):
    data = {
        "userId": userId, 
        "companyId": companyId,
        "queryQuota": queryQuota_update,
        "queryCredits": queryCredits_update,
        "status": status,
        "description": description
    }
    # print(data)
    response = requests.post('https://cqc-bn-api-product-pdk5jzx2pq-uc.a.run.app/credit_update_query', json=data)
    # เรียกใช้งาน API ด้วยคำสั่ง requests.post
    response