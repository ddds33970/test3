from langchain.memory import ConversationBufferWindowMemory

memory = ConversationBufferWindowMemory(
    k=4, return_messages=True, output_key="answer", input_key="question")

# memory = ConversationBufferMemory(memory_key="chat_history", return_messages=True)