import os
import jwt
import bcrypt
import streamlit as st
from datetime import datetime, timedelta
import extra_streamlit_components as stx

from util.pinecone.delete_from_pinecone import delete_namespace_pinecone
from .hasher import Hasher
from .validator import Validator
from .utils import generate_random_pw

from .exceptions import CredentialsError, ForgotError, RegisterError, ResetError, UpdateError
from pymongo.collection import Collection

class Authenticate:
    """
    This class will create login, logout, register user, reset password, forgot password, 
    forgot email, and modify user details widgets.
    """
    def __init__(self, mongoCollection:Collection, cookie_name: str, key: str, cookie_expiry_days: float=1.0, 
        preauthorized: list=None, validator: Validator=None):
        """
        Create a new instance of "Authenticate".

        """

        self.collection = mongoCollection
        self.cookie_name = cookie_name
        self.key = key
        self.cookie_expiry_days = cookie_expiry_days
        self.preauthorized = preauthorized
        self.cookie_manager = stx.CookieManager()
        self.validator = validator if validator is not None else Validator()

        if 'name' not in st.session_state:
            st.session_state['name'] = None
        if 'authentication_status' not in st.session_state:
            st.session_state['authentication_status'] = None
        if 'email' not in st.session_state:
            st.session_state['email'] = None
        if 'logout' not in st.session_state:
            st.session_state['logout'] = None

    def _token_encode(self) -> str:
        """
        Encodes the contents of the reauthentication cookie.

        Returns
        """
        return jwt.encode({'name':st.session_state['name'],
            'email':st.session_state['email'],
            'exp_date':self.exp_date}, self.key, algorithm='HS256')

    def _token_decode(self) -> str:
        """
        Decodes the contents of the reauthentication cookie.

        Returns
        -------
        """
        try:
            return jwt.decode(self.token, self.key, algorithms=['HS256'])
        except:
            return False

    def _set_exp_date(self) -> str:
        """
        Creates the reauthentication cookie's expiry date.

        Returns
        """
        return (datetime.utcnow() + timedelta(days=self.cookie_expiry_days)).timestamp()

    def _check_pw(self,password:str) -> bool:
        """
        Checks the validity of the entered password.

        Returns

        """
        return bcrypt.checkpw(self.password.encode(), password.encode())

    def _check_cookie(self):
        """
        Checks the validity of the reauthentication cookie.
        """
        self.token = self.cookie_manager.get(self.cookie_name)
        if self.token is not None:
            self.token = self._token_decode()
            if self.token is not False:
                if not st.session_state['logout']:
                    if self.token['exp_date'] > datetime.utcnow().timestamp():
                        if 'name' and 'email' in self.token:
                            st.session_state['name'] = self.token['name']
                            st.session_state['email'] = self.token['email']
                            st.session_state['authentication_status'] = True
    

    def _check_credentials(self, inplace: bool=True) -> bool:
        """
        Checks the validity of the entered credentials.

        """

        try:
            user_document = self.collection.find_one({"$or": [{"email": self.username_or_email}, {"username": self.username_or_email}]})
            password = user_document['password']
        except Exception as e:
            return False
        

        if user_document:
            try:
                if self._check_pw(password):
                    if inplace:
                        st.session_state['name'] = user_document['username']
                        st.session_state['email'] = user_document['email']
                        self.exp_date = self._set_exp_date()
                        self.token = self._token_encode()
                        self.cookie_manager.set(self.cookie_name, self.token,
                            expires_at=datetime.now() + timedelta(days=self.cookie_expiry_days))
                        st.session_state['authentication_status'] = True
                    else:
                        return True
                else:
                    if inplace:
                        st.session_state['name'] = user_document['username']
                        st.session_state['authentication_status'] = False
                    else:
                        return False
            except Exception as e:
                print(e)
        else:
            if inplace:
                st.session_state['name'] = user_document['username']
                st.session_state['authentication_status'] = False
            else:
                return False

    def login(self, form_name: str, location: str='main') -> tuple:
        """
        Creates a login widget.
    
        """
        if location not in ['main', 'sidebar']:
            raise ValueError("Location must be one of 'main' or 'sidebar'")
        if not st.session_state['authentication_status']:
            self._check_cookie()
            if not st.session_state['authentication_status']:
                if location == 'main':
                    login_form = st.form('Login')
                elif location == 'sidebar':
                    login_form = st.sidebar.form('Login')
    
                login_form.subheader(form_name)
                self.username_or_email = login_form.text_input('Username or Email').lower()
                st.session_state['username_or_email'] = self.username_or_email
                self.password = login_form.text_input('Password', type='password')
    
                if login_form.form_submit_button('Login'):
                    self._check_credentials()
    
        return st.session_state['name'], st.session_state['authentication_status'], st.session_state['email']

    def logout(self, button_name: str, location: str='main', key: str=None):
        """
        Creates a logout button.

        """
        if location not in ['main', 'sidebar']:
            raise ValueError("Location must be one of 'main' or 'sidebar'")
        if location == 'main':
            if st.button(button_name, key):
                self.cookie_manager.delete(self.cookie_name)
                st.session_state['logout'] = True
                st.session_state['name'] = None
                st.session_state['email'] = None
                st.session_state['authentication_status'] = None
        elif location == 'sidebar':
            if st.sidebar.button(button_name, key):
                self.cookie_manager.delete(self.cookie_name)
                st.session_state['logout'] = True
                st.session_state['name'] = None
                st.session_state['email'] = None
                st.session_state['authentication_status'] = None

    def _update_password(self, email: str, password: str):
        """
        Updates credentials dictionary with user's reset hashed password.

        """
        try:
            query = {"email": email}
            update_data = {"$set": {"password": Hasher([password]).generate()[0] }}
            self.collection.find_one_and_update(query, update_data)
        except Exception as e:
            st.error(e)

    def reset_password(self, email: str, form_name: str, location: str='main') -> bool:
        """
        Creates a password reset widget.

        """
        if location not in ['main', 'sidebar']:
            raise ValueError("Location must be one of 'main' or 'sidebar'")
        if location == 'main':
            reset_password_form = st.form('Reset password')
        elif location == 'sidebar':
            reset_password_form = st.sidebar.form('Reset password')
        
        reset_password_form.subheader(form_name)
        self.email = email.lower()
        self.password = reset_password_form.text_input('Current password', type='password')
        new_password = reset_password_form.text_input('New password', type='password')
        new_password_repeat = reset_password_form.text_input('Repeat password', type='password')

        if reset_password_form.form_submit_button('Reset'):
            if self._check_credentials(inplace=False):
                if len(new_password) > 0:
                    if new_password == new_password_repeat:
                        if self.password != new_password: 
                            self._update_password(self.email, new_password)
                            return True
                        else:
                            raise ResetError('New and current passwords are the same')
                    else:
                        raise ResetError('Passwords do not match')
                else:
                    raise ResetError('No new password provided')
            else:
                raise CredentialsError('password')
    
    def _register_credentials(self, email: str, username: str, companyId: str, password: str, role: str):
        """
        Adds to credentials dictionary the new user's information.

        """

        if not self.validator.validate_name(username):
            raise RegisterError('Name is not valid')
        if not self.validator.validate_email(email):
            raise RegisterError('Email is not valid')
        
        try:
            self.collection.insert_one( {   'userId': os.urandom(16).hex(),
                                            'username': username,
                                            'password': Hasher([password]).generate()[0],
                                            'companyId': companyId,
                                            'email': email, 
                                            'startUpdate': datetime.utcnow(),
                                            'lastUpdate': datetime.utcnow(),
                                            'uploadQuota': 0.00,
                                            'queryQuota': 0.00,
                                            'role': role,
                                            'uploadCredits': 0.00,
                                            'queryCredits': 0.00} )
        except Exception as e :
        
            st.error(e)



    def register_user(self, form_name: str, location: str='main') -> bool:
        """
        Creates a register new user widget.

        """

        if location not in ['main', 'sidebar']:
            raise ValueError("Location must be one of 'main' or 'sidebar'")
        if location == 'main':
            register_user_form = st.form('Register user')
        elif location == 'sidebar':
            register_user_form = st.sidebar.form('Register user')

        register_user_form.subheader(form_name)
        new_email = register_user_form.text_input('Email')
        new_username = register_user_form.text_input('Username')
        new_companyId = register_user_form.text_input('Company')
        new_password = register_user_form.text_input('Password', type='password')
        new_password_repeat = register_user_form.text_input('Repeat password', type='password')
        new_role = register_user_form.radio('Role', options=('Admin', 'User'))

        if register_user_form.form_submit_button('Register'):
            user_document = self.collection.find_one({"email": new_email})
            if len(new_email) and len(new_username) and len(new_password) > 0:
                if not user_document:
                    username_check = self.collection.find_one({"username": new_username})
                    if not username_check:
                        if new_password == new_password_repeat:         
                            self._register_credentials(new_email, new_username, new_companyId, new_password, new_role)
                            return True
                        else:
                            raise RegisterError('Passwords do not match')
                    else:
                        raise RegisterError('Username already taken')
                else:
                    raise RegisterError('Email already taken')
            else:
                raise RegisterError('Please enter an email, username, name, and password')
            

    def _set_random_password(self, email: str) -> str:
        """
        Updates credentials dictionary with user's hashed random password.

        """
        self.random_password = generate_random_pw()
        return self.random_password

    def forgot_password(self, form_name: str, location: str='main') -> tuple:
        """
        Creates a forgot password widget.

        """
        if location not in ['main', 'sidebar']:
            raise ValueError("Location must be one of 'main' or 'sidebar'")
        if location == 'main':
            forgot_password_form = st.form('Forgot password')
        elif location == 'sidebar':
            forgot_password_form = st.sidebar.form('Forgot password')

        forgot_password_form.subheader(form_name)
        email = forgot_password_form.text_input('Email').lower()

        if forgot_password_form.form_submit_button('Submit'):
            if len(email) > 0:
                user_document = self.collection.find_one({"email": email})
                if user_document:
                    user_document['password'] = Hasher([self._set_random_password(email)]).generate()[0]
                    return email, user_document['password'], self._set_random_password(email)
                else:
                    return False, None, None
            else:
                raise ForgotError('Email not provided')
        return None, None, None
    
    def _delete_data(self, email: str):
        """
        Updates credentials dictionary with user's hashed random password.

        """
        try:
            query = {"email": email}
            self.collection.delete_one(query)
        except Exception as e:
            st.error(e)

    def delete_user(self, email: str, form_name: str, location: str='main') -> bool:
            """
            Creates a delete user widget.
            """
            if location not in ['main', 'sidebar']:
                raise ValueError("Location must be one of 'main' or 'sidebar'")
            if location == 'main':
                delete_user_form = st.form('Delete User')
            elif location == 'sidebar':
                delete_user_form = st.sidebar.form('Delete User')

            if email is not None:
                self.email = email.lower()

            delete_user_form.subheader(form_name)
            self.email = email.lower()

            if delete_user_form.form_submit_button('Yes!'):
                try:

                    document = self.collection.find_one({"email": email})
                    if document:
                        self._delete_data(self.email)
                        delete_namespace_pinecone(st.session_state['name'])
                        st.success("User has been deleted.")
                        self.cookie_manager.delete(self.cookie_name)
                        st.session_state['logout'] = True
                        st.session_state['name'] = None
                        st.session_state['email'] = None
                        st.session_state['authentication_status'] = None
                        return True
                    else:
                        print(f"User with email {self.email} not found in the database.")
                        st.error("User not found.")
                        return False
                except Exception as e:
                    print(f"Error deleting user: {e}")
                    st.error("An error occurred while deleting the user.")
                    return False

                

    def _get_username(self, email: str) -> str:
        """
        Retrieves username based on a provided entry.

        """

            
        user_document = self.collection.find_one({"email": email})
        return user_document['username']
        


    def forgot_username(self, form_name: str, location: str='main') -> tuple:
        """
        Creates a forgot username widget.

        """
        if location not in ['main', 'sidebar']:
            raise ValueError("Location must be one of 'main' or 'sidebar'")
        if location == 'main':
            forgot_username_form = st.form('Forgot username')
        elif location == 'sidebar':
            forgot_username_form = st.sidebar.form('Forgot username')

        forgot_username_form.subheader(form_name)
        email = forgot_username_form.text_input('Email')

        if forgot_username_form.form_submit_button('Submit'):
            if len(email) > 0:
                return self._get_username(email), email
            else:
                raise ForgotError('Email not provided')
        return None, email

    def _update_entry(self, username: str, key: str, value: str):
        """
        Updates credentials dictionary with user's updated entry.

        """
        self.collection.update_one({'username': username}, {'$set': {key: value}})

    def update_user_details(self, username: str, form_name: str, location: str='main') -> bool:
        """
        Creates a update user details widget.

        """
        if location not in ['main', 'sidebar']:
            raise ValueError("Location must be one of 'main' or 'sidebar'")
        if location == 'main':
            update_user_details_form = st.form('Update user details')
        elif location == 'sidebar':
            update_user_details_form = st.sidebar.form('Update user details')
        
        update_user_details_form.subheader(form_name)
        self.username = username.lower()
        field = update_user_details_form.selectbox('Choose your info', ['Name', 'Email', 'Company']).lower()
        new_value = update_user_details_form.text_input('New info')

        if update_user_details_form.form_submit_button('Update'):
            if len(new_value) > 0:
                if new_value != self.collection.find_one({'username': self.username})[field]:
                    self._update_entry(self.username, field, new_value)
                    if field == 'name':
                            st.session_state['name'] = new_value
                            self.exp_date = self._set_exp_date()
                            self.token = self._token_encode()
                            self.cookie_manager.set(self.cookie_name, self.token,
                            expires_at=datetime.now() + timedelta(days=self.cookie_expiry_days))
                    return True
                else:
                    raise UpdateError('New and current values are the same')
            if len(new_value) == 0:
                raise UpdateError('New value not provided')