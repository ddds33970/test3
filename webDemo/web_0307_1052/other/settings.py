# settings.py

from os import getenv
from os.path import join, dirname, abspath
from dotenv import load_dotenv

dotenv_path = join(dirname(__file__), '.env')

load_dotenv(dotenv_path)

PORT = int(getenv("PORT"))
MONGODB_URI = str(getenv("MONGODB_URI"))

MONGODB_WEBDEMO_DATABASE_NAME = str(getenv("MONGODB_WEBDEMO_DATABASE_NAME"))
MONGODB_USERDATA_COLLECTION_NAME = str(getenv("MONGODB_USERDATA_COLLECTION_NAME"))
MONGODB_PINECONE_COLLECTION_NAME = str(getenv("MONGODB_PINECONE_COLLECTION_NAME"))
MONGODB_USERCOOKIE_COLLECTION_NAME = str(getenv("MONGODB_USERCOOKIE_COLLECTION_NAME"))

MONGODB_LATESTUPLOADFILE_DATABASE_NAME = str(getenv("MONGODB_LATESTUPLOADFILE_DATABASE_NAME"))
# MONGODB_LATEST_UPLOAD_COLLECTION_NAME = str(getenv("MONGODB_LATEST_UPLOAD_COLLECTION_NAME")) # ไม่ได้ใช้ ย้ายไปแยก DATABASE เพื่อเก็บ latest upload file ของแต่ละ user แยกกัน


PINECONE_API_KEY = str(getenv("PINECONE_API_KEY"))
PINECONE_INDEX_NAME = str(getenv("PINECONE_INDEX_NAME"))
GOOGLE_API_KEY = str(getenv("GEMINI_API_KEY"))