from util.connector.mongodb_connector import mongo_connect_userdata_collection

def mongo_find_one_userdata(username_new):
    # mongo connect "user_data" collection()
    mongo_userdata_collection = mongo_connect_userdata_collection()

    document = mongo_userdata_collection.find_one({"name": username_new})
    
    if document is not None:
        ID = str(document["ID"])
        role = document["role"]
        return ID, role
    else:
        ID = None
        role = None
        return ID, role