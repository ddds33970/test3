
import uuid
import datetime
from util.connector.mongodb_connector import mongo_connect_logslogin_collection_nlp


def mongo_insert_logs_login_success(userId, username, password):
    mongo_logslogin_collection_nlp = mongo_connect_logslogin_collection_nlp()
    mongo_logslogin_collection_nlp.insert_one({"logId": str(uuid.uuid4()),
                                               "userId": userId, 
                                               "status": "Success", 
                                               "username": username, 
                                               "password": password, 
                                               "errorMessages": "-", 
                                               "lastUpdate": datetime.datetime.now(datetime.UTC)})
    
def mongo_insert_logs_login_fail(userId, username, password):
    mongo_logslogin_collection_nlp = mongo_connect_logslogin_collection_nlp()
    mongo_logslogin_collection_nlp.insert_one({"logId": str(uuid.uuid4()),
                                               "userId": userId, 
                                               "status": "Failed", 
                                               "username": username, 
                                               "password": password, 
                                               "errorMessages": "Username/password is incorrect", 
                                               "lastUpdate": datetime.datetime.now(datetime.UTC)})
    
