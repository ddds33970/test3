# python = 3.12
streamlit == 1.32.0
pyyaml == 6.0.1
bcrypt == 4.1.2
extra-streamlit-components == 0.1.70
pymongo == 4.6.2
python-dotenv == 1.0.1
langchain-core == 0.1.30
langchain-openai == 0.1.1
langchain == 0.1.13
langchain-google-genai == 1.0.1
pinecone-client == 3.1.0
pyjwt == 2.8.0
streamlit-modal == 0.1.2
cohere == 4.56
langchain-community == 0.0.30
unidecode == 1.3.8
pymupdf == 1.24.0
unstructured == 0.11.8
python-docx == 1.1.0
# docx2txt == 0.8