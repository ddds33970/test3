import streamlit as st
from util.pinecone.delete_from_pinecone import delete_namespace_pinecone
from util.pinecone.delete_from_stCheckbox import pinecone_delete_from_checkbox
from util.mongodb.mongo_latest_upload import mongo_delete_selected_latest_upload, mongo_query_all_latest_upload

def st_delete_namespace(username):
    # เอาชื่อไฟล์ทั้งหมดของ user คนนั้นที่ได้จาก mongo_query_all_latest_upload และเก็บค่าไว้ที่ filenames
    filenames = mongo_query_all_latest_upload(username)
    if filenames:
        st.header("Delete All Your Files")
        # ลบไฟล์ทั้งหมดของ user คนนั้นออกจาก Pinecone และ ลบชื่อไฟล์ทั้งหมดของ user คนนั้นออกจาก mongodb ซึ่งส่วนที่ลบใน mongodb จะทำในฟังก์ชัน delete_namespace_pinecone แล้ว
        st.button('Delete All Files', on_click=delete_namespace_pinecone, args=(username,))
    else:
        pass
    
def delete_file_from_checkbox(username):
    # เอาชื่อไฟล์ทั้งหมดของ user คนนั้นที่ได้จาก mongo_query_all_latest_upload และเก็บค่าไว้ที่ filenames
    filenames = mongo_query_all_latest_upload(username)
    if filenames:
        with st.form("Files you've uploaded"):
            st.write("### Files you've uploaded")
            # อันนี้เป็นการเลือกไฟล์ที่จะลบ
            selected_files = [st.checkbox(filename) for filename in filenames]
            # ปุ่มลบไฟล์ที่เลือก
            delete_file_button = st.form_submit_button("Delete This File")
            # ถ้ากดปุ่มลบไฟล์ที่เลือก
            if delete_file_button:
                # วนลูปเช็คว่าไฟล์ไหนถูกเลือก แล้วลบไฟล์นั้นออกจาก Pinecone และ MongoDB collection latest_upload (ของ user คนนั้น)
                for filename, selected in zip(filenames, selected_files):
                    # ถ้าไฟล์ถูกเลือก
                    if selected:
                        # เรียกใช้ฟังก์ชัน ลบไฟล์ที่เลือกใน Pinecone และ MongoDB collection latest_upload (ของ user คนนั้น)
                        pinecone_delete_from_checkbox(username, filename)
                        mongo_delete_selected_latest_upload(username, filename)
                    else:
                        pass
            else:
                pass
    else:
        pass