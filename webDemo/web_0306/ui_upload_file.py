import streamlit as st
import os
from util.pinecone.upsert import all_file_loader
from util.pinecone.delete_from_stCheckbox import pinecone_delete_from_checkbox
from util.delete_in_local import delete_files_in_folder, delete_file_duplicate
from util.mongodb.mongo_latest_upload import mongo_query_all_latest_upload, mongo_delete_selected_latest_upload, mongo_find_one_latest_upload


def st_upload_file(name_new):
    # แสดงส่วน "Upload File"
    st.header("Upload File")
    filenames = mongo_query_all_latest_upload()
    if filenames:
        with st.form("Files you've uploaded"):
            # filenames = mongo_query_all_latest_upload()
            selected_files = [st.checkbox(filename) for filename in filenames]
            
            delete_file_button = st.form_submit_button("Delete This File")
            if delete_file_button:
                for filename, selected in zip(filenames, selected_files):
                    if selected:
                        # print("/nfilename : ", filename)
                        # print("selected : ", selected)
                        # ตรงนี้เดี๋ยวเรียกใช้ฟังก์ชัน ลบไฟล์ที่เลือกใน Pinecone และ MongoDB collection latest_upload
                        # delete file from Pinecone and MongoDB
                        pinecone_delete_from_checkbox(name_new, filename)
                        mongo_delete_selected_latest_upload(filename)
                    else:
                        pass
            else:
                pass
    else:
        pass
            
    # แสดงส่วนอัปโหลดไฟล์
    with st.form("Upload File", clear_on_submit=True):
        uploaded_files = st.file_uploader("Please select file", type=['.pdf', '.docx', '.txt', '.csv'], accept_multiple_files=True)
        path = "/Users/j.papontee/Desktop/Botnoi/webDemo/web_0503/file_upload" # ต้องเปลี่ยนเป็น path ที่เราต้องการเก็บไฟล์

        # ตรวจสอบว่ามีการอัปโหลดไฟล์หรือไม่
        if uploaded_files is not None:
            # แสดงชื่อไฟล์
            for uploaded_file in uploaded_files: # ใช้กับ accept_multiple_files=True แต่เราจะให้มัน upload ได้แค่ไฟล์เดียว
                st.write("Filename:", uploaded_file.name)
                # บันทึกไฟล์ลงในโฟลเดอร์ที่เราต้องการ
                with open(os.path.join(path, uploaded_file.name), "wb") as f:
                    f.write(uploaded_file.getbuffer())
        
        # แสดงปุ่ม Upload 
        button_upload = st.form_submit_button("Upload")
        
        # print("button_upload : ", button_upload)
        if button_upload:
            uploaded_filename_list = []
            for filename_local in os.listdir(path):
                uploaded_filename_list.append(filename_local)

            for uploaded_filename in uploaded_filename_list:
                # print("\nuploaded_filename : ", uploaded_filename)
                filename_from_mongo = mongo_find_one_latest_upload(uploaded_filename)
                # print("filename_from_mongo : ", filename_from_mongo)

                if uploaded_filename == filename_from_mongo:
                    st.warning(f"{uploaded_filename} has already been uploaded. \n\n If you want to upload it, please delete the old file first.", icon="⚠️")
                    delete_file_duplicate(uploaded_filename, path)
                    print("\nuploaded_filename : ", uploaded_filename)
                    print("file duplicate : ", uploaded_filename == filename_from_mongo)
                    
                else:
                    # streamlit รันจากบนลงล่างตลอด ต้องมีตัวไว้ดัก เช่น จะทำ...​เมื่อกดปุ่มบลาๆ
                    all_file_loader(name_new, path) # delete_file_duplicate(path) อยู่ในนี้ด้วย
                    print("\nuploaded_filename_list : ", uploaded_filename_list)
                    uploaded_filename_list.remove(uploaded_filename)
                    print("uploaded_filename_list : ", uploaded_filename_list)
            # print("\n uploaded_files : ", uploaded_files)
            # print("\n uploaded_filename_list : ", uploaded_filename_list)
        
        # เรียกใช้ฟังก์ชั่นโดยระบุ path ของโฟลเดอร์ที่คุณต้องการลบไฟล์ อีกรอบเพื่อความมั่นใจ
        delete_files_in_folder(path)
        # ลบค่าในตัวแปร uploaded_files
        uploaded_files = None