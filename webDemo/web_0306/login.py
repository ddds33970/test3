import yaml
from yaml.loader import SafeLoader
from streamlit_authenticator_mongo.db import mongo_userdata_collection
from streamlit_authenticator_mongo.authenticate import Authenticate
import streamlit as st
from ui_chat import st_chat
from ui_upload_file import st_upload_file
from ui_delete_file import st_delete_namespace
from util.connector.mongodb_connector import  mongo_connect_usercookie_collection

mongo_usercookie_collection = mongo_connect_usercookie_collection()

config_document = mongo_usercookie_collection.find_one({})
if config_document is None:
    raise Exception("Configuration document not found in MongoDB")

authenticator = Authenticate(
        mongo_userdata_collection,
        config_document['cookie']['name'], 
        config_document['cookie']['key'], 
        config_document['cookie']['expiry_days'],
    )
authenticator.login('Login', 'main')
if st.session_state["authentication_status"]:
    
    st.sidebar.write(f"Logged in as: *{st.session_state['name']}*")
    authenticator.logout('Logout', 'sidebar', key='unique_key')
    
    # แสดงข้อมูลผู้ใช้
    st.header("Information")
    col1, col2, col3 = st.columns(3)
    col1 = st.write("ID:", "This is String") # ต้องเปลี่ยนเป็น ID จริงๆ ที่ดึงมาจาก mongodb
    col2 = st.write(f"Username: *{st.session_state["name"]}*")
    col3 = st.write("Role:", "This is String") # ต้องเปลี่ยนเป็น role จริงๆ ที่ดึงมาจาก mongodb
    st_upload_file(st.session_state["name"])
    st_delete_namespace(st.session_state["name"])
    st_chat(st.session_state["name"])
elif st.session_state["authentication_status"] is False:
    st.error('Username/password is incorrect')
elif st.session_state["authentication_status"] is None:
    st.warning('Please enter your username and password')

try:
    if authenticator.register_user('Register user','sidebar'):
        st.success('User registered successfully')
except Exception as e:
    st.error(e)