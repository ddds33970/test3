import os
from langchain_openai import OpenAIEmbeddings
from langchain.text_splitter import RecursiveCharacterTextSplitter
from langchain_community.document_loaders import CSVLoader, PyPDFLoader, TextLoader
from langchain_community.document_loaders import UnstructuredWordDocumentLoader
import datetime
from util.delete_in_local import delete_file_duplicate
from util.connector.pinecone_connector import pinecone_connect
from util.connector.mongodb_connector import mongo_connect_latest_upload_collection, mongo_connect_pinecone_collection

# import time

def upsert_to_pinecone(embeddings_model, document, username_new):
    folder = username_new
    userId = username_new
    role = "This is String" # ต้องเปลี่ยนเป็น role จริงๆ ที่ดึงมาจาก mongodb

    # Pinecone connect
    index = pinecone_connect()

    for i in range(len(document)):
        # ดึงค่ามาจาก all_file_loader ทีละตัว
        page_content = document[i].page_content
        fileName = document[i].metadata['source'].split('/')[-1]
        
        # สร้าง item_id เพื่อกำหนด id ของ document ที่จะ upsert ไปใน Pinecone
        item_id = f"{userId}_{fileName}_doc_{i+1}"
        # สร้าง item_data เพื่อกำหนด metadata ของ document ที่จะ upsert ไปใน Pinecone
        item_data = {
            "fileName": fileName,
            "text": page_content
        }
        
        # เรียกใช้ฟังก์ชัน embed_documents ใน ver05/process/all_file_loader.py เพื่อแปลง page_content ให้เป็น vector ทีละตัว
        embeddings_values = embeddings_model.embed_documents(document[i].page_content)
        
        # upsert ข้อมูลไปใน Pinecone
        index.upsert(
            vectors=[
                {
                    "id": item_id,
                    # upsert ข้อมูลไปใน Pinecone ในรูปแบบ vector ทีละตัว โดยเริ่มที่ชุดที่ 0
                    "values": embeddings_values[0][:],
                    "metadata": item_data
                }
            ],
            namespace=folder
        )
        mongo_pinecone_collection = mongo_connect_pinecone_collection()
        # กำหนดข้อมูลที่จะ insert ไปใน MongoDB
        log_upsert_data = {
            "timestamp": datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            "user": userId,
            "role": role,
            "action": "add documents",
            "item_id": item_id,
            "folder": folder
        }
        mongo_pinecone_collection.insert_one(log_upsert_data)

    # clear document = []
    document.clear()

def all_file_loader(username_new, directory):
    # mongo connect "latest_upload" collection
    mongo_latest_upload_collection = mongo_connect_latest_upload_collection()

    # start = time.time()
    embeddings_model = OpenAIEmbeddings()
    ''' ลองแก้ตรงนี้ดู ปัญหา alert ไฟล์ซ้ำ '''
    filename_set = set()
    for filename in os.listdir(directory):
        filename_set.add(filename)
        text_splitter = RecursiveCharacterTextSplitter(chunk_size=1000, chunk_overlap=200)
        if filename.endswith(".csv"):
            document = []
            loader = CSVLoader(os.path.join(directory, filename))
            document.extend(text_splitter.split_documents(loader.load()))
            upsert_to_pinecone(embeddings_model, document, username_new)
            delete_file_duplicate(filename, directory)
        if filename.endswith(".txt"):
            document = []
            loader = TextLoader(os.path.join(directory, filename))
            document.extend(text_splitter.split_documents(loader.load()))
            upsert_to_pinecone(embeddings_model, document, username_new)
            delete_file_duplicate(filename, directory)
        if filename.endswith(".pdf"):
            document = []
            loader = PyPDFLoader(os.path.join(directory, filename))
            document.extend(text_splitter.split_documents(loader.load()))
            upsert_to_pinecone(embeddings_model, document, username_new)
            delete_file_duplicate(filename, directory)
        if filename.endswith(".docx"):
            document = []
            loader = UnstructuredWordDocumentLoader(os.path.join(directory, filename))
            document.extend(text_splitter.split_documents(loader.load()))
            upsert_to_pinecone(embeddings_model, document, username_new)
            delete_file_duplicate(filename, directory)
    
    for filename in filename_set:
        upsert_filename = {
            "timestamp": datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            "filename": filename,
            "user": username_new
        }
        mongo_latest_upload_collection.insert_one(upsert_filename)
    
    # filename_list = list(filename_set)
    # end = time.time()
    # print("start at : ", start)
    # print("end at : ", end)
    # print(end - start)