from pymongo.mongo_client import MongoClient
import other.settings as ENV

# mongo connect "latest_upload" collection
def mongo_connect_latest_upload_collection():
    client = MongoClient(ENV.MONGODB_URI)
    db = client[ENV.MONGODB_DATABASE_NAME]
    mongo_latest_upload_collection = db[ENV.MONGODB_LATEST_UPLOAD_COLLECTION_NAME]
    return mongo_latest_upload_collection

# mongo connect "Pinecone" collection
def mongo_connect_pinecone_collection():
    client = MongoClient(ENV.MONGODB_URI)
    db = client[ENV.MONGODB_DATABASE_NAME]
    mongo_pinecone_collection = db[ENV.MONGODB_PINECONE_COLLECTION_NAME]
    return mongo_pinecone_collection

# mongo connect "user_data" collection
def mongo_connect_userdata_collection():
    client = MongoClient(ENV.MONGODB_URI)
    db = client[ENV.MONGODB_DATABASE_NAME]
    mongo_userdata_collection = db[ENV.MONGODB_USERDATA_COLLECTION_NAME]
    return mongo_userdata_collection

# mongo connect "user_cookie" collection
def mongo_connect_usercookie_collection():
    client = MongoClient(ENV.MONGODB_URI)
    db = client[ENV.MONGODB_DATABASE_NAME]
    mongo_usercookie_collection = db[ENV.MONGODB_USERCOOKIE_COLLECTION_NAME]
    return mongo_usercookie_collection