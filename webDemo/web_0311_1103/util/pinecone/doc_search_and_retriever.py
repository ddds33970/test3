from langchain_community.vectorstores import Pinecone
from langchain_openai import OpenAIEmbeddings
import other.settings as ENV

# ฟังก์ชันนี้ใช้สำหรับเรียก retriever ของ Pinecone ขึ้นมาเพื่อใช้ในการค้นหาข้อมูล
def doc_search_and_retriever(username):
    
    folder = username
    # เชื่อมกับ index ของ Pinecone ที่เราสร้างไว้
    index_name = ENV.PINECONE_INDEX_NAME
    # model ที่ใช้ในการ embed ข้อมูลเพื่อเป็นการค้นหา
    embeddings = OpenAIEmbeddings()
    # กำหนดให้หาที่ namespace ของ user คนนั้น โดยใช้ คำถามที่ user พิมพ์เข้ามา ไป embed ก่อน
    docsearch = Pinecone.from_existing_index(index_name, embeddings, namespace=folder)
    # สร้าง retriever ขึ้นมาเพื่อใช้ในการค้นหาข้อมูล
    retriever_pinecone = docsearch.as_retriever(
        # search_type="similarity_score_threshold",
        # search_kwargs={'score_threshold': 0.9}
        
        # เราสามารถกำหนดค่า k ได้ตามต้องการ แต่ในที่นี้เรากำหนดไว้เป็น 5 หมายถึงเราจะเอาข้อมูลที่คล้ายกันที่สุด 5 ตัว
        search_kwargs={"k": 5}
    )
    # ส่ง ข้อมูลที่เราได้จากการค้นหาข้อมูลไปให้ฟังก์ชันเรียกใช้ฟังก์ชันนี้
    return retriever_pinecone

