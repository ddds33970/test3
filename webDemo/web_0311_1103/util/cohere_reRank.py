# https://api.python.langchain.com/en/latest/retrievers/langchain.retrievers.document_compressors.cohere_rerank.CohereRerank.html
from langchain.retrievers.document_compressors import CohereRerank
from langchain.retrievers import ContextualCompressionRetriever
from util.pinecone.doc_search_and_retriever import doc_search_and_retriever

def CompressionRetriever(username):
    retriever_pinecone = doc_search_and_retriever(username)

    compressor = CohereRerank(model="rerank-multilingual-v2.0",
                            top_n=2)

    compression_retriever = ContextualCompressionRetriever(
        base_compressor=compressor, base_retriever=retriever_pinecone
    )
    return compression_retriever

# https://txt.cohere.com/introducing-embed-v3/
