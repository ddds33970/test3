import time
import tempfile
# import docx2txt
import streamlit as st
from pathlib import Path
from unidecode import unidecode
from util.pinecone.upsert import file_loader
from util.delete_in_local import delete_file_duplicate
from ui.ui_delete_file import delete_file_from_checkbox
from util.mongodb.mongo_latest_upload import mongo_find_one_latest_upload
from util.connector.docker_connector import get_uploadCredits_uploadQuota, send_uploadcreditUpdate_to_docker
from util.sendToDocker.fileLoader import AllFileLoaderAndSplit_forSendToCountSplit, fileLoader_forSendToCreditEmbeddedChecker
    
def st_upload_file(username, userId, companyId, role):
    # แสดงส่วน "Upload File"
    st.header("Upload File")
    delete_file_from_checkbox(username)

    # แสดงส่วนอัปโหลดไฟล์
    with st.form("Upload File", clear_on_submit=True):
        # ช่องเลือกไฟล์ที่จะอัปโหลด และเลือกได้หลายไฟล์ โดยมีเงื่อนไขว่าไฟล์ที่อัปโหลดได้ต้องเป็นไฟล์ประเภท .pdf, .docx, .txt, .csv, .md เท่านั้น
        uploaded_files = st.file_uploader("Please select file", type=['.pdf', '.txt', '.md', '.docx', '.csv'], accept_multiple_files=True)
        # ถ้ามีไฟล์ที่อัปโหลด
        if uploaded_files is not None:
            # จะไปเก็บไว้ใน temp directory และเก็บชื่อไฟล์ไว้ใน uploaded_filename_list
            # ซึ่ง temp directory จะถูกลบทิ้งเมื่อออกจาก with โดยอัตโนมัติ
            with tempfile.TemporaryDirectory() as tmpdirname:
                
                temp_dir = Path(tmpdirname)
                uploaded_filename_list = []

                # ตรงนี้เป็นการแสดงชื่อไฟล์ที่อัปโหลดลงในหน้าเว็บ
                for uploaded_file in uploaded_files:
                    # ทำให้ชื่อไฟล์เป็นแบบ ASCII
                    uploaded_filename = unidecode(uploaded_file.name)
                    st.write("Filename:", uploaded_filename)
                    # เขียนไฟล์ที่อัปโหลดลงใน temp directory
                    with open(temp_dir / uploaded_filename, "wb") as f:
                        f.write(uploaded_file.getbuffer())
                    # เก็บชื่อไฟล์ที่อัปโหลดไว้ใน uploaded_filename_list
                    uploaded_filename_list.append(uploaded_filename)
                
                ###########################################################################
                # # ตรงนี้เป็นการแสดงชื่อไฟล์ที่อัปโหลดลงในหน้าเว็บ
                # for uploaded_file in uploaded_files:
                #     # ทำให้ชื่อไฟล์เป็นแบบ ASCII
                #     uploaded_filename = unidecode(uploaded_file.name)
                #     st.write("Filename:", uploaded_filename)

                #     # ถ้าไฟล์เป็น .docx
                #     if uploaded_filename.endswith('.docx'):
                #         # แปลง .docx เป็น .txt
                #         txt_content = docx2txt.process(uploaded_file)
                #         # สร้างชื่อไฟล์ใหม่โดยแทนที่ .docx เป็น .txt
                #         new_filename = uploaded_filename.replace('.docx', '.txt')
                #         # เขียนไฟล์ .txt ลงใน temp directory
                #         with open(temp_dir / new_filename, "w", encoding="utf-8") as f:
                #             f.write(txt_content)
                #         # เก็บชื่อไฟล์ที่อัปโหลดไว้ใน uploaded_filename_list
                #         uploaded_filename_list.append(new_filename)
                #     else:
                #         # ในกรณีอื่น ๆ เขียนไฟล์ที่อัปโหลดลงใน temp directory โดยไม่ต้องแปลง
                #         with open(temp_dir / uploaded_filename, "wb") as f:
                #             f.write(uploaded_file.getbuffer())
                #         # เก็บชื่อไฟล์ที่อัปโหลดไว้ใน uploaded_filename_list
                #         uploaded_filename_list.append(uploaded_filename)
                
                ###########################################################################
                # # ตรงนี้เป็นการแสดงชื่อไฟล์ที่อัปโหลดลงในหน้าเว็บ
                # for uploaded_file in uploaded_files:
                #     uploaded_filename = unidecode(uploaded_file.name)
                #     st.write("Filename:", uploaded_filename)
                #     # เขียนไฟล์ที่อัปโหลดลงใน temp directory
                #     with open(temp_dir / uploaded_filename, "wb") as f:
                #         f.write(uploaded_file.getbuffer())
                #     # เก็บชื่อไฟล์ที่อัปโหลดไว้ใน uploaded_filename_list
                #     uploaded_filename_list.append(uploaded_filename)
                
                ###########################################################################

                # ปุ่มอัปโหลด
                button_upload = st.form_submit_button("Upload")
                
                # ถ้ากดปุ่มอัปโหลด
                if button_upload:
                    
                    start_time = time.time()
                    user_uploadcredits, user_uploadQuota  = get_uploadCredits_uploadQuota(userId)
                    end_time_getCredit = time.time()
                    st.write(f"Time used to get credit: {end_time_getCredit - start_time} seconds")
                    
                    st.write(f"Your remaining credit is {user_uploadcredits}")
                    
                    start_time_countSplit = time.time()
                    credits_use = AllFileLoaderAndSplit_forSendToCountSplit(temp_dir)
                    end_time_countSplit = time.time()
                    st.write(f"Time used to countSplit: {end_time_countSplit - start_time_countSplit} seconds")
                    
                    Credit_remaining = user_uploadcredits - credits_use
                    
                    if Credit_remaining < 0:
                        
                        start_time_fileLoaderSendToCreditEmbeddedChecker = time.time()
                        fileLoader_forSendToCreditEmbeddedChecker(userId, companyId, role, uploaded_filename_list, temp_dir)
                        end_time_fileLoaderSendToCreditEmbeddedChecker = time.time()
                        st.write(f"Time used to fileLoaderSendToCreditEmbeddedChecker: {end_time_fileLoaderSendToCreditEmbeddedChecker - start_time_fileLoaderSendToCreditEmbeddedChecker} seconds")
                        
                        st.write(f"You have used {credits_use} credit(s)")
                        st.error("You don't have enough credit to upload these files.")
                        status = "Failed"
                        description = "uploadCredits not enough"
                        
                        send_uploadcreditUpdate_to_docker(userId, companyId, user_uploadQuota, user_uploadcredits, status, description)
                        
                    else:
                        # ตรงนี้ load ไฟล์ ไปเช็ค credit โดยเชื่อมกับ docker ของพี่ STAR
                        start_time_fileLoaderSendToCreditEmbeddedChecker = time.time()
                        fileLoader_forSendToCreditEmbeddedChecker(userId, companyId, role, uploaded_filename_list, temp_dir)
                        end_time_fileLoaderSendToCreditEmbeddedChecker = time.time()
                        st.write(f"Time used to fileLoaderSendToCreditEmbeddedChecker: {end_time_fileLoaderSendToCreditEmbeddedChecker - start_time_fileLoaderSendToCreditEmbeddedChecker} seconds")
                        
                        credits_use = 0
                        # จะวนลูปเช็คว่าไฟล์ที่อัปโหลดไปแล้วมีอยู่ใน MongoDB collection latest_upload (ของ user คนนั้น) หรือยัง
                        for uploaded_filename in uploaded_filename_list:
                            
                            # โดยเรียกใช้ฟังก์ชัน mongo_find_one_latest_upload ซึ่งจะไปดึงชื่อไฟล์ทั้งหมดที่อัปโหลดแล้วยังไม่ได้ลบของ user คนนั้น
                            start_time_mongoFilename = time.time()
                            filename_from_mongo = mongo_find_one_latest_upload(username, uploaded_filename)
                            end_time_mongoFilename = time.time()
                            st.write(f"Time used to mongoFilename: {end_time_mongoFilename - start_time_mongoFilename} seconds")
                            
                            # ถ้าชื่อไฟล์ที่อัปโหลดไปแล้วมีอยู่ใน MongoDB collection latest_upload (ของ user คนนั้น)
                            if uploaded_filename == filename_from_mongo:
                                # จะขึ้นเตือนว่าไฟล์นี้อัปโหลดไปแล้ว ถ้าต้องการอัปโหลดใหม่ให้ลบไฟล์เก่าออกก่อน
                                st.warning(f"{uploaded_filename} has already been uploaded. \n\n If you want to upload it, please delete the old file first.", icon="⚠️")
                                # ลบไฟล์ที่ชื่อซ้ำกับใน MongoDB collection latest_upload (ของ user คนนั้น) ออกจาก temp directory
                                start_time_deleteFile = time.time()
                                delete_file_duplicate(uploaded_filename, temp_dir)
                                end_time_deleteFile = time.time()
                                st.write(f"Time used to deleteFile: {end_time_deleteFile - start_time_deleteFile} seconds")
                                
                            # แต่ถ้าชื่อไฟล์ที่อัปโหลดไปแล้วไม่มีอยู่ใน MongoDB collection latest_upload (ของ user คนนั้น)
                            else:
                                # ให้เรียกใช้ฟังก์ชัน file_loader ซึ่งจะไปอัปโหลดไฟล์ไป Pinecone และเก็บชื่อไฟล์ลงใน MongoDB collection latest_upload (ของ user คนนั้น) โดยทั้งหมดนี้จะอยู่ในฟังก์ชัน file_loader
                                start_time_fileLoader = time.time()
                                length = file_loader(username, role, uploaded_filename, temp_dir)
                                end_time_fileLoader = time.time()
                                st.write(f"Time used to embeddings and upsert: {end_time_fileLoader - start_time_fileLoader} seconds")
                                
                                credits_use += length
                        uploadCredits_update = user_uploadcredits - credits_use
                        status = "Success"
                        description = "Credit has been used"
                        user_uploadQuota_update = user_uploadQuota + credits_use
                        send_uploadcreditUpdate_to_docker(userId, companyId, user_uploadQuota_update, uploadCredits_update, status, description)
                        st.write(f"You have used {credits_use} credit(s)")
                        st.write(f"You have {uploadCredits_update} credit(s) remaining.")
                    end_time = time.time()
                    st.write(f"All Time used: {end_time - start_time} seconds")
                    
            # reset ค่า uploaded_files เพื่อรอการอัปโหลดไฟล์ใหม่
            uploaded_files.clear()