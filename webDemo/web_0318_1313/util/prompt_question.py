from langchain_core.prompts import ChatPromptTemplate

template_for_final_question = """from "question" and "Search terms"
```
question:
{question_reqphrase}
Search terms:
{retrieved_keyword}

I want to retain the meaning of "question" and add a keyword from "Search terms" in the context of the question to be "New_Question" if it won't have a keyword. I want a "New_Question" to be a clear, specific, and simple question in Thai.
New_Question:
"""

FINAL_QUESTION_PROMPT = ChatPromptTemplate.from_template(template_for_final_question)
# ควรเอาไป search และ ตอบคำถามสุดท้าย