from util.connector.mongodb_connector import mongo_connect_users_collection_nlp

# ฟังก์ชันนี้ใช้สำหรับดึงข้อมูลของ user จาก MongoDB
def mongo_find_one_userdata(username):
    # mongo connect "user_data" collection
    mongo_users_collection_nlp = mongo_connect_users_collection_nlp()
    # ดึงข้อมูลของ user จาก MongoDB โดยใช้ username เป็นเงื่อนไข
    document = mongo_users_collection_nlp.find_one({"username": username})
    # ถ้ามีข้อมูลของ user คนนั้น ให้ดึงข้อมูลออกมาแล้ว return ออกไป
    if document is not None:
        userId = str(document["userId"])
        companyId = document["companyId"]
        role = document["role"]
        password = document["password"]
        credits = document["credits"]
        return userId, companyId, role, password, credits
    else:
        userId = None
        companyId = None
        role = None
        password = None
        credits = None
        return userId, companyId, role, password, credits
    
