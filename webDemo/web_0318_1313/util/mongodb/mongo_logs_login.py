
import uuid
from util.connector.mongodb_connector import mongo_connect_logslogin_collection_nlp


def mongo_insert_logs_login(userId, status, username, password, errorMessages, lastUpdate):
    mongo_logslogin_collection_nlp = mongo_connect_logslogin_collection_nlp()
    mongo_logslogin_collection_nlp.insert_one({"logId": str(uuid.uuid4()),"userId": userId, "status": status, "username": username, "password": password, "errorMessages": errorMessages, "lastUpdate": lastUpdate})