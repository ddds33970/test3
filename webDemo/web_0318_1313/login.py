import streamlit as st
import datetime
from ui_chat import st_chat
from ui_upload_file import delete_file_from_checkbox, st_upload_file
from ui_delete_file import st_delete_namespace
from util.connector.mongodb_connector import  mongo_connect_usercookie_collection_nlp
from util.mongodb.mongo_logs_login import mongo_insert_logs_login
from util.mongodb.mongo_userdata import mongo_find_one_userdata
from streamlit_authenticator_mongo.db import mongo_users_collection_nlp
from streamlit_authenticator_mongo.authenticate import Authenticate
from util.pinecone.delete_from_pinecone import delete_namespace_pinecone

mongo_usercookie_collection_nlp = mongo_connect_usercookie_collection_nlp()



config_document = mongo_usercookie_collection_nlp.find_one({})
if config_document is None:
    raise Exception("Configuration document not found in MongoDB")

login_menu = st.sidebar.selectbox('Selected Page:', ['Login', 'Register', 'Reset Password']) #, 'Forgot Password'

authenticator = Authenticate(
        mongo_users_collection_nlp,
        config_document['cookie']['name'], 
        config_document['cookie']['key'], 
        config_document['cookie']['expiry_days'],
    )

if login_menu == 'Login':
    authenticator.login('Login', 'main')
    # chat_menu = st.sidebar.selectbox('Selected Chat Page:', ['vector search', 'knowledge graphs'])
    if st.session_state["authentication_status"]:
        
        st.sidebar.write(f"Logged in as: *{st.session_state['name']}*")
        authenticator.logout('Logout', 'sidebar', key='unique_key')
        authenticator.delete_user(st.session_state["email"], 'Delete this user', 'sidebar')
        
        userId, companyId, role, password, credits = mongo_find_one_userdata(st.session_state["name"])
        login_status = "Success"
        errorMessages = "-"
        lastUpdate = datetime.datetime.now(datetime.UTC)
        
        mongo_insert_logs_login(userId, login_status, st.session_state['name'], password, errorMessages, lastUpdate)
        
        option_menu = st.sidebar.selectbox('Options:', ['Upload & Chat', 'Monitor'])
        if option_menu == 'Upload & Chat':
            # แสดงข้อมูลผู้ใช้
            st.header("Information")
            col1, col2, col3, col4 = st.columns(4)
            
            col1 = st.write("ID:", userId) 
            col2 = st.write("Company:", companyId) 
            col3 = st.write(f"Username: *{st.session_state["name"]}*")
            col4 = st.write(f"Role:", role) 

            st_upload_file(st.session_state["name"], userId, companyId, role)
            st_delete_namespace(st.session_state["name"])
            st_chat(st.session_state["name"], userId, companyId, role)
            
        elif option_menu == 'Monitor':
            # แสดงข้อมูลผู้ใช้
            st.header("Information")
            col1, col2, col3, col4, col5 = st.columns(5)
            
            col1 = st.write("ID:", userId) 
            col2 = st.write("Company:", companyId) 
            col3 = st.write(f"Username: *{st.session_state["name"]}*")
            col4 = st.write(f"Role:", role) 
            col5 = st.write(f"Credits:", str(credits))
            
            delete_file_from_checkbox(st.session_state["name"])
        
    elif st.session_state["authentication_status"] is False:
        login_status = "Failed"
        errorMessages = "Username/password is incorrect"
        lastUpdate = datetime.datetime.now()
        userId, companyId, role, password, _ = mongo_find_one_userdata(st.session_state["name"])
        mongo_insert_logs_login(userId, login_status, st.session_state["name"], password, errorMessages, lastUpdate)
        st.error('Username/password is incorrect')
    elif st.session_state["authentication_status"] is None:
        st.warning('Please enter your username and password')


elif login_menu == 'Register':
        try:
            if authenticator.register_user('Register user'):
                st.success('User registered successfully')
            else:
                st.warning("Please fill in both the username and password fields.")
        except Exception as e:
            st.error(f'Error: {e}')
            
# elif login_menu == 'Forgot Password':
#     try:
#         username_of_forgotten_password, email_of_forgotten_password, new_random_password = authenticator.forgot_password('Forgot password')
#         if username_of_forgotten_password:
#         # Display the new password securely, ensuring it's done in a secure manner
#             st.success(f'A new password has been generated and sent securely to your email. Your new password is: {new_random_password}')
#         else:
#             st.warning('Please enter your Email')
#     except Exception as e:
#         st.error(e)
        
elif login_menu == 'Reset Password':
        if st.session_state["authentication_status"]:
            try:
                if authenticator.reset_password(st.session_state["email"], 'Reset password'):
                    st.success('Password modified successfully')
            except Exception as e:
                st.error(e)