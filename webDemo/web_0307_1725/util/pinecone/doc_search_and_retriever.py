# ver05/util/doc_search_and_retriever.py
from langchain_community.vectorstores import Pinecone
from langchain_openai import OpenAIEmbeddings
import other.settings as ENV

def doc_search_and_retriever(username):
    
    folder = username
    
    index_name = ENV.PINECONE_INDEX_NAME
    embeddings = OpenAIEmbeddings()

    docsearch = Pinecone.from_existing_index(index_name, embeddings, namespace=folder)

    retriever_pinecone = docsearch.as_retriever(
        # search_type="similarity_score_threshold",
        # search_kwargs={'score_threshold': 0.9}
        
        search_kwargs={"k": 5}
        # namespace=folder
    )
    return retriever_pinecone

