# ver05/process/delete_from_pinecone.py
from pinecone import Pinecone
import other.settings as ENV
from pymongo import MongoClient
import datetime
from util.mongodb.mongo_latest_upload import mongo_delete_all_latest_upload
from util.connector.pinecone_connector import pinecone_connect
from util.connector.mongodb_connector import mongo_connect_pinecone_collection
from util.mongodb.mongo_userdata import mongo_find_one_userdata

def delete_namespace_pinecone(username):
    folder = username
    userId = username
    _, _, role = mongo_find_one_userdata(username)
    # role = "This is String" # ต้องเปลี่ยนเป็น role จริงๆ ที่ดึงมาจาก mongodb
    
    # Pinecone connect
    index = pinecone_connect()
    
    # mongo connect "Pinecone" collection
    mongo_pinecone_collection = mongo_connect_pinecone_collection()
    
    # ลบข้อมูลใน Pinecone ทั้งหมดที่อยู่ใน namespace นั้น
    index.delete(delete_all=True, namespace=folder)
    
    # กำหนดข้อมูลที่จะ insert ไปใน MongoDB
    log_delete_data_namespace = {
        "timestamp": datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
        "user": userId,
        "role": role,
        "action": f"delete namespace: {folder}",
        "namespace": folder
    }
    
    # insert log ขึ้นใน MongoDB
    mongo_pinecone_collection.insert_one(log_delete_data_namespace)
    mongo_delete_all_latest_upload(username)
    
    # print(f"\ndelete folder: {folder} completed.")