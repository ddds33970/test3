from pinecone import Pinecone
import other.settings as ENV
from util.connector.pinecone_connector import pinecone_connect

def pinecone_delete_from_checkbox(username, filename):
    # Pinecone connect
    index = pinecone_connect()

    prefix_for_delete = username + "_" + filename

    for ids in index.list(prefix=prefix_for_delete, namespace=username):
        index.delete(ids,namespace=username)