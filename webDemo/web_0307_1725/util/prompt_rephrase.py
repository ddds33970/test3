# ver05/util/prompt_rephrase.py
from langchain.prompts.prompt import PromptTemplate

template_reqphrase = """Given the following conversation from "Chat History" and a follow-up question from "Follow-Up Input" " if it has "Chat History" you are tasked to rephrase the follow-up question to be a "Standalone question" by "Standalone question" to clear and specific following follow-up question answer in {language} language.
Chat History:
{chat_history}
Follow-Up Input: {question}
Standalone question:"""

CONDENSE_QUESTION_PROMPT = PromptTemplate.from_template(template_reqphrase)
# ควรเป็นสิ่งที่ไป search และตอบคำถามสุดท้าย