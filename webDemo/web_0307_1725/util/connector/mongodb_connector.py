from pymongo.mongo_client import MongoClient
import other.settings as ENV

# mongo connect "latest_upload" collection
def mongo_connect_latest_upload_collection(username):
    client = MongoClient(ENV.MONGODB_URI)
    db = client[ENV.MONGODB_LATESTUPLOADFILE_DATABASE_NAME]
    latestUploadCollection = "latest_upload_" + username
    mongo_latest_upload_collection = db[latestUploadCollection]
    return mongo_latest_upload_collection

# mongo connect "Pinecone" collection
def mongo_connect_pinecone_collection():
    client = MongoClient(ENV.MONGODB_URI)
    db = client[ENV.MONGODB_WEBDEMO_DATABASE_NAME]
    mongo_pinecone_collection = db[ENV.MONGODB_PINECONE_COLLECTION_NAME]
    return mongo_pinecone_collection

# mongo connect "user_data" collection
def mongo_connect_users_collection_nlp():
    client = MongoClient(ENV.MONGODB_URI_NLP)
    db = client[ENV.MONGODB_USER_DATABASE_NAME_NLP]
    mongo_users_collection_nlp = db[ENV.MONGODB_USER_COLLECTION_NAME_NLP]
    return mongo_users_collection_nlp

# mongo connect "user_cookie" collection
def mongo_connect_usercookie_collection_nlp():
    client = MongoClient(ENV.MONGODB_URI_NLP)
    db = client[ENV.MONGODB_USER_DATABASE_NAME_NLP]
    mongo_usercookie_collection_nlp = db[ENV.MONGODB_USERCOOKIE_COLLECTION_NAME_NLP]
    return mongo_usercookie_collection_nlp