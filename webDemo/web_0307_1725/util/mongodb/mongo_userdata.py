from util.connector.mongodb_connector import mongo_connect_users_collection_nlp

def mongo_find_one_userdata(username):
    # mongo connect "user_data" collection()
    mongo_users_collection_nlp = mongo_connect_users_collection_nlp()

    document = mongo_users_collection_nlp.find_one({"username": username})
    
    if document is not None:
        userId = str(document["userId"])
        companyId = document["companyId"]
        role = document["role"]
        return userId, companyId, role
    else:
        userId = None
        companyId = None
        role = None
        return userId, companyId, role