import yaml
from yaml.loader import SafeLoader
from streamlit_authenticator.authenticate import Authenticate
import streamlit as st
from ui_chat import st_chat
from ui_upload_file import st_upload_file
from ui_delete_file import delete_namespace

path_cofig = "/Users/j.papontee/Desktop/Botnoi/webDemo/web_0103/config.yaml"

with open(path_cofig) as file:
    config = yaml.load(file, Loader=SafeLoader)



authenticator = Authenticate(
    config['credentials'],
    config['cookie']['name'],
    config['cookie']['key'],
    config['cookie']['expiry_days'],
    config['preauthorized']
)
authenticator.login()

try:
    email_of_registered_user, username_of_registered_user, name_of_registered_user = authenticator.register_user(preauthorization=False,location="sidebar")
    if email_of_registered_user:
        st.success('User registered successfully')
        st.session_state["authentication_status"] ==True
        with open(path_cofig, 'w') as file:
            yaml.dump(config, file, default_flow_style=False)
except Exception as e:
    st.error(e)


if st.session_state["authentication_status"]:
    
    st.sidebar.write(f"Logged in as: *{st.session_state['name']}*")
    authenticator.logout(location="sidebar")
    
    # แสดงข้อมูลผู้ใช้
    st.header("Information")
    col1, col2, col3 = st.columns(3)
    col1 = st.write("ID:", "65c9bbcc45f343ddfeb5d3ab")
    col2 = st.write(f"Username: *{st.session_state["name"]}*")
    col3 = st.write("Role:", "user")
    st_upload_file(st.session_state["name"])
    delete_namespace(st.session_state["name"])
    st_chat(st.session_state["name"])
elif st.session_state["authentication_status"] is False:
    st.error('Username/password is incorrect')
elif st.session_state["authentication_status"] is None:
    st.warning('Please enter your username and password')