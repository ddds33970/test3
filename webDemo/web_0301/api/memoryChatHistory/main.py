from fastapi import FastAPI, Depends, HTTPException
# from fastapi.responses import JSONResponse
# from fastapi_jwt_auth import AuthJWT
# from fastapi_jwt_auth.exceptions import AuthJWTException
from fastapi.middleware.cors import CORSMiddleware
import uvicorn
# from datetime import timedelta

# Controller
from controller import chat_histories_controller

# Repositories
from repositories.chat_histories_repositories import *

# Model
from model.data_model import *

# Setting
import settings as ENV

app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"]
)

# กำหนดเส้นทางสำหรับ URL
@app.get("/")
async def index():
    return 'memoryChatHistory API Service'

# เพิ่มเส้นทางจาก chat_histories_controller
app.include_router(chat_histories_controller.memory_chathistory_app)

if __name__ == '__main__':
    uvicorn.run("main:app", port=ENV.PORT)