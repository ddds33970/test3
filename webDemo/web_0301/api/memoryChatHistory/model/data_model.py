from pydantic import BaseModel
from typing import Optional

# ใช้ BaseModel ในการสร้างโมเดลของข้อมูลที่จะรับเข้ามา
class MemoryChatHistory(BaseModel):
    userId: str
    query: str
    respond: str
    usage_time: float
    total_token: int
    price: float

