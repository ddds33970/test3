from datetime import datetime

class Model:

    # รูปแบบข้อมูลที่จะเก็บลง Log
    @staticmethod
    def format_UsersData(user_id, query, respond, timestamp):
        users_data = {
            "userId" : user_id,
            "query" : query,
            "respond" : respond,
            "timestamp" : timestamp 
        }
        return users_data
    
    @staticmethod
    def format_ChatLogs(user_id, query, respond, usage_time, total_token, price, timestamp):
        chat_Logs = {
            "userId" : user_id,
            "usage_time" : usage_time,
            "req_text" : query,
            "response_text" : respond,
            "total_token" : total_token,
            "price" : price,
            "timestamp" : timestamp
        }
        return chat_Logs
 