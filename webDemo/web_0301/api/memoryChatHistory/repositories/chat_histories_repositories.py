from dbconnector.mongo_connector import MongoConnector

# เชื่อมต่อกับ MongoDB และเลือก Database ที่เกี่ยวข้อง
user_db, log_db, memory_db = MongoConnector.connect()

# Collection ที่จะใช้งาน
users_collection = user_db["users"]
logs_collection = log_db["logs"]
users_data_collection = memory_db["users_data"]
chat_logs_collection = memory_db["chat_logs"]

class MemoryChatHistoryRepositories:

    @staticmethod
    def find_user(user_id):
        # find ข้อมูล user จาก collection users โดยใช้ userId 
        return users_collection.find_one({'userId': user_id}, {'_id': False})
    
    @staticmethod
    def insert_usersdata(users_data):
        # insert ข้อมูล user ใหม่ลงใน collection users_data
        return users_data_collection.insert_one(users_data)
    
    @staticmethod
    def find_usersdata(user_id):
        # find ข้อมูล user จาก collection users_data โดยใช้ userId 
        return users_data_collection.find_one({'userId': user_id}, {'_id': False})
    
    @staticmethod
    def update_usersdata(users_data, user_id):
        # update ข้อมูล user ใน collection users_data โดยใช้ userId
        return users_data_collection.update_one({'userId': user_id}, {"$set": users_data})
    
    @staticmethod
    def count_chatlogs(user_id):
        # count ข้อมูลประวัติการสนทนาของ user จาก collection chat_logs
        return chat_logs_collection.count_documents({'userId': user_id})

    @staticmethod
    def insert_chatlogs(users_data):
        # insert ข้อมูลประวัติการสนทนาใหม่ลงใน collection chat_logs
        return chat_logs_collection.insert_one(users_data)

    @staticmethod
    def find_chatlogs(user_id):
        # find ข้อมูลประวัติการสนทนาล่าสุดของ user จาก collection chat_logs และเรียงลำดับตามเวลาในลำดับ
        return chat_logs_collection.find({'userId': user_id}).sort('timestamp', 1).limit(1).next()

    @staticmethod
    def delete_chatlogs(oldest_data_id):
        # delete ข้อมูลประวัติการสนทนาที่เก่าที่สุดจาก collection chat_logs โดยใช้ _id
        return chat_logs_collection.delete_one({'_id': oldest_data_id})