import settings as ENV
from utils.singleton_template import Singleton
from pymongo import MongoClient

class MongoConnector(metaclass=Singleton):
    # สร้าง Client เพื่อเชื่อมต่อ MongoDB โดยใช้ข้อมูลจากไฟล์ settings.py
    client = MongoClient(ENV.MONGODB_URI)

    # เลือก Database โดยใช้ข้อมูลจากไฟล์ settings.py
    user_db = client[ENV.USER_DATABASE_NAME]
    log_db = client[ENV.LOG_DATABASE_NAME]
    memory_db = client[ENV.MEMORY_DATABASE_NAME]

     # เลือก Collection
    users_collection = user_db["users"]
    logs_collection = log_db["logs"]
    users_data_collection = memory_db["users_data"]
    chat_logs_collection = memory_db["chat_logs"]

    @classmethod
    def connect(cls) -> MongoClient:
        return cls.user_db, cls.log_db, cls.memory_db

    @classmethod
    def disconnect(cls):
        return cls.client.close()

