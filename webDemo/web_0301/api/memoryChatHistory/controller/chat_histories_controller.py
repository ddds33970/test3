from fastapi import APIRouter, HTTPException
# from fastapi_jwt_auth import AuthJWT
# from fastapi.responses import JSONResponse

# Service
from services.chat_histories_services import * 

# model
from model.data_model import MemoryChatHistory

# สร้าง APIRouter instance สำหรับ memory chathistory controller
memory_chathistory_app = APIRouter()

@memory_chathistory_app.post("/memory_chathistory")
async def memory_chathistory(chat_model: MemoryChatHistory):
    print("/memory_chathistory")
    try:
        # เรียกใช้ mothod process_chatlogs จาก MemoryChatHistoryService เพื่อประมวลผลข้อมูลประวัติการสนทนา
        response = MemoryChatHistoryService.process_chatlogs(chat_model)
        
        # ถ้าการประมวลผลเสร็จสิ้นและมี userId ใน docs_model จะเรียกใช้ method process_usersdata เพื่อประมวลผลข้อมูล user
        if response.get("message") == "Data saved successfully" and chat_model.userId:
            MemoryChatHistoryService.process_usersdata(chat_model)

        return response
    
    except Exception as e:
        # หากเกิดข้อผิดพลาดในการประมวลผลจะพิมพ์ข้อผิดพลาดและเรียกใช้ HTTPException เพื่อส่งข้อผิดพลาด
        print(
            "Controller: memory_chathistory:" + str(e),
            type(e).__name__,          
            __file__,                  
            e.__traceback__.tb_lineno  
        )
        raise HTTPException(
            status_code=401,
            detail="Could not validate credentials",
            headers={"WWW-Authenticate": "Bearer"},
        )