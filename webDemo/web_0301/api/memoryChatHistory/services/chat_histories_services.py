# repositories
from repositories.chat_histories_repositories import *

# model
from model.format_model import *

class MemoryChatHistoryService:

    @staticmethod
    def process_chatlogs(chat_model):
        # ดึง user_id จาก chat_model
        user_id = chat_model.userId

        # ค้นหาข้อมูล user ใน database
        existing_user = MemoryChatHistoryRepositories.find_user(user_id)

        # ถ้าพบข้อมูล user
        if existing_user:
            # จัดรูปแบบข้อมูล chat_logs
            chat_logs = Model.format_ChatLogs(
                user_id,
                chat_model.query,
                chat_model.respond,
                chat_model.usage_time,
                chat_model.total_token,
                chat_model.price,
                datetime.utcnow()
            )

            # count ข้อมูลประวัติการสนทนาที่มีอยู่
            user_id = chat_logs["userId"]
            duplicate_count = MemoryChatHistoryRepositories.count_chatlogs(user_id)

            # ถ้ามีข้อมูลประวัติการสนทนามากกว่า 4 รายการ
            if duplicate_count > 4:
                # ค้นหาข้อมูลประวัติการสนทนาที่เก่าที่สุด
                oldest_data = MemoryChatHistoryRepositories.find_chatlogs(user_id)
                oldest_data_id = oldest_data['_id']

                # delete ข้อมูลประวัติการสนทนาที่เก่าที่สุด
                MemoryChatHistoryRepositories.delete_chatlogs(oldest_data_id)

            # insert ข้อมูลประวัติการสนทนาใหม่ลงใน database
            MemoryChatHistoryRepositories.insert_chatlogs(chat_logs)
            print("Chat logs inserted:", chat_logs)

            return {"message": "Data saved successfully"}
        else:
            return {"message": f"User with ID {user_id} not found"}

    @staticmethod
    def process_usersdata(chat_model):
        # ดึง user_id จาก chat_model
        user_id = chat_model.userId

        # ค้นหาข้อมูล user ใน database
        existing_user = MemoryChatHistoryRepositories.find_user(user_id)

        # ถ้าพบข้อมูล user
        if existing_user:
            # จัด format_data ของ users_data
            users_data = Model.format_UsersData(
                user_id,
                chat_model.query,
                chat_model.respond,
                datetime.utcnow()
            )

            user_id = users_data["userId"]

            # ค้นหาข้อมูล user ใน database users_data
            existing_user = MemoryChatHistoryRepositories.find_usersdata(user_id)

            # ถ้าพบข้อมูล user ใน database users_data
            if existing_user:
                # update ข้อมูล user ใน database users_data
                MemoryChatHistoryRepositories.update_usersdata(users_data, user_id)
            else:
                # เพิ่มข้อมูล user ใหม่ลงใน database users_data
                MemoryChatHistoryRepositories.insert_usersdata(users_data)
                print("User data inserted:", users_data)

            return {"message": "Data saved successfully"}
        else:
            return {"message": f"User with ID {user_id} not found"}