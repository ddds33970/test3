# import json, openai

# with open('config.json', 'r') as f:
#     config = json.load(f)

# openai.api_key = config['openai_api_key']
# openai.api_type = config['openai_api_type']
# openai.api_base = config['openai_api_base']
# openai.api_version = config['openai_api_version']

# persist_directory = 'db'

# # For Azure openai embedding

# embeddings = OpenAIEmbeddings(deployment="Test_embed",
#                 model="text-embedding-ada-002", chunk_size=1,
#                 openai_api_base= openai.api_base,
#                 openai_api_version= openai.api_version,
#                 openai_api_key= openai.api_key,
#                 openai_api_type = openai.api_type)

# vectordb2 = Chroma(persist_directory=persist_directory,
#                   embedding_function=embeddings,
#                    )

# retriever = vectordb2.as_retriever(search_kwargs={"k": 4})

# # Set up Azure openai service LLM

# llm_azure = AzureChatOpenAI(deployment_name="Test_gpt35", model_name="gpt-35-turbo",temperature=0,
#                     max_tokens=1000,
#                     openai_api_base= openai.api_base,
#                     openai_api_version= openai.api_version,
#                     openai_api_key= openai.api_key,
#                     openai_api_type = openai.api_type,
#                     )


# fixed_prompt = """You are a female Bangkok Hospital's chatbot provide information, price and promotional offers to customer.\
# Use only the following pieces of context to answer the users question. If you don't know the answer, just say that you don't know, don't try to make up an answer.\
# ----------------
#   {context}"""

# fixed_question = """customer: {question}
# Please answer in the same language as customer\
# You're feminine, so it's not appropriate to say gender specific words like "ครับ" or "ผม"\
# You won't say a greeting word like "สวัสดีค่ะ".\
# Answer: """