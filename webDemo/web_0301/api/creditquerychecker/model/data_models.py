from pydantic import BaseModel
from typing import Optional

# ใช้ BaseModel ในการสร้างโมเดลของข้อมูลที่จะรับเข้ามา
class QueryObject(BaseModel):
    userId: str
    companyId: str
    role: str
    query: str
