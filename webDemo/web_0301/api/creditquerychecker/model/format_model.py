from datetime import datetime

class Model:

    # รูปแบบข้อมูลที่จะเก็บลง Log
    @staticmethod
    def format_message(user_id, message, channel, count, selectdate):
        data = {
            "userId" : user_id,
            "message" : message,
            "channel" : channel,
            "count" : count,
            "datetime" : selectdate
        }
        return data
