from os import getenv, getcwd
from os.path import join, dirname, abspath
from dotenv import load_dotenv


# กำหนดตำแหน่งของไฟล์ .env
basedir = abspath(dirname(__name__))
dotenv_path = join(dirname(__file__), '.env')


# กำหนดตัวแปร RESOURCE_PATH เริ่มต้นเป็น "data"
RESOURCE_PATH = "data"


# ถ้าตำแหน่งทำงานปัจจุบันเป็น "/main" แสดงว่าอยู่ใน Docker Mode
if getcwd() == "/main":
    print("Docker Mode")
    RESOURCE_PATH = "/data"


# โหลดค่าต่างๆจากไฟล์ .env
load_dotenv(dotenv_path)


# ดึงค่าที่ต้องใช้จากไฟล์ .env และกำหนดให้เป็นตัวแปร
OUT_OF_CREDIT_URL = str(getenv("OUT_OF_CREDIT_URL"))
MONGODB_URI = str(getenv("MONGODB_URI"))
USER_DATABASE_NAME = str(getenv("USER_DATABASE_NAME"))
CREDIT_DATABASE_NAME = str(getenv("CREDIT_DATABASE_NAME"))

