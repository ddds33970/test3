# repositories
from repositories.credit_repositories import CreditRepositories

# utils
from utils.credit_utils import check_credit

# model
from model.format_model import Model

from fastapi import HTTPException
from datetime import datetime
from bson import ObjectId

class CreditCheckerService:
    
    @staticmethod
    def process_credit_check(query_model):
        try:
            # ค้นหาข้อมูลของ user จาก userId
            users_data = CreditRepositories.find_user(query_model.userId)
            print(users_data)
            if not users_data:
                # ถ้าไม่พบ user จะส่ง HTTPException กลับไป
                raise HTTPException(status_code=404, detail="user_Id not found")

            # เรียกใช้ method เพื่อเก็บข้อมูลการตรวจสอบ credit
            formatted_data = CreditCheckerService.store_logscredit(query_model)
            if 'credits' in users_data:
                # ถ้ามี 'credits' ในข้อมูลของ user จะดำเนินการตรวจสอบ credit
                return CreditCheckerService.process_credit(query_model, users_data, formatted_data)
            else:
                # ถ้าไม่มี 'credits' ในข้อมูลของ user จะบันทึกข้อมูลลงใน database และส่งข้อความแจ้ง
                print("No 'credits' key in users_collection")
                no_credits_key_data = {
                    "_id": ObjectId(),
                    "status": "failed",
                    "detail": "No 'credits' key in users_collection",
                    "formatData": formatted_data
                }
                CreditRepositories.insert_log(no_credits_key_data)
                return {"status": "failed", "message": "No 'credits' key in users_collection"}

        except HTTPException as http_error:
            # หากเกิด HTTPException จะส่งข้อผิดพลาดกลับไป
            raise http_error

    @staticmethod
    def store_logscredit(query_model):
        # เรียกใช้ method check_credit (utils) เพื่อตรวจสอบ credit
        count = check_credit(query_model.query)
        # จัด format_data สำหรับเก็บลงใน Database
        formatted_data = Model.format_message(
            user_id=query_model.userId,
            message="Data received successfully",
            channel="-",
            count=count, 
            selectdate=datetime.utcnow()
        )
        print(formatted_data)
        return formatted_data

    @staticmethod
    def process_credit(query_model, users_data, formatted_data):
        # ตรวจสอบ credit
        count = check_credit(query_model.query)
        print(f"Count value: {count}")

        # แสดงข้อมูล credit ก่อนการ update
        print(f"Current credit: {users_data['credits']}")
        print(f"Current queryQuota: {users_data.get('queryQuota', 0)}")

        # คำนวณค่าเงินก่อนโดนหัก credit
        initial_money = users_data['credits'] * 5
        print(f"Initial money before deduction: {initial_money} THB")

        credits = users_data['credits']
        new_credits = float(credits) - float(count)

        if new_credits < 0:
            # หาก credit ไม่พอ จะบันทึกข้อมูลลง Database และส่งข้อความแจ้ง
            print("Out of credit")
            out_of_credit_data = {
                "_id": ObjectId(),
                "status": "failed",
                "detail": "Out of credit",
                "formatData": formatted_data
            }
            CreditRepositories.insert_log(out_of_credit_data)
            return {"status": "failed", "message": "Out of credit"}

        # update credit
        CreditRepositories.update_credits(query_model.userId, new_credits)

        # แสดงข้อมูล credit หลังการ update
        print(f"Updated credit: {new_credits}")

        users_queryquota = users_data.get('queryQuota', 0) 
        new_queryquota = users_queryquota + count
        CreditRepositories.update_queryquota(query_model.userId, new_queryquota)

        # แสดงข้อมูล uploadQuota หลังการ update
        print(f"Updated queryQuota: {new_queryquota}")

        # คำนวณค่าเงินหลังจากที่โดนหัก credit
        remaining_money = new_credits * 5
        print(f"Remaining money after deduction: {remaining_money} THB")

        # บันทึกข้อมูลการ update credit ลงใน Database
        credit_update_data = {
            "_id": ObjectId(),
            "status": "success",
            "detail": "Credits updated successfully",
            "formatData": formatted_data
        }
        CreditRepositories.insert_log(credit_update_data)
        return {"status": "success", "message": "Credits updated successfully"}