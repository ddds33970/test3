# Library
from fastapi import FastAPI, Request, Form, Body, UploadFile, File, Depends
# from fastapi.responses import JSONResponse
# from fastapi_jwt_auth import AuthJWT
# from fastapi_jwt_auth.exceptions import AuthJWTException
from fastapi.middleware.cors import CORSMiddleware
import uvicorn
# from datetime import timedelta

# Controller
from controller import pdf_controller

# Setting
import settings as ENV

import json
import os

app = FastAPI()

# @AuthJWT.load_config
# def get_config():
#     return Settings()

# @app.exception_handler(AuthJWTException)
# def authjwt_exception_handler(request: Request, exc: AuthJWTException):
#     return JSONResponse(
#         status_code=exc.status_code,
#         content={"detail": exc.message}
#     )

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"]
)

@app.get("/")
async def index():
    return 'pdfPreprocess API Service'

app.include_router(pdf_controller.pdf_preprocess_app)

if __name__ == '__main__':
    uvicorn.run("main:app", port=ENV.PORT)


# สรุป main.py connect กับ settings.py และ controller