from os import getcwd, getenv
from os.path import join, dirname, abspath
from dotenv import load_dotenv

# ตั้งค่าตำแหน่งของไดเรกทอรีหลักของโปรแกรม
basedir = abspath(dirname(__name__))

# กำหนดตำแหน่งของไฟล์ .env
dotenv_path = join(dirname(__file__), '.env')

# กำหนดตำแหน่งที่เก็บข้อมูล (RESOURCE_PATH) โดยใช้ "data" เป็นค่าเริ่มต้น
RESOURCE_PATH = "data"

# ถ้าเป็น Docker Mode กำหนด RESOURCE_PATH เป็น "/data"
if getcwd() == "/main":
    print("Docker Mode")
    RESOURCE_PATH = "/data"

# โหลดค่าจากไฟล์ .env
load_dotenv(dotenv_path)

# ดึงค่า PORT และ CREDIT_EMBEDDED_CHECKER_URL จากไฟล์ .env
PORT = int(getenv("PORT"))
CREDIT_EMBEDDED_CHECKER_URL = str(getenv("CREDIT_EMBEDDED_CHECKER_URL"))

# ส่วนนี้ไป connect กับ .env
