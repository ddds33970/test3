## Build

```
docker compose up
```

PdfPreProcess เป็นส่วนหนึ่งของ API ที่ถูกสร้างขึ้นด้วย FastAPI ในภาษา Python เพื่อประมวลผลไฟล์ PDF ที่ถูกอัปโหลด โดยมีโครงสร้าง API ที่ถูกแบ่งเป็นส่วนต่าง ๆ ดังนี้:

pdf_controller.py: เป็นส่วนที่กำหนด API endpoint /upload_pdf ซึ่งรับ HTTP POST request และรับพารามิเตอร์ต่าง ๆ เช่น userId, companyId, role, และ files (รับไฟล์ PDF) ซึ่งทำหน้าที่เรียกใช้ PdfServices เพื่อดำเนินการกับไฟล์ PDF ที่ได้รับ และเรียกใช้ CreditProvider เพื่อตรวจสอบเครดิตจากเอกสาร PDF.

data_models.py (DocumentsObject): กำหนดโครงสร้างข้อมูล (Data Model) ที่ใช้ในการรับข้อมูลจาก HTTP POST request ซึ่งรวมถึง userId, companyId, role, และ documents ที่เกี่ยวข้องกับไฟล์ PDF.

credit_provider.py: มีคลาส CreditProvider ที่มี static method credit_embedded_checker ที่ใช้ส่งข้อมูลไปตรวจสอบเครดิตจากเอกสาร PDF โดยใช้ HTTP POST request ไปยัง URL ที่กำหนด.

pdf_services.py: มีคลาส PdfServices ที่มีเมธอด insert_pdf ที่ใช้สร้าง ThreadPoolExecutor เพื่อทำการบันทึกไฟล์ PDF และดึงข้อมูลจากไฟล์ PDF พร้อมกันด้วยการใช้ ThreadPoolExecutor เช่นกัน ซึ่งสุดท้ายจะรวมข้อมูลจากทุกไฟล์ PDF เข้าด้วยกัน.

pdf_process.py: มีฟังก์ชัน save_pdf ที่ใช้ในการบันทึกไฟล์ PDF และฟังก์ชัน extract_pdf ที่ใช้ในการดึงข้อมูลจากไฟล์ PDF โดยใช้ PyMuPDFLoader และ RecursiveCharacterTextSplitter เพื่อแยกข้อความ.

.env: ไฟล์ที่ใช้ในการกำหนดค่าพารามิเตอร์สำหรับโปรแกรม เช่น PORT และ CREDIT_EMBEDDED_CHECKER_URL.

docker-compose.yml: ไฟล์ Docker Compose ที่ใช้ในการกำหนดการรัน Docker container พร้อมกับการเชื่อมต่อ network และ volume.

Dockerfile: ไฟล์ Docker ที่ใช้ในการสร้าง Docker image โดยใช้ Python 3.8-slim-buster และกำหนด dependencies ที่ต้องใช้.

main.py: เป็นไฟล์ที่ใช้สร้าง FastAPI application และเรียกใช้ controller ที่ถูกกำหนดใน pdf_controller.py.

requirements.txt: ไฟล์ที่ระบุ dependencies ที่ต้องติดตั้งด้วย pip.

settings.py: ไฟล์ที่ใช้ในการตั้งค่าต่าง ๆ ของโปรแกรม เช่น RESOURCE_PATH, PORT, และ CREDIT_EMBEDDED_CHECKER_URL.

โดยทั้งหมดนี้รวมกันเพื่อสร้าง API ที่สามารถรับไฟล์ PDF และประมวลผลเพื่อตรวจสอบเครดิตจากเอกสาร PDF ได้ โดยมีการใช้งาน Parallel Processing เพื่อเพิ่มประสิทธิภาพในการบันทึกไฟล์ PDF และดึงข้อมูลจากไฟล์ PDF พร้อมกัน.
    