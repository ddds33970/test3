import requests
# นำเข้าไลบรารี requests เพื่อให้สามารถทำ HTTP requests ได้
import settings as ENV
# นำเข้าไฟล์ settings และตั้งชื่อให้เป็น ENV เพื่อเข้าถึงตัวแปรและการตั้งค่าที่อยู่ในไฟล์นี้


class CreditProvider:


    # กำหนดเมธอด credit_embedded_checker ในรูปแบบ static method ซึ่งจะสามารถเรียกใช้ได้โดยไม่ต้องสร้าง instance ของคลาส
    @staticmethod
    def credit_embedded_checker(userId, companyId, role, documents):
        # กำหนดข้อมูลที่จะส่งในรูปแบบของ dictionary
        data = {
            "userId": userId, 
            "companyId": companyId,
            "role": role,
            "documents": documents
        }
        
        # ทำ HTTP POST request ไปยัง URL ที่กำหนดในไฟล์ settings โดยใช้ requests.post
        # และให้พารามิเตอร์ json รับค่า dictionary data เพื่อส่งในรูปแบบ JSON
        # พารามิเตอร์ verify=False ใช้ปิดการตรวจสอบ SSL certificate ในกรณีที่ไม่ได้ต้องการการตรวจสอบ SSL
        resp = requests.post(ENV.CREDIT_EMBEDDED_CHECKER_URL, json=data, verify=False)
        
        # พิมพ์สถานะการตอบกลับ (HTTP status code) ที่ได้จาก response
        print("res status : ", resp.status_code)
        
        # ส่งคืนข้อมูลที่ได้จาก response
        return resp.content