from langchain.text_splitter import RecursiveCharacterTextSplitter
# เป็นตัวแยกข้อความที่ใช้ในการแยกข้อความจากเนื้อหา PDF ออกเป็นชุดข้อความ
# ที่มีขนาดหนึ่งหรือมากกว่า และมีการซ้อนทับกันบางส่วนเพื่อให้การตัดแบ่งข้อความเกิดขึ้นได้ถูก

from langchain.document_loaders import PyMuPDFLoader
# เป็นตัวโหลดไฟล์ PDF โดยใช้ PyMuPDF library ที่ให้บริการในการโหลดข้อมูลจากไฟล์ PDF

from fixthaipdf import clean
# เป็นฟังก์ชันที่ใช้ในการทำความสะอาดข้อความที่ได้จาก PDF ภาษาไทย 
# โดยตัดออกตัวอักษรที่ไม่เกี่ยวข้องหรือทำให้ข้อความผิดพลาด

import os
# ใช้ในการทำงานกับระบบปฏิบัติการเพื่อจัดการไฟล์และทำงานกับทางที่อยู่ของไฟล์

import settings as ENV
# Import ไฟล์ settings และตั้งชื่อให้เป็น ENV เพื่อให้เข้าถึงตัวแปรและการตั้งค่าที่อยู่ในไฟล์นี้

def save_pdf(pdf_file, directory: str = ENV.RESOURCE_PATH) -> str:
# ฟังก์ชัน save_pdf ใช้ในการบันทึกไฟล์ PDF ที่ได้รับเข้ามาที่ที่กำหนด (directory) 
# และส่งคืนที่อยู่ของไฟล์ที่ถูกบันทึก
    try:
        filename = pdf_file.filename
        pdf_path = os.path.join(directory, filename)

        # เปิดไฟล์ PDF และเขียนข้อมูลลงในไฟล์
        with open(pdf_path, "wb") as file_object:
            file_object.write(pdf_file.file.read())
        print("pdf_path : ", pdf_path)
        return pdf_path
    
    except Exception as e:
        # จัดการข้อผิดพลาดที่เกิดขึ้นในกรณีที่ไม่สามารถบันทึกไฟล์ PDF ได้
        print(f"An error occurred: {str(e)}")
        return ""

def extract_pdf(pdf_path: str):
# ฟังก์ชัน extract_pdf ทำหน้าที่โหลดข้อมูลจากไฟล์ PDF โดยใช้ PyMuPDFLoader และจากนั้นใช้ 
# RecursiveCharacterTextSplitter เพื่อแยกข้อความ จากนั้นทำการ clean ข้อความที่ได้ด้วย
# fixthaipdf.clean และส่งคืนข้อมูลในรูปแบบที่มีโครงสร้างใหม่ ในกรณีเกิดข้อผิดพลาดในขั้นตอนใด ๆ ก็จะถูกจัดการและส่งคืนข้อผิดพลาด
    try:
        loader = PyMuPDFLoader(pdf_path)
        documents = loader.load()

        # ใช้ RecursiveCharacterTextSplitter เพื่อแยกข้อความจากเนื้อหา PDF
        text_splitter = RecursiveCharacterTextSplitter(chunk_size=1000, chunk_overlap=200)
        documents = text_splitter.split_documents(documents)
        # print("documents : ", documents)

        new_documents = []
        for i in range(len(documents)):
            text = documents[i].page_content  # ดึงข้อความจาก 'Document' object
            cleaned_text = clean(text)  # เรียกใช้ฟังก์ชัน 'clean' กับข้อความที่ดึงมา
            # print(f"{text}\n{cleaned_text}\n----------------\n")
            # documents[i].page_content = cleaned_text  # กำหนดข้อความที่ถูก clean กลับไปยัง 'Document' object

            # สร้างโครงสร้างข้อมูลใหม่
            new_documents.append(
                {"page_content": cleaned_text, "metadata": documents[i].metadata})
        return new_documents
    
    except Exception as e:
        # จัดการข้อผิดพลาดที่เกิดขึ้นในกรณีที่ไม่สามารถดึงข้อมูล PDF ได้
        print(f"An error occurred: {str(e)}")
        return (f"An error occurred: {str(e)}")
    
# เป็น Low Level อยู่สุดท้ายของการ Dev
