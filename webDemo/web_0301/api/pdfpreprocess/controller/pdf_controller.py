
from fastapi import APIRouter, Form, UploadFile, File, HTTPException
# from fastapi_jwt_auth import AuthJWT
# from fastapi.responses import JSONResponse
from typing import List

# Service
from services.pdf_services import PdfServices

# Provider
from providers.credit_provider import CreditProvider

# Model
from model.data_models import *

# สร้าง APIRouter เพื่อจัดการเส้นทางของ API
pdf_preprocess_app = APIRouter()
# สร้าง PdfServices instance เพื่อให้บริการต่าง ๆ ที่เกี่ยวข้องกับ PDF
pdf_service = PdfServices()
# สร้าง CreditProvider instance เพื่อให้บริการตรวจสอบเครดิตจากเอกสาร PDF
credit_provider = CreditProvider()

# กำหนด API endpoint ที่รอรับ HTTP POST request ที่เรียกว่า "/upload_pdf"
@pdf_preprocess_app.post("/upload_pdf")
async def upload_pdf(
    userId: str=Form(...), 
    companyId: str=Form(...), 
    role: str=Form(...), 
    files: List[UploadFile]=File(...)):
    print("/upload_pdf")
    try:
        # เรียกใช้ PdfServices เพื่อดำเนินการทำงานกับไฟล์ PDF ที่ได้รับ
        documents = pdf_service.insert_pdf(files)

        # เรียกใช้ CreditProvider เพื่อตรวจสอบเครดิตจากเอกสาร PDF
        resp = credit_provider.credit_embedded_checker(
            userId=userId,
            companyId=companyId,
            role=role,
            documents=documents
        )
        print("resp : ", resp)
    except Exception as e:
        # จัดการข้อผิดพลาดและสร้าง HTTPException ในกรณีที่เกิดข้อผิดพลาด
        print(
                "Controller: pdf_control:" + str(e),
                type(e).__name__,          # TypeError
                __file__,                  # /tmp/example.py
                e.__traceback__.tb_lineno  # 2
            )
        raise HTTPException(status_code=401, detail="Could not validate credentials", headers={"WWW-Authenticate": "Bearer"},)
