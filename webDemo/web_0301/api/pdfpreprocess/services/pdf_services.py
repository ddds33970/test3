from utils.pdf_process import save_pdf, extract_pdf
# นำเข้าไฟล์ save_pdf และ extract_pdf จากไฟล์ pdf_process ในโฟลเดอร์ utils
import concurrent.futures


class PdfServices:


    # สร้างเมธอด insert_pdf ในรูปแบบคลาสเมธอด (classmethod) เพื่อทำการบันทึกและดึงข้อมูลจากไฟล์ PDF
    @classmethod
    def insert_pdf(cls, pdf_files: list) -> str:
        
        pdf_paths = []  # สร้างลิสต์เพื่อเก็บที่อยู่ของไฟล์ PDF ที่ถูกบันทึก
        with concurrent.futures.ThreadPoolExecutor() as executor:
            try:
                # ใช้ executor.map เพื่อทำการบันทึกไฟล์ PDF พร้อมกัน
                pdf_paths = list(executor.map(save_pdf, pdf_files))
                print("pdf_path : ", pdf_paths)
            except Exception as e:
                # จัดการข้อผิดพลาดที่เกิดขึ้นในกรณีที่ไม่สามารถบันทึกไฟล์ PDF ได้
                print("save_pdf failed : ", e)
                return {
                    "message": "Fail to save pdf",
                    "status": 403}
        
            try:
                # ใช้ executor.map เพื่อทำการดึงข้อมูลจากไฟล์ PDF พร้อมกัน
                documents = list(executor.map(PdfServices.extract_pdf, pdf_paths))
                # รวมข้อมูลจากทุกไฟล์ PDF เข้าด้วยกัน
                documents = sum(documents, [])
                print("number of documents : ", len(documents))
            except Exception as e:
                # จัดการข้อผิดพลาดที่เกิดขึ้นในกรณีที่ไม่สามารถดึงข้อมูล PDF ได้
                print("extract_pdf failed : ", e)
                return {
                    "message": "Fail to extract_pdf",
                    "status": 403}
            
            print("sameple data : ", documents[0])
            return documents
    
    # สร้าง static method extract_pdf เพื่อดึงข้อมูลจากไฟล์ PDF ด้วย extract_pdf จากไฟล์ pdf_process
    @staticmethod
    def extract_pdf(pdf_path: str) -> list:
        print("PdfServices : extract_pdf")
        return extract_pdf(pdf_path=pdf_path)
