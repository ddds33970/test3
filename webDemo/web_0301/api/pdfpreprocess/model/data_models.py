from pydantic import BaseModel
from typing import Optional

# กำหนดคลาส DocumentsObject ที่สืบทอดจาก BaseModel ของ Pydantic
class DocumentsObject(BaseModel):
    userId: str
    companyId: str
    role: str
    documents: list = []

    # สร้างเมธอด @classmethod to_json() เพื่อแปลงข้อมูลในรูปแบบของ JSON
    @classmethod
    def to_json(cls):
        # สร้างและส่งคืน dictionary ที่ประกอบไปด้วยค่าของฟิลด์ทั้งหมดใน DocumentsObject
        return {
            "userId": cls.userId,
            "companyId": cls.companyId,
            "role": cls.role,
            "documents": cls.documents
        }