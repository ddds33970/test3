from pydantic import BaseModel
from typing import Optional

# ใช้ BaseModel ในการสร้างโมเดลของข้อมูลที่จะรับเข้ามา
class DocumentsObject(BaseModel):
    userId: str
    companyId: str
    role: str
    documents: list
