from fastapi import APIRouter, HTTPException
# from fastapi_jwt_auth import AuthJWT
# from fastapi.responses import JSONResponse

# Service
from services.creditchecker_service import * 

# providers
from providers.outofcredit_provider import OutofcreditProvider

# model
from model.data_models import DocumentsObject

# สร้าง APIRouter instance สำหรับ credit embedded controller
credit_embedded_app = APIRouter()

@credit_embedded_app.post("/credit_embedded_checker")
async def credit_embedded_checker(docs_model: DocumentsObject):
    print("/credit_embedded_checker")
    try:
        # เรียกใช้ CreditCheckerService เพื่อตรวจสอบ credit
        response = CreditCheckerService.process_credit_check(docs_model)

        # ตรวจสอบว่าการตรวจสอบ credit ล้มเหลวหรือไม่
        if response.get("status") == "failed":
            # เรียกใช้ OutofcreditProvider เพื่อจัดการเหตุการณ์เมื่อ credit หมด
            OutofcreditProvider.handle_out_of_credit()

        return response
    
    # จัดการข้อผิดพลาดของ HTTP
    except HTTPException as http_error:
        raise http_error
    
    # จัดการข้อผิดพลาดอื่น ๆ
    except Exception as e:
        print(
            "Controller: credit_embedded_control:" + str(e),
            type(e).__name__,          # ประเภทของข้อผิดพลาด
            __file__,                  # ไฟล์ที่มีการเกิดข้อผิดพลาด
            e.__traceback__.tb_lineno  # เลขบรรทัดที่เกิดข้อผิดพลาด
        )
        raise HTTPException(
            status_code=401,
            detail="Could not validate credentials",
            headers={"WWW-Authenticate": "Bearer"},
        )
