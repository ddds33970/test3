from fastapi.responses import RedirectResponse
import requests
import settings as ENV

class OutofcreditProvider:

    @staticmethod
    def handle_out_of_credit():
        # ส่ง request.get ไปยัง URL ที่กำหนดในไฟล์ settings.py เมื่อ OutOfCredit
        resp = requests.get(ENV.OUT_OF_CREDIT_URL, verify=False)
        print("Status returned:", resp.status_code)
        # ตรวจสอบว่าการส่งคำขอสำเร็จหรือไม่
        if resp.status_code == 200:
            # กลับไปยัง URL ที่กำหนดในไฟล์ settings.py เมื่อ OutOfCredit
            return RedirectResponse(url=ENV.OUT_OF_CREDIT_URL)
        
# ปัจจุบันส่งไปยัง www.google.com สามารถเปลี่ยนไปที่หน้า occ ได้
        