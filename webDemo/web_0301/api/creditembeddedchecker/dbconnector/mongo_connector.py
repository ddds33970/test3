import settings as ENV
from utils.singleton_template import Singleton
from pymongo import MongoClient

class MongoConnector(metaclass=Singleton):
    # สร้าง Client เพื่อเชื่อมต่อ MongoDB โดยใช้ข้อมูลจากไฟล์ settings.py
    client = MongoClient(ENV.MONGODB_URI)

    # เลือก Database โดยใช้ข้อมูลจากไฟล์ settings.py
    user_db = client[ENV.USER_DATABASE_NAME]
    creditchecker_db = client[ENV.CREDIT_DATABASE_NAME]

    # เลือก Collection
    users_collection = user_db["users"]
    embed_logs_collection = creditchecker_db["embed_logs"]

    @classmethod
    def connect(cls) -> MongoClient:
        return cls.user_db, cls.creditchecker_db

    @classmethod
    def disconnect(cls):
        return cls.client.close()

