from fastapi import FastAPI, Depends, HTTPException
# from fastapi.responses import JSONResponse
# from fastapi_jwt_auth import AuthJWT
# from fastapi_jwt_auth.exceptions import AuthJWTException
from fastapi.middleware.cors import CORSMiddleware
import uvicorn
# from datetime import timedelta

# Controller
from controller import credit_embedded_controller

# Repositories
from repositories.credit_repositories import *

# Model
from model.data_models import *

# Setting
import settings as ENV

app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"]
)

# กำหนดเส้นทางสำหรับ URL
@app.get("/")
async def index():
    return 'creditEmbeddedChecker API Service' 

# กำหนดเส้นทางสำหรับเพิ่ม credit ให้กับ user (สร้างขึ้นเพื่อทดสอบ)
@app.post("/add_credits")
async def add_credits(
    user_id: str,
    additional_credits: float, 
):
    try:
        # พยายามค้นหาข้อมูล user ด้วย user_id
        users_data = CreditRepositories.find_user(user_id)

        if users_data:
            # ตรวจสอบว่า user มี credit อยู่แล้วหรือไม่
            if 'credits' in users_data:  
                # รับค่า credit ปัจจุบัน
                current_credits = float(users_data['credits']) 
                # คำนวณ credit ใหม่หลังจากทำการเพิ่ม
                new_credits = current_credits + additional_credits

                # update credit ของ user ใน database
                CreditRepositories.update_credits(user_id, new_credits)  

                response_data = {
                    "status": "success",
                    "message": f"Credits added successfully. Latest credit now: {new_credits}"
                }
            else:
                # หาก user ไม่มี creidt ให้เพิ่ม credit ใหม่
                users_data['credits'] = additional_credits
                CreditRepositories.update_credits(user_id, additional_credits)

                response_data = {
                    "status": "success",
                    "message": f"Credits added successfully. Latest credit now: {additional_credits}"
                }
        else:
            # หากไม่พบ user ให้กำหนดข้อความผิดพลาด
            response_data = {
                "status": "error",
                "message": f"User with ID {user_id} not found"
            }
        return response_data  

    except Exception as e:
        # จัดการข้อผิดพลาด
        print(
            "Error in add_credits endpoint: " + str(e),
            type(e).__name__,          # รับชนิดของข้อผิดพลาด
            __file__,                  # ไฟล์ที่เกิดข้อผิดพลาด
            e.__traceback__.tb_lineno  # เลขบรรทัดที่เกิดข้อผิดพลาด
        )
        # ยกเลิก HTTPException พร้อมรายละเอียดที่เหมาะสม
        raise HTTPException(
            status_code=401,
            detail="Could not validate credentials",
            headers={"WWW-Authenticate": "Bearer"},
        )

# เพิ่มเส้นทางจาก credit_embedded_controller
app.include_router(credit_embedded_controller.credit_embedded_app)

# รันแอปพลิเคชัน FastAPI โดยใช้ uvicorn
if __name__ == '__main__':
    uvicorn.run("main:app", PORT=ENV.PORT)
