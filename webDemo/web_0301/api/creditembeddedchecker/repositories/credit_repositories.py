from dbconnector.mongo_connector import MongoConnector
from datetime import datetime

# เชื่อมต่อกับ MongoDB และเลือก Database ที่เกี่ยวข้อง
user_db, creditchecker_db = MongoConnector.connect()

# Collection ที่จะใช้งาน
users_collection = user_db["users"]
embed_logs_collection = creditchecker_db["embed_logs"]

class CreditRepositories:

    @staticmethod
    def insert_log(data):
        # insert ข้อมูลลงใน collection embed_logs
        return embed_logs_collection.insert_one(data)

    @staticmethod
    def find_user(user_id):
        # find ข้อมูล user โดยใช้ user_id
        return users_collection.find_one({'userId': user_id}, {'_id': False})

    @staticmethod
    def update_credits(user_id, credits):
        # update จำนวน credits ของ user 
        return users_collection.update_one({'userId': user_id}, {"$set": {'credits': credits}})

    @staticmethod
    def update_uploadquota(user_id, uploadquota):
        # update uploadQuota ของ user และ lastUpdate แบบ utcnow
        users_collection.update_one({'userId': user_id}, {"$set": {'uploadQuota': uploadquota}})
        users_collection.update_one({'userId': user_id}, {"$set": {'lastUpdate': datetime.utcnow()}})

    # @staticmethod
    # def update_lastupdate(user_id, lastupdate):
    #     return users_collection.update_one({'userId': user_id}, {"$set": {'lastUpdate': lastupdate}})