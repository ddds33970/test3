import os
import settings as ENV

## services
# 1 credit = 5 บาท ปัจจุขับให้แสดงผ่าน credit 
# โดยจะแสดงก่อน และ หลังโดน count หักว่ามีกี่บาท

## utils
def check_credit(documents: list):
  # คำนวณผลรวมของความยาวของเนื้อหาทุกหน้าของทุก doc
  total_characters = sum(len(doc["page_content"]) for doc in documents)
  # คำนวณจำนวน Tokens
  tokens = total_characters // 4
  # คำนวณค่าใช้จ่าย (USD) text-embedding-3-large $0.00013 / 1K tokens
  cost_usd = tokens / 1000 * 0.00013
  # แปลง USD เป็น บาท
  cost_thb = cost_usd * 36
  return cost_thb

