class Singleton(type):

    # ใช้เทคนิค metaclass เพื่อสร้าง class ที่มีเพียง instance เดียวเท่านั้น
    def __init__(cls, name, bases, attrs, **kwargs):
        super().__init__(name, bases, attrs)
        cls._instance = None

    # เมื่อมีการเรียกใช้ class ที่ถูกสร้างด้วย method __call__ โดยใช้เทคนิค metaclass
    def __call__(cls, *args, **kwargs):
        # ตรวจสอบว่ามี instance ของ class นั้นหรือไม่ ถ้าไม่มีจะสร้าง instance ใหม่
        # หากมี instance อยู่แล้ว จะคืนค่า instance ออกไปเลย
        if cls._instance is None:
            cls._instance = super().__call__(*args, **kwargs)
        return cls._instance

