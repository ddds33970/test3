function uploadFile() {
    var form = document.getElementById('uploadForm');
    var formData = new FormData(form);
    formData.append('userId', 'a10000');
    formData.append('companyId', '10000');
    formData.append('role', 'admin');

    fetch('http://127.0.0.1:8000/upload_pdf', {
        method: 'POST',
        body: formData
    })
    .then(response => response.json())
    .then(data => {
        console.log('Success:', data);
    })
    .catch((error) => {
        console.error('Error:', error);
    });
}