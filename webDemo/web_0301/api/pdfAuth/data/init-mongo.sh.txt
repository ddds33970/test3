mongo -- "$MONGO_INITDB_DATABASE" <<EOF
    var rootUser = '$MONGO_INITDB_ROOT_USERNAME';
    var rootPassword = '$MONGO_INITDB_ROOT_PASSWORD';
    var admin = db.getSiblingDB('admin');
    admin.auth(rootUser, rootPassword);

    var user = '$MONGO_INITDB_USERNAME';
    var passwd = '$MONGO_INITDB_PASSWORD';

    db.createUser({
      user: user,
      pwd: passwd,
      roles: [
        { role: 'readWriteAnyDatabase', db: 'admin' },
        { role: 'readWrite', db: '$MONGO_INITDB_DATABASE' },
        { role: 'read', db: '$MONGO_INITDB_DATABASE' }
      ]
    });

    var user1 = '$MONGO_INITDB_USERNAME1';
    var passwd1 = '$MONGO_INITDB_PASSWORD1';

    db.createUser({
      user: user1,
      pwd: passwd1,
      roles: [
        { role: 'readAnyDatabase', db: 'admin' },
        { role: 'read', db: '$MONGO_INITDB_DATABASE' }
      ]
    });
EOF
