from pydantic import BaseModel, Field
from bson import ObjectId
from datetime import datetime
from typing import Optional

class AuthDetails(BaseModel):
    username: str
    password: str

class User(BaseModel):
    userId: str = Field(default_factory=lambda: str(ObjectId()))
    username: str
    password: str
    companyId: str = Field(default_factory=lambda: str(ObjectId()))
    startUpdate: Optional[datetime] = Field(default_factory=datetime.utcnow)
    lastUpdate: Optional[datetime] = Field(default_factory=datetime.utcnow)
    uploadQuota: Optional[float] = 0.000000
    queryQuota: Optional[float] = 0.000000
    role: Optional[str] = "normal"
    credits: Optional[float] = 0.000000

class LogLogin(BaseModel):
    logId: str
    userId: str
    status: str
    username: str
    password: Optional[str] = "-"
    errorMessage: str
    lastUpdate: Optional[datetime]

    @classmethod
    def log_login_success(cls, user_id: str, username: str, password: str):
        # สร้าง LogObject สำหรับเข้าสู่ระบบสำเร็จ
        log_object = cls(
            logId=str(ObjectId()),
            userId=user_id,
            status="success",
            username=username,
            password=password,
            errorMessage="-",
            lastUpdate=datetime.utcnow(),
        )
        return log_object

    @classmethod
    def log_login_failure(cls, username: str, password_hashed: str):
        # สร้าง LogObject สำหรับเข้าสู่ระบบไม่สำเร็จ
        log_object = cls(
            logId=str(ObjectId()),
            userId="-",
            status="failed",
            username=username,
            password="-",
            errorMessage="Invalid username or password",
            lastUpdate=datetime.utcnow(),
        )
        return log_object
    
    @classmethod
    def update_log(cls, log_collection, filter_query, update_data):
        # ตรวจสอบว่ามี log เดิมไหม
        existing_log = log_collection.find_one(filter_query)
        if existing_log:
            if "logObject" in existing_log:
                new_update_data = {
                    "$set": {
                        "lastUpdate": datetime.utcnow(),
                        "logObject": existing_log['logObject'].dict()
                    }
                }
            else:
                new_update_data = {
                    "$set": {
                        "lastUpdate": datetime.utcnow(),
                        "logObject": update_data['logObject'].dict()
                    }
                }
            # update log
            log_collection.update_one(filter_query, new_update_data)
        else:
            # insert log
            log_collection.insert_one(update_data.dict())

