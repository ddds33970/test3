import uvicorn
from fastapi.staticfiles import StaticFiles
from fastapi.security import OAuth2PasswordBearer
from fastapi import FastAPI, Depends, Request, UploadFile
from fastapi.responses import HTMLResponse
from fastapi.templating import Jinja2Templates
from fastapi.middleware.cors import CORSMiddleware

# Controllers
from controllers.auth_controller import *

# Services
from services.auth_service import *

# Model
from model.data_model import *

# Repository
from repositories.db_repository import *

# Setting
import settings as ENV

# กำหนดตำแหน่งที่จะบันทึกไฟล์อัปโหลด จาก Dropzone
from pathlib import Path
UPLOAD_DIR = Path() / 'uploads'

app = FastAPI()
mongodb = dbRepository()
mongo_connector = MongoConnector()
templates = Jinja2Templates(directory="templates")


app.mount("/static", StaticFiles(directory="static"), name="static")
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],  
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


# กำหนด OAuth2 Scheme สำหรับการรับ Token
oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")
auth_handler = AuthHandler()

# function สำหรับดึงข้อมูล user ปัจจุบันจาก Token
async def get_current_user(token: str = Depends(oauth2_scheme)):
    return auth_handler.decode_token(token)

# Endpoint หน้าต่างๆ 
@app.get("/", response_class=HTMLResponse)
async def read_root(request: Request):
    return templates.TemplateResponse("index.html", {"request": request})

@app.get("/pdf", response_class=HTMLResponse)
async def read_pdf(request: Request):
    return templates.TemplateResponse("pdf.html", {"request": request})

@app.get("/home", response_class=HTMLResponse)
async def home_route(request: Request):
    return await home(request)

@app.get('/logout', response_class=HTMLResponse)
async def logout_route(request: Request):
    return await logout(request)

@app.post("/register")
async def register_route(users: User):
    return await register(users)

@app.post('/login')
async def login_route(data: AuthDetails):
    return await login(data)

@app.post('/uploadfile')
async def create_upload_file(file_upload: UploadFile):
    data = await file_upload.read()
    save_to = UPLOAD_DIR / file_upload.filename
    with open(save_to, 'wb') as f:
        f.write(data)
    return {"filename": file_upload.filename}


if __name__ == "__main__":
    uvicorn.run("main:app", port=int(ENV.PORT), host=ENV.HOST, reload=True)
