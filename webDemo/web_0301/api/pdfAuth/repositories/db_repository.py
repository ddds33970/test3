from dbconnector.mongo_connector import MongoConnector
from model.data_model import *

# เชื่อมต่อกับ MongoDB และเลือก Database ที่เกี่ยวข้อง
user_db, log_db = MongoConnector.connect()

# Collection ที่จะใช้งาน
users_collection = user_db["users"]
logs_collection = log_db["logs"]

class dbRepository:

    @staticmethod
    def save_user(user: User):
        # แปลงข้อมูล user จาก Pydantic model เป็น dict
        user_dict = user.dict()
        # แปลง userId, companyId เป็น str เพราะในฐานข้อมูลเป็น ObjectId
        user_dict["userId"] = str(user_dict["userId"])
        user_dict["companyId"] = str(user_dict["companyId"])
        # บันทึกข้อมูล user ลงใน database collection users
        users_collection.insert_one(user_dict)
