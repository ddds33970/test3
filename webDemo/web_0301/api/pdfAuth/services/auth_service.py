import jwt
from fastapi import HTTPException, Security
from fastapi.security import HTTPAuthorizationCredentials, HTTPBearer
from passlib.context import CryptContext
from datetime import datetime, timedelta

class AuthHandler():
    # กำหนด Security เป็น HTTPBearer เพื่อให้ FastAPI รู้ว่าจะใช้ Bearer Token
    security = HTTPBearer()

    # กำหนด context สำหรับการเข้ารหัสและตรวจสอบรหัสผ่าน
    pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")
    
    # กำหนดค่า Secret Key สำหรับการเซ็นและเข้ารหัส Token
    secret = 'SECRET'

    # function เข้ารหัสรหัสผ่าน
    def get_password_hash(self, password):
        return self.pwd_context.hash(password)

    # function ตรวจสอบรหัสผ่าน
    def verify_password(self, plain_password, hashed_password):
        return self.pwd_context.verify(plain_password, hashed_password)

    # function สร้าง Token
    def encode_token(self, user_id):
        payload = {
            'exp': datetime.utcnow() + timedelta(days=0, minutes=5),
            'iat': datetime.utcnow(),
            'sub': user_id
        }
        return jwt.encode(
            payload,
            self.secret,
            algorithm='HS256'
        )

    # function ถอดรหัส Token
    def decode_token(self, token):
        try:
            payload = jwt.decode(token, self.secret, algorithms=['HS256'])
            return payload['sub']
        except jwt.ExpiredSignatureError:
            # หาก Token หมดอายุให้เกิด HTTPException ที่รหัส 401
            raise HTTPException(status_code=401, detail='Signature has expired')
        except jwt.InvalidTokenError as e:
            # หาก Token ไม่ถูกต้องให้เกิด HTTPException ที่รหัส 401
            raise HTTPException(status_code=401, detail='Invalid token')

    # function Wrapper สำหรับตรวจสอบ Token ในรายการที่ต้องการ
    def auth_wrapper(self, auth: HTTPAuthorizationCredentials = Security(security)):
        return self.decode_token(auth.credentials)