from fastapi import HTTPException, Request, status
from fastapi.responses import RedirectResponse
from fastapi.templating import Jinja2Templates

# services
from services.auth_service import AuthHandler

# Model
from model.data_model import *

# Repository
from repositories.db_repository import *

# ใช้งาน Jinja2Templates สำหรับสร้าง templates
templates = Jinja2Templates(directory="templates")

# เชื่อมต่อกับ MongoDB
mongo_connector = MongoConnector()

# Create Object
mongodb = dbRepository()
auth_handler = AuthHandler()

# เริ่มการทำงานของหน้า Home
async def home(request: Request):
    return templates.TemplateResponse("home.html", {"request": request})

async def read_home(request: Request):
    return templates.TemplateResponse("home.html", {"request": request})

# ออกจากระบบ
async def logout(request: Request):
    # เรียกใช้งาน RedirectResponse เพื่อเปลี่ยนเส้นทางการเข้าถึงหน้าหลัก
    response = RedirectResponse(url="/", status_code=status.HTTP_200_OK)
    return response

# ลงทะเบียน user ใหม่
async def register(users: User):
    # ตรวจสอบว่ามี username ที่ถูกลงทะเบียนไว้แล้วไหม
    existing_user = mongo_connector.users_collection.find_one({"username": users.username})

    if existing_user:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Username already registered")

    # เข้ารหัสผ่านของ user
    hashed_password = auth_handler.get_password_hash(users.password)
    users.password = hashed_password

    # บันทึกข้อมูล user ใหม่ลงใน database
    mongodb.save_user(users)
    return {"message": "User registered successfully"}

# เข้าสู่ระบบ
async def login(data: AuthDetails):
    username = data.username
    password = data.password
    user = mongo_connector.users_collection.find_one({"username": username})

    # ตรวจสอบการเข้าสู่ระบบ
    if user and auth_handler.verify_password(password, user["password"]):
        user_id = str(user["userId"])
        token = auth_handler.encode_token(user_id)

        hashed_password = auth_handler.get_password_hash(password)
        log_data = LogLogin.log_login_success(user_id, username, hashed_password)

        filter_query = {"logObject.userId": user_id}
        LogLogin.update_log(mongo_connector.logs_collection, filter_query, log_data)

        return {"token": token, "status": "success"}
    else:
        existing_log = mongo_connector.logs_collection.find_one({"logObject.username": username})

        if existing_log:
            # update ข้อมูลการเข้าสู่ระบบล่าสุด
            update_data = {
                "$set": {
                    "lastUpdate": datetime.utcnow(),
                    "logObject.password_hashed": auth_handler.get_password_hash(password)
                }
            }
            filter_query = {"logObject.username": username}
            LogLogin.update_log(mongo_connector.logs_collection, filter_query, update_data)
        else:
            # บันทึกข้อมูลการเข้าสู่ระบบที่ไม่สำเร็จ
            log_data = LogLogin.log_login_failure(username, password)
            filter_query = {"logObject.username": username, "logObject.password": password}
            LogLogin.update_log(mongo_connector.logs_collection, filter_query, log_data)

        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Invalid username or password")
