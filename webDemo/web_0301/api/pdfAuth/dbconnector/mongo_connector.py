import settings as ENV
from pymongo import MongoClient

class MongoConnector:
    # สร้าง Client เพื่อเชื่อมต่อ MongoDB โดยใช้ข้อมูลจากไฟล์ settings.py
    client = MongoClient(ENV.MONGODB_URI)

    # เชื่อมต่อกับ MongoDB และเลือก Database ที่เกี่ยวข้อง
    user_db = client[ENV.USER_DATABASE_NAME]
    log_db = client[ENV.LOG_DATABASE_NAME]

    # Collection ที่จะใช้งาน
    users_collection = user_db["users"]
    logs_collection = log_db["logs"]


    @classmethod
    def connect(cls):
        return cls.user_db, cls.log_db 

    @classmethod
    def disconnect(cls):
        return cls.client.close()
