# Localhost
PORT=8010
MONGODB_URI=mongodb://Admin:Admin15277@localhost:8010/User

1. python -m venv env
2. .\env\Scripts\activate
3. pip install -r requirements.txt
4. uvicorn main:app --reload

```
docker compose up
```

https://blog.me-idea.in.th/mongodb-docker-compose-up-%E0%B8%9B%E0%B8%B8%E0%B9%8A%E0%B8%9A%E0%B8%AA%E0%B8%A3%E0%B9%89%E0%B8%B2%E0%B8%87-mongo-database-%E0%B8%9B%E0%B8%B1%E0%B9%8A%E0%B8%9A-d27004a9fd78

ในส่วนของ docker-compose.db.yml 
```
ต้อง Run - ./init-mongo.sh:/docker-entrypoint-initdb.d/init-mongo.sh ก่อน
โดยนำ init-mongo.sh มา Run เพื่อสร้าง LocalDatabase โดยกำหนด Role 
```
```
เมื่อ Run เสร็จแล้วให้กลับมา Run ./mongodb-init-scripts:/docker-entrypoint-initdb.d 
เพื่อความ Secure โดยวิธีนี้จะเป็นแบบ Local แต่ปัจจุบันเราใช้ MongoDB Atlas แทนแล้ว
แต้ว่ายังต้องเก็บเอาไว้เผื่อยังได้ใช้
```

เหตุผลเพื่อจะซ่อน init.sh (ลบ mongodb-init-scripts ก่อนเล่น)

To hide the init.sh script used for initializing a MongoDB container when using Docker Compose's docker-compose up, you can follow these steps:

Place the init.sh script in a separate directory, let's call it mongodb-init-scripts. This directory should be at the same level as your docker-compose.yml file.

Update your docker-compose.yml file to mount the mongodb-init-scripts directory as a volume inside the MongoDB container. You can do this by adding a volumes section under the MongoDB service definition. Here's an example:

version: '3'
services:
  mongodb:
    image: mongo:latest
    volumes:
      - ./mongodb-init-scripts:/docker-entrypoint-initdb.d
    ports:
      - "27017:27017"


In this example, we're mounting the mongodb-init-scripts directory into the /docker-entrypoint-initdb.d directory inside the MongoDB container. MongoDB automatically executes scripts in this directory during container initialization.

With this setup, you can now run docker-compose up without exposing the init.sh script in the host machine's filesystem, as it's now only available inside the container.

When you run docker-compose up, Docker Compose will start the MongoDB container and execute the scripts in the docker-entrypoint-initdb.d directory, including your init.sh script, during container initialization.

This approach keeps the initialization script hidden from the host machine while allowing it to be used by the MongoDB container during startup.

*ถ้าจะใช้ local database ให้เปิด container ทุกครั้งก่อน Run uvicorn เพื่อเล่น*