# settings.py

from os import getenv
from os.path import join, dirname, abspath
from dotenv import load_dotenv

dotenv_path = join(dirname(__file__), '.env')

load_dotenv(dotenv_path)

PORT = int(getenv("PORT"))
MONGODB_URI = str(getenv("MONGODB_URI"))
MONGODB_DATABASE_NAME = str(getenv("MONGODB_DATABASE_NAME"))
PINECONE_API_KEY = str(getenv("PINECONE_API_KEY"))
PINECONE_INDEX_NAME = str(getenv("PINECONE_INDEX_NAME"))
GOOGLE_API_KEY = str(getenv("GEMINI_API_KEY"))