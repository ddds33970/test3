# ver05/util/prompt_rephrase.py
from langchain.prompts.prompt import PromptTemplate

template_reqphrase = """Given the following conversation and a follow up question, rephrase the follow up question to be a standalone question.
Chat History:
{chat_history}
Follow Up Input: {question}
Remember just answer Standalone questions in {language} language.
Standalone question:"""

CONDENSE_QUESTION_PROMPT = PromptTemplate.from_template(template_reqphrase)
# ควรเป็นสิ่งที่ไป search และตอบคำถามสุดท้าย