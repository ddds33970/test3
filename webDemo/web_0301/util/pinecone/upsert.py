import os
from langchain_openai import OpenAIEmbeddings
from langchain.text_splitter import RecursiveCharacterTextSplitter
from langchain_community.document_loaders import CSVLoader, PyPDFLoader, TextLoader
from langchain_community.document_loaders import UnstructuredWordDocumentLoader
from pinecone import Pinecone
from pymongo import MongoClient
import datetime
import other.settings as ENV



def all_file_loader(directory):
    
    embeddings_model = OpenAIEmbeddings()
    documents = []

    for filename in os.listdir(directory):
        text_splitter = RecursiveCharacterTextSplitter(chunk_size=1000, chunk_overlap=200)
        if filename.endswith(".csv"):
            loader = CSVLoader(os.path.join(directory, filename))
            documents.extend(text_splitter.split_documents(loader.load()))
        if filename.endswith(".txt"):
            loader = TextLoader(os.path.join(directory, filename))
            documents.extend(text_splitter.split_documents(loader.load()))
        if filename.endswith(".pdf"):
            loader = PyPDFLoader(os.path.join(directory, filename))
            documents.extend(text_splitter.split_documents(loader.load()))
        if filename.endswith(".docx"):
            loader = UnstructuredWordDocumentLoader(os.path.join(directory, filename))
            documents.extend(text_splitter.split_documents(loader.load()))

    return embeddings_model, documents

def upsert_to_pinecone(username_new, directory):  
    folder = username_new
    userId = username_new
    role = "role" # ต้องเปลี่ยนเป็น role จริงๆ ที่ดึงมาจาก mongodb

    # Pinecone connect
    api_key = ENV.PINECONE_API_KEY
    pc = Pinecone(api_key=api_key)
    index = pc.Index(ENV.PINECONE_INDEX_NAME)
    
    
    # MongoDB connect
    client = MongoClient(os.getenv("MONGODB_URI"))
    db = client[os.getenv("MONGODB_DATABASE_NAME")]
    collection = db[os.getenv("MONGODB_COLLECTION_NAME")]

    # ดึงค่ามาจาก all_file_loader ใน ver05/process/all_file_loader.py
    embeddings_model, documents = all_file_loader(directory)

    # loop ในการ upsert ข้อมูลไปใน Pinecone และ insert log ลงใน MongoDB
    for i in range(len(documents)):
        # ดึงค่ามาจาก all_file_loader ทีละตัว
        page_content = documents[i].page_content
        fileId = documents[i].metadata['source'].split('/')[-1]
        
        # สร้าง item_id เพื่อกำหนด id ของ document ที่จะ upsert ไปใน Pinecone
        item_id = f"{userId}_{fileId}_doc_{i+1}"
        # สร้าง item_data เพื่อกำหนด metadata ของ document ที่จะ upsert ไปใน Pinecone
        item_data = {
            "fileId": fileId,
            "text": page_content
        }
        
        # เรียกใช้ฟังก์ชัน embed_documents ใน ver05/process/all_file_loader.py เพื่อแปลง page_content ให้เป็น vector ทีละตัว
        embeddings_values = embeddings_model.embed_documents(documents[i].page_content)
        
        # upsert ข้อมูลไปใน Pinecone
        index.upsert(
            vectors=[
                {
                    "id": item_id,
                    # upsert ข้อมูลไปใน Pinecone ในรูปแบบ vector ทีละตัว โดยเริ่มที่ชุดที่ 0
                    "values": embeddings_values[0][:],
                    "metadata": item_data
                }
            ],
            namespace=folder
        )
        # กำหนดข้อมูลที่จะ insert ไปใน MongoDB
        log_upsert_data = {
            "timestamp": datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            "action": f"add documents ({item_id}) at {folder} by {userId} ({role})"
        }
        # insert log ขึ้นใน MongoDB
        collection.insert_one(log_upsert_data)
    
    print(f"Data loaded into Pinecone successfully.\n")
    # delete_files_in_folder(directory)
    # print(fileId, "\n" + f"text:", page_content, "\n\n\n")

# upsert_to_pinecone()