# ver05/process/delete_from_pinecone.py
from pinecone import Pinecone
import other.settings as ENV
from pymongo import MongoClient
import datetime
import os

def delete_namespace_pinecone(username_new):
    folder = username_new
    userId = username_new
    role = "role" # ต้องเปลี่ยนเป็น role จริงๆ ที่ดึงมาจาก mongodb
    
    # Pinecone connect
    api_key = ENV.PINECONE_API_KEY
    pc = Pinecone(api_key=api_key)
    index = pc.Index(ENV.PINECONE_INDEX_NAME)
    
    # MongoDB connect
    client = MongoClient(os.getenv("MONGODB_URI"))
    db = client[os.getenv("MONGODB_DATABASE_NAME")]
    collection = db[os.getenv("MONGODB_COLLECTION_NAME")]
    
    
    
    index.delete(delete_all=True, namespace=folder)
    
    # กำหนดข้อมูลที่จะ insert ไปใน MongoDB
    log_delete_data_namespace = {
    "timestamp": datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
        "action": f"delete documents ({folder}) by {userId} ({role})"
    }
    
    # insert log ขึ้นใน MongoDB
    collection.insert_one(log_delete_data_namespace)
    
    print(f"\ndelete folder: {folder} completed.")