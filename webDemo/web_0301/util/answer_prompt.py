from langchain_core.prompts import ChatPromptTemplate # ผ่าน parameter ตรงนี้ ตอนจะเรียกภาษาได้

template = """You are a helpful assistant. Your task is to answer the question using only the provided "Document" delimited by triple backticks:
Document:
```
{context1}
{context2}
```
Question: {question}
Remember using knowledge from "Document" to answer the "Question" in {language} language.
"""
ANSWER_PROMPT = ChatPromptTemplate.from_template(template) # {"language": "Thai"}