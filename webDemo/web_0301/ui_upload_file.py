import streamlit as st
import os
import other.settings as ENV
from util.pinecone.upsert import upsert_to_pinecone
from util.delete_in_local import delete_files_in_folder
from pinecone import Pinecone

# Pinecone connect
api_key = ENV.PINECONE_API_KEY
pc = Pinecone(api_key=api_key)
index = pc.Index(ENV.PINECONE_INDEX_NAME)

def st_upload_file(username_new):
    # แสดงส่วน "Upload File"
    st.header("Upload File")

    # แสดงส่วนอัปโหลดไฟล์
    with st.form("Upload File", clear_on_submit=True):
        uploaded_files = st.file_uploader("Please select file", type=['.pdf', '.docx', '.txt', '.csv'], accept_multiple_files=False)
        path = "/Users/j.papontee/Desktop/Botnoi/webDemo/web_0103/file_upload" # ต้องเปลี่ยนเป็น path ที่เราต้องการเก็บไฟล์

        # ตรวจสอบว่ามีการอัปโหลดไฟล์หรือไม่
        if uploaded_files is not None:
            # แสดงชื่อไฟล์
            for uploaded_file in uploaded_files:
                st.write("Filename:", uploaded_file.name)
            # บันทึกไฟล์ลงในโฟลเดอร์ที่เราต้องการ
                with open(os.path.join(path, uploaded_file.name), "wb") as f:
                    f.write(uploaded_file.getbuffer())
        
        # แสดงปุ่ม Upload 
        button_upload = st.form_submit_button("Upload")
        
        print("button_upload : ", button_upload)
        if button_upload:
            # streamlit รันจากบนลงล่างตลอด ต้องมีตัวไว้ดัก เช่น จะทำ...​เมื่อกดปุ่มบลาๆ
            upsert_to_pinecone(username_new, path) # delete_files_in_folder(path) อยู่ในนี้
        # เรียกใช้ฟังก์ชั่นโดยระบุ path ของโฟลเดอร์ที่คุณต้องการลบไฟล์
        delete_files_in_folder(path)
        # ลบค่าในตัวแปร uploaded_files
        uploaded_files = None