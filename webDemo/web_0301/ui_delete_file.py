import streamlit as st
from util.pinecone.delete_from_pinecone import delete_namespace_pinecone

def delete_namespace(username_new):
    st.header("Delete File on Pinecone")
    st.button('Delete File on Pinecone', on_click=delete_namespace_pinecone, args=(username_new,))