from langchain_core.prompts import ChatPromptTemplate

template_for_specific_question = """from "question" and "Search terms"
```
question:
{question_reqphrase}
Search terms:
{retrieved_keyword}

I want to retain the meaning of "question" and add a keyword from "Search terms" in the context of the question to be "New_Question" if it won't have a keyword. I want a "New_Question" to be a clear, specific, and simple question in one question not complex in {language} language.
New_Question:
"""

SPECIFIC_QUESTION_PROMPT = ChatPromptTemplate.from_template(template_for_specific_question)
# ควรเอาไป search และ ตอบคำถามสุดท้าย # ต้องเช็คเพื่อความมั่นใจเสมอว่า คำตอบสุดท้ายที่ได้มายังตรงกับที่ user ถามไหม