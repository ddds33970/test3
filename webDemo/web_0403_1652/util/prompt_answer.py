from langchain_core.prompts import ChatPromptTemplate # ผ่าน parameter ตรงนี้ ตอนจะเรียกภาษาได้

template = """You are a helpful assistant. Your task is to answer the question using only the provided "Document" delimited by triple backticks.
"Document" have many sections and each section ends with an underscore. Use the section that is relevant to answer the following question.

Document:
```
{context1}
```
Question: {question}
Remember using knowledge only from "Document" to answer the "Question" in {language} language.
Answer:
"""
ANSWER_PROMPT = ChatPromptTemplate.from_template(template) # {"language": "Thai"}