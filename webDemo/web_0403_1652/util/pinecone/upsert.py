import os
import uuid
import datetime
import itertools
import streamlit as st
import other.settings as ENV
# from langchain.embeddings import CohereEmbeddings
from util.delete_in_local import delete_file_duplicate
from util.connector.cohere_connector import cohere_connector
# from util.connector.pinecone_connector import pinecone_connect
from langchain.text_splitter import RecursiveCharacterTextSplitter
from util.connector.pinecone_connector import pinecone_connect_parallel
from langchain_community.document_loaders import UnstructuredMarkdownLoader
from langchain_community.document_loaders import UnstructuredWordDocumentLoader
from langchain_community.document_loaders import CSVLoader, PyMuPDFLoader, TextLoader #, PyPDFLoader
from util.connector.mongodb_connector import mongo_connect_fileOfUser_collection, mongo_connect_latest_upload_collection

def chunks(iterable, batch_size=100):
    """A helper function to break an iterable into chunks of size batch_size."""
    it = iter(iterable)
    chunk = tuple(itertools.islice(it, batch_size))
    while chunk:
        yield chunk
        chunk = tuple(itertools.islice(it, batch_size))
        
def upsert_to_pinecone(document, username):
    folder = username
    userId = username
    # Cohere connect
    cohere_connect = cohere_connector()
    
    # Pinecone connect
    # index = pinecone_connect()
    index, pc = pinecone_connect_parallel()
    
    embeddings_values_list = []
    chunk_size_embeddings = 90 # จำนวนข้อมูลที่ต้องการดึง
    for i in range(0, len(document), chunk_size_embeddings):
        chunkForEmbeddings = [doc.page_content for doc in document[i:i+chunk_size_embeddings] if hasattr(doc, 'page_content')]
        if not chunkForEmbeddings:
            continue

        embeddings_values = cohere_connect.embed(
            texts=chunkForEmbeddings,
            input_type="search_document",
            model='embed-multilingual-v3.0',
            truncate=None
        )
        embeddings_values_list.extend(embeddings_values.embeddings)

    upsert_data = []
    for i, (EBDV, doc) in enumerate(zip(embeddings_values_list, document)):
        fileName = doc.metadata['source'].split('/')[-1]
        id = f"{userId}_{fileName}_doc_{i+1}"
        metadata = {
            "fileName": fileName,
            "text": doc.page_content
        }
        upsert_data.append({
            "id": id,
            "values": EBDV,
            "metadata": metadata
        })
    # Upsert data with 100 vectors per upsert request asynchronously
    # - Pass async_req=True to index.upsert()
    with pc.Index(ENV.PINECONE_INDEX_NAME, pool_threads=30) as index:
        # Send requests in parallel
        async_results = [
            index.upsert(vectors=upsert_data, namespace=folder, async_req=True)
            for upsert_data in chunks(upsert_data, batch_size=100)
        ]
        # Wait for and retrieve responses (this raises in case of error)
        [async_result.get() for async_result in async_results]
    
    ###########################################################################
    # # loop ทีละ document แล้ว upsert ข้อมูลไปใน Pinecone ซึ่ง document คือ list ของ document ที่ได้จากการ split แล้ว
    # for i in range(len(document)):
    #     # ดึงค่ามาจาก all_file_loader ทีละตัว
    #     page_content = document[i].page_content
    #     fileName = document[i].metadata['source'].split('/')[-1]
        
    #     # สร้าง item_id เพื่อกำหนด id ของ document ที่จะ upsert ไปใน Pinecone
    #     item_id = f"{userId}_{fileName}_doc_{i+1}"
    #     # สร้าง item_data เพื่อกำหนด metadata ของ document ที่จะ upsert ไปใน Pinecone
    #     item_data = {
    #         "fileName": fileName,
    #         "text": page_content
    #     }
        
    #     # เรียกใช้ฟังก์ชัน embed_documents ใน ver05/process/all_file_loader.py เพื่อแปลง page_content ให้เป็น vector ทีละตัว
    #     embeddings_values = embeddings_model.embed_documents([document[i].page_content])
        
    #     # upsert ข้อมูลไปใน Pinecone
    #     index.upsert(
    #         vectors=[
    #             {
    #                 "id": item_id,
    #                 # upsert ข้อมูลไปใน Pinecone ในรูปแบบ vector ทีละตัว โดยเริ่มที่ชุดที่ 0
    #                 "values": embeddings_values[0][:],
    #                 "metadata": item_data
    #             }
    #         ],
    #         # ให้ upsert ไปใน namespace ของ user คนนั้น
    #         namespace=folder
    #     )
    ###########################################################################
        
    # clear document = [] เมื่อจบการทำงานของฟังก์ชันนี้
    document.clear()
# ฟังก์ชันนี้ จะทำการลบไฟล์ที่อัปโหลดซ้ำออกจาก local directory และเอาเฉพาะไฟล์ที่ชื่อไม่ซ้ำมา split และ upsert ไปใน Pinecone
def file_loader(username, role, filename, directory):
    # mongo connect "latest_upload" collection ของ user คนนั้น
    mongo_latest_upload_collection = mongo_connect_latest_upload_collection(username)
    mongo_fileOfUser_collection = mongo_connect_fileOfUser_collection(username)
    
    # อันนี้เป็นฟังก์ชันที่ใช้ในการ split text ให้เป็นหลายๆ ชุด โดยใช้ RecursiveCharacterTextSplitter
    text_splitter = RecursiveCharacterTextSplitter(chunk_size=1000, chunk_overlap=200)
    # ตรงนี้จะเช็คว่าไฟล์ที่อัปโหลดเป็นไฟล์ประเภทอะไร แล้วทำการ split และ upsert ไปใน Pinecone หลังจากนั้นจะลบไฟล์ที่อัปโหลดออกจาก local directory

    fileId = str(uuid.uuid4())
    
    if filename.endswith(".csv"):
        document = []
        type = "csv"
        loader = CSVLoader(os.path.join(directory, filename))
        document.extend(text_splitter.split_documents(loader.load()))
        length = len(document)
        upsert_to_pinecone(document, username)
        delete_file_duplicate(filename, directory)
    if filename.endswith(".txt"):
        document = []
        type = "txt"
        loader = TextLoader(os.path.join(directory, filename))
        document.extend(text_splitter.split_documents(loader.load()))
        length = len(document)
        upsert_to_pinecone(document, username)
        delete_file_duplicate(filename, directory)
    if filename.endswith(".pdf"):
        document = []
        type = "pdf"
        loader = PyMuPDFLoader(os.path.join(directory, filename))
        # loader = PyPDFLoader(os.path.join(directory, filename))
        document.extend(text_splitter.split_documents(loader.load()))
        length = len(document)
        upsert_to_pinecone(document, username)
        delete_file_duplicate(filename, directory)
    if filename.endswith(".md"):
        document = []
        type = "pdf"
        loader = UnstructuredMarkdownLoader(os.path.join(directory, filename))
        document.extend(text_splitter.split_documents(loader.load()))
        length = len(document)
        upsert_to_pinecone(document, username)
        delete_file_duplicate(filename, directory)
    if filename.endswith(".docx"):
        document = []
        type = "docx"
        loader = UnstructuredWordDocumentLoader(os.path.join(directory, filename))
        document.extend(text_splitter.split_documents(loader.load()))
        length = len(document)
        upsert_to_pinecone(document, username)
        delete_file_duplicate(filename, directory)
    
    # mongo insert filename
    upsert_filename = {
        "timestamp": datetime.datetime.now(datetime.UTC),
        "filename": filename,
        "user": username
    }
    mongo_latest_upload_collection.insert_one(upsert_filename)
    
    upload_file = {
        "fileId": fileId,
        "filename": filename,
        "type": type,
        "userUpload": username,
        "status": "uploaded",
        "credentialRole": role,
        "startUpdate": datetime.datetime.now(datetime.UTC),
        "latestUpdate": datetime.datetime.now(datetime.UTC)
    }
    
    mongo_fileOfUser_collection.insert_one(upload_file)
    return length