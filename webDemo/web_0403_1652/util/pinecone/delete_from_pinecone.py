from util.connector.pinecone_connector import pinecone_connect
from util.mongodb.mongo_latest_upload import mongo_drop_latest_upload_collection

# ฟังก์ชันนี้ใช้สำหรับลบข้อมูลใน Pinecone ทั้งหมดที่อยู่ใน namespace ของ user คนนั้น
def delete_namespace_pinecone(username):
    folder = username

    # Pinecone connect
    index = pinecone_connect()
    
    # ลบข้อมูลใน Pinecone ทั้งหมดที่อยู่ใน namespace นั้น
    index.delete(delete_all=True, namespace=folder)
    
    mongo_drop_latest_upload_collection(username)