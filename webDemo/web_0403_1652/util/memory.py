from langchain.memory import ConversationBufferWindowMemory

chatMemory = ConversationBufferWindowMemory(
    k=4, return_messages=True, output_key="answer", input_key="question")