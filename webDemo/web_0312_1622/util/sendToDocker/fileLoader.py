import os
from langchain_community.document_loaders import CSVLoader, PyPDFLoader, TextLoader, UnstructuredWordDocumentLoader
from util.connector.docker_connector import send_fileDocument_to_docker



def fileLoader_forSendToCreditEmbeddedChecker(userId, companyId, role, uploaded_filename_list, directory):
    documents = []
    for filename in uploaded_filename_list:
        if filename.endswith(".csv"):
            loader = CSVLoader(os.path.join(directory, filename))
            loader_dict = {}
            for key, value in loader.load()[0]:
                if key != 'type':
                    loader_dict[key] = value
            documents.append(loader_dict)
            
        elif filename.endswith(".txt"):
            loader = TextLoader(os.path.join(directory, filename))
            loader_dict = {}
            for key, value in loader.load()[0]:
                if key != 'type':
                    loader_dict[key] = value
            documents.append(loader_dict)
            
        elif filename.endswith(".pdf"):
            loader = PyPDFLoader(os.path.join(directory, filename))
            loader_dict = {}
            for key, value in loader.load()[0]:
                if key != 'type':
                    loader_dict[key] = value
            documents.append(loader_dict)
            
        elif filename.endswith(".docx"):
            loader = UnstructuredWordDocumentLoader(os.path.join(directory, filename))
            loader_dict = {}
            for key, value in loader.load()[0]:
                if key != 'type':
                    loader_dict[key] = value
            documents.append(loader_dict)
    
    
    # return documents
    send_fileDocument_to_docker(userId, companyId, role, documents)
    # print("\n",documents)
    