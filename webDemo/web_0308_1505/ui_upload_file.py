import streamlit as st
import tempfile
from pathlib import Path
from util.pinecone.upsert import file_loader
from util.pinecone.delete_from_stCheckbox import pinecone_delete_from_checkbox
from util.delete_in_local import delete_file_duplicate
from util.mongodb.mongo_latest_upload import mongo_query_all_latest_upload, mongo_delete_selected_latest_upload, mongo_find_one_latest_upload


def st_upload_file(username):
    # แสดงส่วน "Upload File"
    st.header("Upload File")
    # เอาชื่อไฟล์ทั้งหมดของ user คนนั้นที่ได้จาก mongo_query_all_latest_upload และเก็บค่าไว้ที่ filenames
    filenames = mongo_query_all_latest_upload(username)
    if filenames:
        with st.form("Files you've uploaded"):
            # อันนี้เป็นการเลือกไฟล์ที่จะลบ
            selected_files = [st.checkbox(filename) for filename in filenames]
            # ปุ่มลบไฟล์ที่เลือก
            delete_file_button = st.form_submit_button("Delete This File")
            # ถ้ากดปุ่มลบไฟล์ที่เลือก
            if delete_file_button:
                # วนลูปเช็คว่าไฟล์ไหนถูกเลือก แล้วลบไฟล์นั้นออกจาก Pinecone และ MongoDB collection latest_upload (ของ user คนนั้น)
                for filename, selected in zip(filenames, selected_files):
                    # ถ้าไฟล์ถูกเลือก
                    if selected:
                        # เรียกใช้ฟังก์ชัน ลบไฟล์ที่เลือกใน Pinecone และ MongoDB collection latest_upload (ของ user คนนั้น)
                        pinecone_delete_from_checkbox(username, filename)
                        mongo_delete_selected_latest_upload(username, filename)
                    else:
                        pass
            else:
                pass
    else:
        pass
            
    # แสดงส่วนอัปโหลดไฟล์
    with st.form("Upload File", clear_on_submit=True):
        # ช่องเลือกไฟล์ที่จะอัปโหลด และเลือกได้หลายไฟล์ โดยมีเงื่อนไขว่าไฟล์ที่อัปโหลดได้ต้องเป็นไฟล์ประเภท .pdf, .docx, .txt, .csv เท่านั้น
        uploaded_files = st.file_uploader("Please select file", type=['.pdf', '.docx', '.txt', '.csv'], accept_multiple_files=True)
        # ถ้ามีไฟล์ที่อัปโหลด
        if uploaded_files is not None:
            # จะไปเก็บไว้ใน temp directory และเก็บชื่อไฟล์ไว้ใน uploaded_filename_list
            # ซึ่ง temp directory จะถูกลบทิ้งเมื่อออกจาก with โดยอัตโนมัติ
            with tempfile.TemporaryDirectory() as tmpdirname:
                temp_dir = Path(tmpdirname)
                uploaded_filename_list = []
                # ตรงนี้เป็นการแสดงชื่อไฟล์ที่อัปโหลดลงในหน้าเว็บ
                for uploaded_file in uploaded_files:
                    st.write("Filename:", uploaded_file.name)
                    # เขียนไฟล์ที่อัปโหลดลงใน temp directory
                    with open(temp_dir / uploaded_file.name, "wb") as f:
                        f.write(uploaded_file.getbuffer())
                    # เก็บชื่อไฟล์ที่อัปโหลดไว้ใน uploaded_filename_list
                    uploaded_filename_list.append(uploaded_file.name)

                # ปุ่มอัปโหลด
                button_upload = st.form_submit_button("Upload")
                # ถ้ากดปุ่มอัปโหลด
                if button_upload:
                    # จะวนลูปเช็คว่าไฟล์ที่อัปโหลดไปแล้วมีอยู่ใน MongoDB collection latest_upload (ของ user คนนั้น) หรือยัง
                    for uploaded_filename in uploaded_filename_list:
                        # โดยเรียกใช้ฟังก์ชัน mongo_find_one_latest_upload ซึ่งจะไปดึงชื่อไฟล์ทั้งหมดที่อัปโหลดแล้วยังไม่ได้ลบของ user คนนั้น
                        filename_from_mongo = mongo_find_one_latest_upload(username, uploaded_filename)
                        # ถ้าชื่อไฟล์ที่อัปโหลดไปแล้วมีอยู่ใน MongoDB collection latest_upload (ของ user คนนั้น)
                        if uploaded_filename == filename_from_mongo:
                            # จะขึ้นเตือนว่าไฟล์นี้อัปโหลดไปแล้ว ถ้าต้องการอัปโหลดใหม่ให้ลบไฟล์เก่าออกก่อน
                            st.warning(f"{uploaded_filename} has already been uploaded. \n\n If you want to upload it, please delete the old file first.", icon="⚠️")
                            # ลบไฟล์ที่ชื่อซ้ำกับใน MongoDB collection latest_upload (ของ user คนนั้น) ออกจาก temp directory
                            delete_file_duplicate(uploaded_filename, temp_dir)
                        # แต่ถ้าชื่อไฟล์ที่อัปโหลดไปแล้วไม่มีอยู่ใน MongoDB collection latest_upload (ของ user คนนั้น)
                        else:
                            # ให้เรียกใช้ฟังก์ชัน file_loader ซึ่งจะไปอัปโหลดไฟล์ไป Pinecone และเก็บชื่อไฟล์ลงใน MongoDB collection latest_upload (ของ user คนนั้น) โดยทั้งหมดนี้จะอยู่ในฟังก์ชัน file_loader
                            file_loader(username, uploaded_filename, temp_dir)
            # reset ค่า uploaded_files เพื่อรอการอัปโหลดไฟล์ใหม่
            uploaded_files.clear()