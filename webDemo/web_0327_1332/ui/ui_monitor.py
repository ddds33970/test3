import streamlit as st
from ui.ui_upload_file import delete_file_from_checkbox

def st_monitor(userId, companyId, username, role, uploadCredits, queryCredits):
    # แสดงข้อมูลผู้ใช้
    st.header("Information")

    col1, col2 = st.columns(2)

    col1.metric(label="Upload Credits", value=str(uploadCredits))
    col2.metric(label="Query Credits", value=str(queryCredits))

    col4, col5, col6, col7 = st.columns(4)

    col4 = st.write("ID:", userId) 
    col5 = st.write("Company:", companyId) 
    col6 = st.write(f"Username:", username)
    col7 = st.write(f"Role:", role) 

    delete_file_from_checkbox(st.session_state["name"])