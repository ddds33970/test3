# new check if is doc do format_document
from langchain.schema import format_document
from langchain.prompts.prompt import PromptTemplate
import langchain_core.documents.base as document_base
import re

def is_document_class(obj):
  """Checks if the object is of type langchain_core.documents.base.Document."""
  return isinstance(obj, document_base.Document)

DEFAULT_DOCUMENT_PROMPT = PromptTemplate.from_template(template="{page_content}")

def _combine_and_remove_redundancy_documents(*args, document_prompt=DEFAULT_DOCUMENT_PROMPT,
                                             document_separator="_____________________________________________________________\n"):
    # Check if args contains only one element
    if len(args) == 1 and isinstance(args[0], list):
        args = args[0]  # Extract the single list element

    doc_strings = [format_document(doc, document_prompt) if is_document_class(doc) else doc for doc in args]

    combined_text = document_separator.join(list(set(doc_strings)))

    # Preprocess text to replace more than two consecutive newline characters with just one newline characters
    combined_text = re.sub(r'\n{2,}', '\n', combined_text)

    return combined_text