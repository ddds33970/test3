from pinecone import Pinecone
import other.settings as ENV

# Pinecone connect
def pinecone_connect():
    api_key = ENV.PINECONE_API_KEY
    pc = Pinecone(api_key=api_key)
    pinecone_index = pc.Index(ENV.PINECONE_INDEX_NAME)
    return pinecone_index