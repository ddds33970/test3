from util.connector.pinecone_connector import pinecone_connect

# ฟังก์ชันนี้ใช้สำหรับลบข้อมูลใน Pinecone โดยการลบทีละตัว
def pinecone_delete_from_checkbox(username, filename):
    # Pinecone connect
    index = pinecone_connect()
    # กำหนด prefix ของไฟล์ที่จะลบ
    prefix_for_delete = username + "_" + filename
    # ลบข้อมูลใน Pinecone ทีละตัวที่มี prefix ตามที่กำหนดไว้
    for ids in index.list(prefix=prefix_for_delete, namespace=username):
        index.delete(ids,namespace=username)