from langchain_core.prompts import ChatPromptTemplate

template_for_retrieved = """from "Chat History" and "Follow Up Input"
```
Chat History:
{chat_history}

Follow Up Input: {question}
```
What you want to search, Explain your "Search terms" and answer just "Search terms" with no need for another word in Thai and English in one line
"""
# What you want to search, Explain your search terms clearly in {language} language in one sentence.
RETRIEVED_CONDENSE_QUESTION_PROMPT = ChatPromptTemplate.from_template(template_for_retrieved)
# ควรเป็นสิ่งที่ไป search internet,retreival doc ให้ไป search ทั้ง 2 ภาษา