import streamlit as st
from util.pinecone.delete_from_pinecone import delete_namespace_pinecone
from util.mongodb.mongo_latest_upload import mongo_query_all_latest_upload

def st_delete_namespace(username):
    # เอาชื่อไฟล์ทั้งหมดของ user คนนั้นที่ได้จาก mongo_query_all_latest_upload และเก็บค่าไว้ที่ filenames
    filenames = mongo_query_all_latest_upload(username)
    if filenames:
        st.header("Delete All File on Pinecone")
        # ลบไฟล์ทั้งหมดของ user คนนั้นออกจาก Pinecone และ ลบชื่อไฟล์ทั้งหมดของ user คนนั้นออกจาก mongodb ซึ่งส่วนที่ลบใน mongodb จะทำในฟังก์ชัน delete_namespace_pinecone แล้ว
        st.button('Delete All File on Pinecone', on_click=delete_namespace_pinecone, args=(username,))
    else:
        pass