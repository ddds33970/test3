from util.connector.mongodb_connector import mongo_connect_users_collection_nlp
# Connect to the MongoDB database

try:
    # Create a new client and connect to the server
    # mongo connect "user_data" collection
    mongo_users_collection_nlp = mongo_connect_users_collection_nlp()
except Exception as e:
    print(e)
