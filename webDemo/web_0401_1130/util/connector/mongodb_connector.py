import other.settings as ENV
from pymongo.mongo_client import MongoClient

# mongo connect "latest_upload" collection
def mongo_connect_latest_upload_collection(username):
    client = MongoClient(ENV.MONGODB_URI_NLP)
    db = client[ENV.MONGODB_LATESTUPLOADFILE_DATABASE_NAME_NLP]
    latestUploadCollection = "latest_upload_" + str(username)
    mongo_latest_upload_collection = db[latestUploadCollection]
    return mongo_latest_upload_collection

# mongo connect "user_data" collection
def mongo_connect_users_collection_nlp():
    client = MongoClient(ENV.MONGODB_URI_NLP)
    db = client[ENV.MONGODB_USER_DATABASE_NAME_NLP]
    mongo_users_collection_nlp = db[ENV.MONGODB_USER_COLLECTION_NAME_NLP]
    return mongo_users_collection_nlp

# mongo connect "user_cookie" collection
def mongo_connect_usercookie_collection_nlp():
    client = MongoClient(ENV.MONGODB_URI_NLP)
    db = client[ENV.MONGODB_USER_DATABASE_NAME_NLP]
    mongo_usercookie_collection_nlp = db[ENV.MONGODB_USERCOOKIE_COLLECTION_NAME_NLP]
    return mongo_usercookie_collection_nlp

def mongo_connect_logslogin_collection_nlp():
    client = MongoClient(ENV.MONGODB_URI_NLP)
    db = client[ENV.MONGODB_LOG_DATABASE_NAME_NLP]
    mongo_logslogin_collection_nlp = db[ENV.MONGODB_LOGSLOGIN_COLLECTION_NAME_NLP]
    return mongo_logslogin_collection_nlp

# mongo connect "fileOfUser" collection
def mongo_connect_fileOfUser_collection(username):
    client = MongoClient(ENV.MONGODB_URI_NLP)
    db = client[ENV.MONGODB_USERFILE_DATABASE_NAME_NLP]
    fileOfUserCollection = "file_of_" + str(username)
    mongo_fileOfUser_collection = db[fileOfUserCollection]
    return mongo_fileOfUser_collection