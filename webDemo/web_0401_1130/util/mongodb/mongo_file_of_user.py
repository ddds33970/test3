from util.connector.mongodb_connector import mongo_connect_fileOfUser_collection

def mongo_drop_fileOfUser_collection(username):
    # mongo connect "fileOfUser" collection ของ user คนนั้น
    mongo_fileOfUser_collection = mongo_connect_fileOfUser_collection(username)
    # ลบ collection นั้นทิ้งไป
    mongo_fileOfUser_collection.drop()