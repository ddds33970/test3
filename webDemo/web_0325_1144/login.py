import streamlit as st
from ui.ui_chat import st_chat
from ui.ui_upload_file import st_upload_file
from ui.ui_delete_file import st_delete_namespace
from util.connector.mongodb_connector import  mongo_connect_usercookie_collection_nlp
from util.mongodb.mongo_logs_login import mongo_insert_logs_login_fail, mongo_insert_logs_login_success
from util.mongodb.mongo_userdata import mongo_find_one_userdata
from streamlit_authenticator_mongo.db import mongo_users_collection_nlp
from streamlit_authenticator_mongo.authenticate import Authenticate
from ui.ui_monitor import st_monitor

mongo_usercookie_collection_nlp = mongo_connect_usercookie_collection_nlp()

config_document = mongo_usercookie_collection_nlp.find_one({})
if config_document is None:
    raise Exception("Configuration document not found in MongoDB")


authenticator = Authenticate(
        mongo_users_collection_nlp,
        config_document['cookie']['name'], 
        config_document['cookie']['key'], 
        config_document['cookie']['expiry_days'],
    )

userId, companyId, role, password, uploadCredits, queryCredits, uploadQuota, queryQuota = mongo_find_one_userdata(st.session_state["name"])

login_menu = st.sidebar.selectbox(
    'Selected Page:',
    ('Login', 'Register','Forgot Password', 'Forgot Username') if not st.session_state["authentication_status"] else ('Login', 'Reset Password', 'Update info','Upload & Delete', 'Chat', 'Monitor')
)

if login_menu == 'Login':
    authenticator.login('Login', 'main')
    if st.session_state["authentication_status"]:
        
        st.sidebar.write(f"Logged in as: *{st.session_state['name']}*")
        authenticator.logout('Logout', 'sidebar', key='unique_key')
        authenticator.delete_user(st.session_state["email"], 'Delete this user', 'sidebar')

        st.title("Home Page")
        st.write("Welcome to the Home Page of our web application!")
        st.write("Under Development...")
        
        mongo_insert_logs_login_success(userId, st.session_state['name'], password)
                
    elif st.session_state["authentication_status"] is False:
        userId, companyId, role, password, _, _, _ = mongo_find_one_userdata(st.session_state["name"])
        mongo_insert_logs_login_fail(userId, st.session_state["name"], password)
        st.error('Username/password is incorrect')
    elif st.session_state["authentication_status"] is None:
        st.warning('Please enter your username and password')

elif login_menu == 'Register':
        try:
            if authenticator.register_user('Register user'):
                st.success('User registered successfully')
            else:
                st.warning("Please fill in both the username and password fields.")
        except Exception as e:
            st.error(f'Error: {e}')
            
elif login_menu == 'Forgot Password':
    try:
        username_of_forgotten_password, email_of_forgotten_password, new_random_password = authenticator.forgot_password('Forgot password')
        if username_of_forgotten_password:
            st.success(f'A new password has been generated and sent securely to your email. Your new password is: {new_random_password}')
        else:
            st.warning('Please enter your Email')
    except Exception as e:
        st.error(e)
        
elif login_menu == 'Reset Password':
    if st.session_state["authentication_status"]:
        st.sidebar.write(f"Logged in as: *{st.session_state['name']}*")
        authenticator.logout('Logout', 'sidebar', key='unique_key')
        authenticator.delete_user(st.session_state["email"], 'Delete this user', 'sidebar')
        try:
            if authenticator.reset_password(st.session_state["email"], 'Reset password'):
                st.success('Password modified successfully')
        except Exception as e:
            st.error(e)

elif login_menu == 'Update info':
    if st.session_state["authentication_status"]:
        st.sidebar.write(f"Logged in as: *{st.session_state['name']}*")
        authenticator.logout('Logout', 'sidebar', key='unique_key')
        authenticator.delete_user(st.session_state["email"], 'Delete this user', 'sidebar')
        try:
            if authenticator.update_user_details(st.session_state["name"],'Update information','main'):
                st.success('You can check it in your email')
        except Exception as e:
            st.error(e)

elif login_menu == 'Forgot Username':
    try:
        username_forgot_username, email_forgot_username = authenticator.forgot_username('Forgot username')
        if username_forgot_username:
            st.success(f'A username has been generated and sent securely to your email. Your username is: {username_forgot_username}')
        else:
            st.warning('Please enter your Email')
    except Exception as e:
        st.error(e)

elif login_menu == 'Upload & Delete':
    st.sidebar.write(f"Logged in as: *{st.session_state['name']}*")
    authenticator.logout('Logout', 'sidebar', key='unique_key')
    authenticator.delete_user(st.session_state["email"], 'Delete this user', 'sidebar')
    st_upload_file(st.session_state["name"], userId, companyId, role)
    st_delete_namespace(st.session_state["name"])
            
elif login_menu == 'Chat':
    st.sidebar.write(f"Logged in as: *{st.session_state['name']}*")
    authenticator.logout('Logout', 'sidebar', key='unique_key')
    authenticator.delete_user(st.session_state["email"], 'Delete this user', 'sidebar')
    st_chat(st.session_state["name"], userId, companyId, role)

elif login_menu == 'Monitor':
    st.sidebar.write(f"Logged in as: *{st.session_state['name']}*")
    authenticator.logout('Logout', 'sidebar', key='unique_key')
    authenticator.delete_user(st.session_state["email"], 'Delete this user', 'sidebar')
    st_monitor(userId, companyId, st.session_state["name"], role, uploadCredits, queryCredits)


