from langchain_community.vectorstores import Pinecone
# from langchain_openai import OpenAIEmbeddings
from langchain.embeddings import CohereEmbeddings
import other.settings as ENV
from langchain_community.embeddings import CohereEmbeddings

# ฟังก์ชันนี้ใช้สำหรับเรียก retriever ของ Pinecone ขึ้นมาเพื่อใช้ในการค้นหาข้อมูล
def doc_search_and_retriever(username, kFromUser):
    
    folder = username
    # เชื่อมกับ index ของ Pinecone ที่เราสร้างไว้
    index_name = ENV.PINECONE_INDEX_NAME
    # model ที่ใช้ในการ embed ข้อมูลเพื่อเป็นการค้นหา
    # embeddings = CohereEmbeddings(cohere_api_key=ENV.COHERE_API_KEY,
    #                                                   model='embed-multilingual-v3.0')
    embeddings = CohereEmbeddings(model="embed-multilingual-v3.0")
    # กำหนดให้หาที่ namespace ของ user คนนั้น โดยใช้ คำถามที่ user พิมพ์เข้ามา ไป embed ก่อน
    docsearch = Pinecone.from_existing_index(index_name, embeddings, namespace=folder)
    # สร้าง retriever ขึ้นมาเพื่อใช้ในการค้นหาข้อมูล
    retriever_pinecone = docsearch.as_retriever(
        # search_type="similarity_score_threshold",
        # search_kwargs={'score_threshold': 0.9}
        
        # เราสามารถกำหนดค่า k ได้ตามต้องการ แต่ในที่นี้เรากำหนดไว้เป็น 3 หมายถึงเราจะเอาข้อมูลที่คล้ายกันที่สุด 3 ตัว
        search_kwargs={"k": kFromUser}
    )
    # print("retriever_pinecone: ", retriever_pinecone)
    # ส่ง ข้อมูลที่เราได้จากการค้นหาข้อมูลไปให้ฟังก์ชันเรียกใช้ฟังก์ชันนี้
    return retriever_pinecone

