from util.connector.mongodb_connector import mongo_connect_latest_upload_collection

def mongo_insert_latest_upload(username_new, fileName):
    # mongo connect "latest_upload" collection
    mongo_latest_upload_collection = mongo_connect_latest_upload_collection(username_new)

    mongo_latest_upload_collection.insert_one({"fileName": fileName})
    print(f"\n insert {fileName} \n")

def mongo_find_one_latest_upload(username_new, uploaded_filename):
    # mongo connect "latest_upload" collection()
    mongo_latest_upload_collection = mongo_connect_latest_upload_collection(username_new)

    document = mongo_latest_upload_collection.find_one({"filename": uploaded_filename})
    
    if document is not None:
        filename_from_mongo = document["filename"]
        return filename_from_mongo
    else:
        filename_from_mongo = None
        return filename_from_mongo

def mongo_query_all_latest_upload(username_new):
    # mongo connect "latest_upload" collection()
    mongo_latest_upload_collection = mongo_connect_latest_upload_collection(username_new)
    # เก็บข้อมูลที่ได้จากการ query ลงใน list
    filenames_list = [document["filename"] for document in mongo_latest_upload_collection.find({})]
    # แปลง list เป็น set เพื่อลบข้อมูลที่ซ้ำกัน
    filenames = set(filenames_list)
    # เรียงชื่อไฟล์ตามตัวอักษร
    sorted_filenames = sorted(list(filenames))
    return sorted_filenames

def mongo_delete_selected_latest_upload(username_new, filename):
    # mongo connect "latest_upload" collection()
    mongo_latest_upload_collection = mongo_connect_latest_upload_collection(username_new)

    document = mongo_latest_upload_collection.find_one({"filename": filename})
    mongo_latest_upload_collection.delete_one(document)

def mongo_delete_all_latest_upload(username_new):
    # mongo connect "latest_upload" collection()
    mongo_latest_upload_collection = mongo_connect_latest_upload_collection(username_new)

    mongo_latest_upload_collection.delete_many({})