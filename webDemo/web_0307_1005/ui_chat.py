import streamlit as st
from util.askChat_reRank import ask_reRank
from util.askChat_no_reRank import ask_no_reRank
from util.memory import memory
# ส่วน chat
def st_chat(name_new):
    
    st.title("Echo Bot")
    
    on = st.toggle('"re-rank" feature')

    clear_memory_button = st.button("Clear memory")
    if clear_memory_button:
        memory.clear()
    else:
        pass

    # Initialize chat history
    if "messages" not in st.session_state:
        st.session_state.messages = []

    # Display chat messages from history on app rerun
    for message in st.session_state.messages:
        with st.chat_message(message["role"]):
            st.markdown(message["content"])

    # React to user input
    if question := st.chat_input("What is up?"):
        # Display user message in chat message container
        st.chat_message("user").markdown(question)
        # Add user message to chat history
        st.session_state.messages.append({"role": "user", "content": question})
        
        if on:
            result = ask_reRank(question, name_new)
        else:
            result = ask_no_reRank(question, name_new)

        response = f"Echo: {result['answer'].content}"
        
        memory.save_context({"question": question}, {"answer": result['answer'].content}) # save memory

        # print(memory.load_memory_variables({}))
        # Display assistant response in chat message container
        with st.chat_message("assistant"):
            st.markdown(response)
        # Add assistant response to chat history
        st.session_state.messages.append({"role": "assistant", "content": response})
