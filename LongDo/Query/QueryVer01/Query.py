# QueryVer01/Query.py

import settings as ENV
from langchain_community.vectorstores import Pinecone
from langchain_openai import OpenAIEmbeddings



embeddings = OpenAIEmbeddings()
# embeddings = embeddings_model.embed_documents()


index_name = ENV.PINECONE_INDEX_NAME


query = "IT Center will not open classrooms"
docsearch = Pinecone.from_existing_index(index_name, embeddings)
# docsearch = Pinecone(index_name=index_name, embedding_dim=len(embeddings[0]))

docs = docsearch.similarity_search(query)

# แสดงเนื้อหาของเอกสารแรกที่ค้นพบ
print(docs[0].page_content)

# ขาด memory