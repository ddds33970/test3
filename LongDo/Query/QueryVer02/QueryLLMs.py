# QueryVer02/Query.py

import settings as ENV
from langchain_community.vectorstores import Pinecone
from langchain_openai import OpenAIEmbeddings
from langchain.chains.question_answering import load_qa_chain
from langchain.llms import OpenAI



embeddings = OpenAIEmbeddings()
# embeddings = embeddings_model.embed_documents()


index_name = ENV.PINECONE_INDEX_NAME


query = "What are the Registrar's procedures in response to a reservation request, and how are they communicated to the requester?"
docsearch = Pinecone.from_existing_index(index_name, embeddings)
# docsearch = Pinecone(index_name=index_name, embedding_dim=len(embeddings[0]))
namespace = "BBB"
docs = docsearch.similarity_search(query=query, namespace=namespace)

llm = OpenAI()
chain = load_qa_chain(llm, chain_type="stuff")
response = chain.run(input_documents=docs, question=query)

new_query = query + response
new_response = llm.predict(new_query)

print(new_response)
