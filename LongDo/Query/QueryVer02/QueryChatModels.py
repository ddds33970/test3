# QueryVer02/QueryChatModels.py

from langchain.chat_models import ChatOpenAI
from langchain.schema import SystemMessage, HumanMessage, AIMessage
from langchain.prompts import PromptTemplate
from langchain.chains import LLMChain
import settings as ENV
from langchain_community.vectorstores import Pinecone
from langchain_openai import OpenAIEmbeddings

embeddings = OpenAIEmbeddings()

index_name = ENV.PINECONE_INDEX_NAME

namespace = "BBB"

query = "What are the Registrar's procedures in response to a reservation request, and how are they communicated to the requester?"
docsearch = Pinecone.from_existing_index(index_name, embeddings)

docs = docsearch.similarity_search(query=query, namespace=namespace)
print(docs[0].page_content)
# print(docs[0].page_content)

llm = ChatOpenAI(model_name = "gpt-3.5-turbo", temperature = 0.7)

messages = [SystemMessage(content = "I would like you to answer questions about the article I will send you from {docs}."),
            HumanMessage(content = "Hi, I would like to know how to reserve a classroom. Where do I send the email?"),
            AIMessage(content = "To reserve a classroom please send email to scheduling.registrar@lehman.cuny.edu.  Attach to the email request, a completed electronic copy of the Computer Lab Request Form (insert link) in Microsoft Word format.  Instructors may schedule long-term or one day sessions at the I.T. Center.  If it is not practical to attach the completed form to the email, please include all the relevant information in the email message: \n 1. Discipline, course code, section and 4-digit registration number \n 2. Days and times \n 3. Instructor name \n 4. Contact information \n 5. Required software \n 6. Particular room requested (optional)"),
            HumanMessage(content = query)
            ]

res = llm(messages)
print(res)