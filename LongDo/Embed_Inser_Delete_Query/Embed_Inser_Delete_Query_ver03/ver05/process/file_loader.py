# ver05/process/embeddingcode.py

import os
import glob
from langchain_community.document_loaders import DirectoryLoader, UnstructuredWordDocumentLoader
from langchain.text_splitter import RecursiveCharacterTextSplitter
from langchain.embeddings.openai import OpenAIEmbeddings
from langchain_community.document_loaders import CSVLoader

# กลับไปใช้ OPENAI แทน

def file_loader():
    documents = []
    embeddings_model = OpenAIEmbeddings()

    folder_path = "/Users/j.papontee/Library/CloudStorage/GoogleDrive-st32673@kn.ac.th/ไดรฟ์ของฉัน/PIM/Botnoi/LongDo/Embed_Inser_Delete_Query/Embed_Inser_Delete_Query_ver03/Test_file"

    # ทำการโหลดไฟล์ .docx
    docx_files = DirectoryLoader(folder_path, glob="./*.docx", loader_cls=UnstructuredWordDocumentLoader).load()

    text_splitter = RecursiveCharacterTextSplitter(chunk_size=1000, chunk_overlap=200)
    docx_documents = text_splitter.split_documents(docx_files)
    documents.extend(docx_documents)

    # ทำการโหลดไฟล์ .csv
    csv_files = glob.glob(os.path.join(folder_path, '*.csv'))

    for csv_file in csv_files:
        csv_loader = CSVLoader(file_path=csv_file)
        csv_documents = csv_loader.load()

        text_splitter = RecursiveCharacterTextSplitter(chunk_size=1000, chunk_overlap=200)
        csv_documents = text_splitter.split_documents(csv_documents)
        documents.extend(csv_documents)

    return embeddings_model, documents

