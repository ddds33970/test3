# ver04/process/AskChatModels.py

import settings as ENV
from ver05.user_input import ask_chatbot_json
from langchain.chat_models import ChatOpenAI
from langchain.schema import SystemMessage, HumanMessage, AIMessage
from langchain.prompts import PromptTemplate
from langchain.chains import LLMChain
from langchain_community.vectorstores import Pinecone
from langchain_openai import OpenAIEmbeddings
from pymongo import MongoClient
import datetime
import os

# query = "What is the designated email address for reserving a classroom?"
# def query_pinecone(query):
#     api_key = ENV.PINECONE_API_KEY
#     pc = Pinecone(api_key=api_key)
#     index = pc.Index(ENV.PINECONE_INDEX_NAME)
#     embeddings = OpenAIEmbeddings()
#     # embeddings = embeddings_model.embed_documents()

#     # query = "What is the designated email address for reserving a classroom?"
#     docsearch = Pinecone.from_existing_index(index, embeddings)
#     # docsearch = Pinecone(index_name=index_name, embedding_dim=len(embeddings[0]))

#     docs = docsearch.similarity_search(query)

#     # llm = OpenAI()
#     # chain = load_qa_chain(llm, chain_type="stuff")
#     # response = chain.run(input_documents=docs, question=query)

#     print(docs)
    
def query_and_ask(request):
    
    embeddings = OpenAIEmbeddings()

    index_name = ENV.PINECONE_INDEX_NAME
    
    question, folder = ask_chatbot_json(request)
    
    namespace = folder
    
    # MongoDB connect
    client = MongoClient(os.getenv("MONGODB_URI"))
    db = client[os.getenv("MONGODB_DATABASE_NAME")]
    collection = db[os.getenv("MONGODB_COLLECTION_NAME")]

    docsearch = Pinecone.from_existing_index(index_name, embeddings)
    docs = docsearch.similarity_search(query=question, namespace=namespace)
    
    llm = ChatOpenAI(model_name = "gpt-3.5-turbo", temperature = 0.7)

    messages = [SystemMessage(content = "I would like you to answer questions about the article I will send you from {docs}."),
                HumanMessage(content = "Hi, I would like to know how to reserve a classroom. Where do I send the email?"),
                AIMessage(content = "To reserve a classroom please send email to scheduling.registrar@lehman.cuny.edu.  Attach to the email request, a completed electronic copy of the Computer Lab Request Form (insert link) in Microsoft Word format.  Instructors may schedule long-term or one day sessions at the I.T. Center.  If it is not practical to attach the completed form to the email, please include all the relevant information in the email message: \n 1. Discipline, course code, section and 4-digit registration number \n 2. Days and times \n 3. Instructor name \n 4. Contact information \n 5. Required software \n 6. Particular room requested (optional)"),
                HumanMessage(content = question)
                ]
    
    res = llm(messages)
    
    # กำหนดข้อมูลที่จะ insert ไปใน MongoDB
    log_upsert_data = {
        "timestamp": datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
        "action": f"ask question ({question}) \n AI answer: {res}"
    }
    # insert log ขึ้นใน MongoDB
    collection.insert_one(log_upsert_data)
    
    print(f"\n\n\ninsert log completed.\n")
    print(f"Log inserted: {log_upsert_data}\n")