# ver05/process/embeddingcode.py

import os
import glob
from langchain_community.document_loaders import DirectoryLoader, UnstructuredWordDocumentLoader
from langchain.text_splitter import RecursiveCharacterTextSplitter
from langchain.embeddings.openai import OpenAIEmbeddings
from langchain_community.document_loaders import CSVLoader

# กลับไปใช้ OPENAI แทน

def file_loader_def_docx():
    # แก้ path เป็นที่อยู่ของไฟล์
    # อันนี้ใช้ .docx
    loader = DirectoryLoader('/Users/j.papontee/Library/CloudStorage/GoogleDrive-st32673@kn.ac.th/ไดรฟ์ของฉัน/PIM/Botnoi/LongDo/EmbedAndInsertDoc/EmbedAndInsertDocVer05/Test_file', glob="./*.docx", loader_cls=UnstructuredWordDocumentLoader)
    documents = loader.load()
    # split documents into chunks of 1000 characters with 200 character overlap
    text_splitter = RecursiveCharacterTextSplitter(chunk_size=1000, chunk_overlap=200)
    documents = text_splitter.split_documents(documents)
    
    os.environ["OPENAI_API_KEY"] = os.getenv("OPENAI_API_KEY")
    # embeddings_data = CohereEmbeddings(cohere_api_key=os.getenv("COHERE_API_KEY"), model='embed-multilingual-v3.0', truncate = None)
    embeddings_model = OpenAIEmbeddings()
    # embeddings = []
    # for document in documents:
    #     document_embeddings = embeddings_data.embed([document.page_content], input_type=[str])
    #     embeddings.append(document_embeddings)
        
    return embeddings_model, documents

def file_loader_def_csv():
    # รายที่ตั้งของโฟลเดอร์ที่มีไฟล์ .csv
    folder_path = "/Users/j.papontee/Library/CloudStorage/GoogleDrive-st32673@kn.ac.th/ไดรฟ์ของฉัน/PIM/Botnoi/LongDo/Embed_Inser_Delete_Query_ver02/Test_file"
    # ใช้ glob เพื่อดึงรายชื่อไฟล์ .csv ทั้งหมดในโฟลเดอร์
    csv_files = glob.glob(os.path.join(folder_path, '*.csv'))
    
    documents = []
    
    for file_path in csv_files:
        # ใช้ .csv loader
        loader = CSVLoader(file_path=file_path)
        loaded_documents = loader.load()
        
        # แบ่งเอกสารเป็นชิ้นย่อยๆ
        text_splitter = RecursiveCharacterTextSplitter(chunk_size=1000, chunk_overlap=200)
        split_documents = text_splitter.split_documents(loaded_documents)
        
        documents.extend(split_documents)
    
    os.environ["OPENAI_API_KEY"] = os.getenv("OPENAI_API_KEY")
    embeddings_model = OpenAIEmbeddings()
       
    return embeddings_model, documents

# def file_loader_def_csv():
#     # อันนี้ใช้ .csv
#     loader = CSVLoader(file_path="/Users/j.papontee/Library/CloudStorage/GoogleDrive-st32673@kn.ac.th/ไดรฟ์ของฉัน/PIM/Botnoi/LongDo/EmbedAndInsertDoc/EmbedAndInsertDocVer05/Test_file/CHAT_GPT_DEMO_KSIM_-_1_-_CHAT_GPT_DEMO_KSIM_-_1.csv.csv")
#     documents = loader.load()
#     # split documents into chunks of 1000 characters with 200 character overlap
#     text_splitter = RecursiveCharacterTextSplitter(chunk_size=1000, chunk_overlap=200)
#     documents = text_splitter.split_documents(documents)
    
#     os.environ["OPENAI_API_KEY"] = os.getenv("OPENAI_API_KEY")
#     embeddings_model = OpenAIEmbeddings()
       
#     return embeddings_model, documents
