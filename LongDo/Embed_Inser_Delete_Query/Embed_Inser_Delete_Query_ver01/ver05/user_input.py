# ver04/user_input.py

#  JSON ไว้ลองเล่น
# {
#     "userId": "userId01",
#     "companyId": "companyId01",
#     "role": "Dev",
#     "folder": "Math"
    
# }

import json


def process_upsert_delete_json(request):
    # รับ JSON จาก Postman
    json_data = request.get_json()

    # ตรวจสอบว่ามีข้อมูลที่ต้องการหรือไม่
    if not json_data:
        return 'ไม่พบข้อมูล JSON', 400

    try:
        # แปลง JSON เป็น list โดยตรง
        data_list = list(json_data.values())

        # ตรวจสอบความสมบูรณ์ของข้อมูล JSON
        required_keys = ['userId', 'companyId', 'role', 'folder']
        for key in required_keys:
            if key not in json_data:
                return f'ไม่พบคีย์ {key} ในข้อมูล JSON', 400

        return data_list

    except KeyError as e:
        # หากไม่พบ key ที่ต้องการใน JSON
        return f'ไม่พบ key {e} ใน JSON', 400
    
def ask_chatbot_json(request):
    # รับ JSON จาก Postman
    json_data = request.get_json()

    # ตรวจสอบว่ามีข้อมูลที่ต้องการหรือไม่
    if not json_data:
        return 'ไม่พบข้อมูล JSON', 400

    try:
        # แปลง JSON เป็น list โดยตรง
        data_list = list(json_data.values())

        # ตรวจสอบความสมบูรณ์ของข้อมูล JSON
        required_keys = ['question', 'folder']
        for key in required_keys:
            if key not in json_data:
                return f'ไม่พบคีย์ {key} ในข้อมูล JSON', 400

        return data_list

    except KeyError as e:
        # หากไม่พบ key ที่ต้องการใน JSON
        return f'ไม่พบ key {e} ใน JSON', 400