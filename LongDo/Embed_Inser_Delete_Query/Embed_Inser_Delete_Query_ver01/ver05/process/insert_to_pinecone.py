# ver05/process/insert_to_pinecone.py
from pinecone import Pinecone
import settings as ENV
from ver05.user_input import process_json
from ver05.process.file_loader import file_loader_def_docx, file_loader_def_csv
from pymongo import MongoClient
import datetime
import os

def docx_upsert_to_pinecone(request):
    # Pinecone connect
    api_key = ENV.PINECONE_API_KEY
    pc = Pinecone(api_key=api_key)
    index = pc.Index(ENV.PINECONE_INDEX_NAME)
    
    # MongoDB connect
    client = MongoClient(os.getenv("MONGODB_URI"))
    db = client[os.getenv("MONGODB_DATABASE_NAME")]
    collection = db[os.getenv("MONGODB_COLLECTION_NAME")]
    
    # ตรงนี้ใช้สร้าง index ใหม่ ถ้าไม่มี index นี้อยู่ใน Pinecone
    # if index not in pc.list_indexes():
    #     # create a new index
    #     pc.create_index(
    #         name=index,
    #         metric='cosine',
    #         dimension = 1536
    # )
    
    # ดึงค่ามาจาก file_loader_def ใน ver05/process/embeddingcode.py
    embeddings_model, documents = file_loader_def_docx()
    
    # loop ในการ upsert ข้อมูลไปใน Pinecone และ insert log ลงใน MongoDB
    for i in range(len(documents)):
        # ดึงค่ามาจาก file_loader_def ทีละตัว
        page_content = documents[i].page_content
        fileId = documents[i].metadata['source'].split('/')[-1]
        
        # ดึงค่าที่ input มาจาก user ในรูปแบบ json จากฟังก์ชัน process_json ในไฟล์ ver05/user_input.py 
        userId, companyId, role, folder = process_json(request)
        
        # สร้าง item_id เพื่อกำหนด id ของ document ที่จะ upsert ไปใน Pinecone
        item_id = f"{userId}_{fileId}_doc_{i+1}"
        # สร้าง item_data เพื่อกำหนด metadata ของ document ที่จะ upsert ไปใน Pinecone
        item_data = {
            "fileId": fileId,
            "userId": userId,
            "companyId": companyId,
            "role": role,
            "text": page_content
        }
        
        # เรียกใช้ฟังก์ชัน embed_documents ใน ver05/process/embeddingcode.py เพื่อแปลง text ให้เป็น vector ทีละตัว
        embeddings_values = embeddings_model.embed_documents(documents[i].page_content)
        
        # upsert ข้อมูลไปใน Pinecone
        index.upsert(
            vectors=[
                {
                    "id": item_id,
                    # upsert ข้อมูลไปใน Pinecone ในรูปแบบ vector ทีละตัว โดยเริ่มที่ชุดที่ 0
                    "values": embeddings_values[0][:],
                    "metadata": item_data
                }
            ],
            namespace=folder
        )
        
        # กำหนดข้อมูลที่จะ insert ไปใน MongoDB
        log_upsert_data = {
            "timestamp": datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            "action": f"add documents ({item_id}) at {folder} by {userId} ({role})"
        }
        
        # insert log ขึ้นใน MongoDB
        collection.insert_one(log_upsert_data)
        
        print(f"\n\n\ninsert log completed.\n")
        print(f"Log inserted: {log_upsert_data}\n")
        
        print(f"Data loaded into Pinecone successfully.\n")
        print(fileId, "\n" + f"page_content:", page_content)
      
def csv_upsert_to_pinecone(request):
    # Pinecone connect
    api_key = ENV.PINECONE_API_KEY
    pc = Pinecone(api_key=api_key)
    index = pc.Index(ENV.PINECONE_INDEX_NAME)
    
    # MongoDB connect
    client = MongoClient(os.getenv("MONGODB_URI"))
    db = client[os.getenv("MONGODB_DATABASE_NAME")]
    collection = db[os.getenv("MONGODB_COLLECTION_NAME")]
    
    # ตรงนี้ใช้สร้าง index ใหม่ ถ้าไม่มี index นี้อยู่ใน Pinecone
    # if index not in pc.list_indexes():
    #     # create a new index
    #     pc.create_index(
    #         name=index,
    #         metric='cosine',
    #         dimension = 1536
    # )
    
    # ดึงค่ามาจาก file_loader_def ใน ver05/process/embeddingcode.py
    embeddings_model, documents = file_loader_def_csv()
    
    # loop ในการ upsert ข้อมูลไปใน Pinecone และ insert log ลงใน MongoDB
    for i in range(len(documents)):
        # ดึงค่ามาจาก file_loader_def ทีละตัว
        page_content = documents[i].page_content
        fileId = documents[i].metadata['source'].split('/')[-1]
        
        # ดึงค่าที่ input มาจาก user ในรูปแบบ json จากฟังก์ชัน process_json ในไฟล์ ver05/user_input.py 
        userId, companyId, role, folder = process_json(request)
        
        # สร้าง item_id เพื่อกำหนด id ของ document ที่จะ upsert ไปใน Pinecone
        item_id = f"{userId}_{fileId}_doc_{i+1}"
        # สร้าง item_data เพื่อกำหนด metadata ของ document ที่จะ upsert ไปใน Pinecone
        item_data = {
            "fileId": fileId,
            "userId": userId,
            "companyId": companyId,
            "role": role,
            "text": page_content
        }
        
        # เรียกใช้ฟังก์ชัน embed_documents ใน ver05/process/embeddingcode.py เพื่อแปลง text ให้เป็น vector ทีละตัว
        embeddings_values = embeddings_model.embed_documents(documents[i].page_content)
        
        # upsert ข้อมูลไปใน Pinecone
        index.upsert(
            vectors=[
                {
                    "id": item_id,
                    # upsert ข้อมูลไปใน Pinecone ในรูปแบบ vector ทีละตัว โดยเริ่มที่ชุดที่ 0
                    "values": embeddings_values[0][:],
                    "metadata": item_data
                }
            ],
            namespace=folder
        )
        
        # กำหนดข้อมูลที่จะ insert ไปใน MongoDB
        log_upsert_data = {
            "timestamp": datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            "action": f"add documents ({item_id}) at {folder} by {userId} ({role})"
        }
        
        # insert log ขึ้นใน MongoDB
        collection.insert_one(log_upsert_data)
        
        print(f"\n\n\ninsert log completed.\n")
        print(f"Log inserted: {log_upsert_data}\n")
        
        print(f"Data loaded into Pinecone successfully.\n")
        print(fileId, "\n" + f"page_content:", page_content)
      
  