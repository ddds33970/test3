# requirements.txt

# python=3.8
fastapi
uvicorn[standard]
Flask==3.0.1
pymongo==4.6.1
pinecone-client==3.0.0
requests==2.31.0
pydantic==2.5.3
dotenv
# embeddings
pandas
langchain==0.1.2
langchain-openai==0.0.3
openai==1.9.0
tiktoken==0.5.2
chromadb==0.4.22
sentence_transformers==2.1.0
cohere==4.44
python-docx==1.1.0
pypdf==4.0.0
langchain-community==0.0.15
# pinecone-client==2.2.4 
tqdm==4.66.1
pymupdf==1.19.0
# requests
unstructured
onnxruntime==1.14.1
unstructured
python-docx
lxml==5.1.0
tiktoken