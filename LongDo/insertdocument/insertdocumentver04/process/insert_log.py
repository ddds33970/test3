# process/insert_log.py

import datetime
import os
import pymongo
from dotenv import load_dotenv
load_dotenv()
from pymongo import MongoClient
from user_input import process_json


def insert_log_to_mongodb(request):
    
    client = MongoClient(os.getenv("MONGODB_URI"))
    db = client[os.getenv("MONGODB_DATABASE_NAME")]
    collection = db[os.getenv("MONGODB_COLLECTION_NAME")]
    
    log_data = process_json(request)
    
    log = {
        "timestamp": datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
        "action": f"add documents ({log_data[0]}) by {log_data[1]} ({log_data[3]})"
        # log_data[0] = fileId, log_data[1] = userId, log_data[2] = companyId, log_data[3] = role, log_data[4] = documents
    }
    
    collection.insert_one(log)
    print(f"insert log completed.")
    print(f"Log inserted: {log}")
