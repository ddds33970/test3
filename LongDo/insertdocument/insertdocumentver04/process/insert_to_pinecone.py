# process/insert_to_pinecone.py
from pinecone import Pinecone
import settings as ENV
from user_input import process_json

def upsert_to_pinecone(request):
    api_key = ENV.PINECONE_API_KEY
    pc = Pinecone(api_key=api_key)
    index = pc.Index(ENV.PINECONE_INDEX_NAME)

    fileId, userId, companyId, role, documents = process_json(request)

    item_id = f"{fileId}_{userId}"
    item_data = {
        "fileId": fileId,
        "userId": userId,
        "companyId": companyId,
        "role": role,
    }
    
    index.upsert(
        vectors=[
            {
                "id": item_id,
                "values": documents,
                "metadata": item_data
            }
        ]
    )
    print(f"Data loaded into Pinecone successfully.")
