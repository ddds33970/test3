# main.py

from flask import Flask, request
from process.insert_log import insert_log_to_mongodb
from process.insert_to_pinecone import upsert_to_pinecone
from user_input import process_json
import uvicorn
from dotenv import load_dotenv
import settings as ENV

app = Flask(__name__)

@app.route('/upsert', methods=['POST'])
def main():
    upsert_to_pinecone(request)
    insert_log_to_mongodb(request)
    return {"message": "upsert complete"}

if __name__ == '__main__':
    app.run(debug=True, port=ENV.PORT)