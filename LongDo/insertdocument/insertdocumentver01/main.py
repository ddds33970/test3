# main.py
from flask import Flask, request, jsonify
from input import InputData
from process.insert_documents import insert_documents
from mongo_connector import get_user_data

app = Flask(__name__)

# root path
@app.route('/')
def home():
    return 'Welcome to the main page!'

@app.route('/insert_documents', methods=['POST'])
def insert_documents_api():
    try:
        # รับ JSON จาก request
        request_data = request.get_json()

        # สร้าง instance ของ InputData จากข้อมูลที่รับเข้ามา
        user_data = InputData(
            userId=request_data['userId'],
            companyId=request_data['companyId'],
            role=request_data['role'],
            documents=request_data['documents']
        )

        # เรียกข้อมูล user จาก MongoDB
        user_info = get_user_data(user_data.userId)

        # เรียกใช้ insert_documents.py
        insert_documents(user_info, user_data.documents)

        # ส่งข้อความผลลัพธ์กลับ
        response = {'message': 'Documents inserted successfully'}
        return jsonify(response), 200

    except Exception as e:
        # หากมีข้อผิดพลาด
        response = {'error': str(e)}
        return jsonify(response), 500

if __name__ == '__main__':
    # เรียกใช้ Flask API ที่ port 5000
    app.run(debug=True, port=5000)
