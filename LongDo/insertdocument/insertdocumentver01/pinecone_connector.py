# pinecone_connector.py

from pinecone import Pinecone

from input import InputData
import settings as ENV
import os

def insert_to_pinecone(user_info, documents):
    
    # Set your Pinecone API key and create a Pinecone client
    api_key = os.getenv("API_PINECONE")
    pinecone = Pinecone(api_key=api_key)

    # Specify the index you want to insert into
    index_name = os.getenv("PINECONE_DATABASE_NAME")

    # Create an instance of InputData and access the 'documents' attribute
    input_data_instance = InputData(userId=user_info['userId'], companyId=user_info['companyId'],
                                    role=user_info['role'], documents=documents)

    # Access the 'documents' attribute from the InputData instance
    data = input_data_instance.documents
    
    # นำค่า input ไปเชื่อมกับ Pinecone database และ insert data
    try:
        # Assuming Pinecone returns status code along with other information
        result = pinecone.insert(index_name, data)
        status_code = result.get('status_code', 500)  # Default to 500 if no status code is returned
    except Exception as e:
        print(f"Error inserting data into Pinecone: {e}")
        status_code = 500  # Internal Server Error
    
    return status_code
