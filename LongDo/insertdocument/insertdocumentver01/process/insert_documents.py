# process/insert_documents.py

from output import display_status_code
from process.insert_log import insert_log
from pinecone_connector import insert_to_pinecone


def insert_documents(user_info, documents):
    # เรียกใช้ insert_log.py
    insert_log(user_info)

    # เชื่อมต่อ Pinecone database และ insert data
    status_code = insert_to_pinecone(user_info, documents)

    # เรียกใช้ output.py เพื่อแสดง status code
    display_status_code(status_code)
