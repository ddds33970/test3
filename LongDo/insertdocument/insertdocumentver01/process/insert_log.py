# process/insert_log.py

import datetime
from mongo_connector import insert_log_to_mongo

def insert_log(user_info):
    # ทำการบันทึก log ว่า user เข้ามาใช้งานตอนไหน
    log_data = {
        "userId": user_info["userId"],
        "action": f"add documents by {user_info['userId']} ({user_info['role']})",  # Add log action # เพิ่ม DocID # role บ / private /บลาๆ
        "timestamp": datetime.datetime.now() # Add timestamp
    }

    # เรียกใช้งานฟังก์ชันใน mongo_connector.py เพื่อบันทึก log ลงใน MongoDB
    insert_log_to_mongo(log_data)