# settings.py

from os import getcwd, getenv
from os.path import join, dirname, abspath
from dotenv import load_dotenv

basedir = abspath(dirname(__name__))
dotenv_path = join(dirname(__file__), '.env')

RESOURCE_PATH = "data"
if getcwd() == "/main":
    print("Docker Mode")
    RESOURCE_PATH = "/data"

load_dotenv(dotenv_path)

PORT = int(getenv("PORT"))
MONGODB_URI = getenv("MONGODB_URI")
DATABASE_NAME = getenv("DATABASE_NAME")