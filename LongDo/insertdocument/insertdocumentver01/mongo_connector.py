# mongo_connector.py

import os
import settings as ENV
from pymongo import MongoClient

def insert_log_to_mongo(log_data):
    # ทำการเชื่อมต่อ MongoDB
    client = MongoClient(os.getenv("MONGODB_URI"))
    db = client[os.getenv("DATABASE_NAME")]
    collection = db[os.getenv("COLLECTION_NAME")]
    
    try:
        # Insert log data into MongoDB
        collection.insert_one(log_data)
        # ปิดการเชื่อมต่อ MongoDB
        client.close()
        return True
    except Exception as e:
        print(f"Error inserting log data into MongoDB: {e}")
        return False
    
def get_user_data(user_id):
    # ทำการเชื่อมต่อ MongoDB
    client = MongoClient(os.getenv("MONGODB_URI"))
    db = client[os.getenv("DATABASE_NAME")]
    collection = db[os.getenv("COLLECTION_NAME")]
    
    try:
        # ค้นหาข้อมูล user จาก MongoDB
        user_info = collection.find_one({"userId": user_id})
        # ปิดการเชื่อมต่อ MongoDB
        client.close()
        return user_info
    except Exception as e:
        print(f"Error getting user data from MongoDB: {e}")
        return None