# requirements.txt

# python=3.12
fastapi
uvicorn[standard]
pymongo==4.6.1
pinecone-client==3.0.0
requests==2.31.0
pydantic==2.5.3