# main.py

from fastapi import FastAPI
import uvicorn
from process.insert_log import insert_log_to_mongodb, log_data
import settings as ENV
from user_input import *
from process.insert_to_pinecone import upsert_to_pinecone
from pinecone import Pinecone
import pinecone

app = FastAPI()

@app.get("/")
async def root():
    return {"message": "Hello World"}

@app.get("/testinput")
async def test_input():
    # สร้างอ็อบเจ็กต์ของ InputData จากตัวแปรที่คุณตั้งไว้
    input_data_object = InputData(fileId, userId, companyId, role, documents)
    
    # สร้างอ็อบเจ็กต์ของ InputDataModel จาก Pydantic Model
    input_data_model = InputDataModel(
        fileId=input_data_object.fileId,
        userId=input_data_object.userId,
        companyId=input_data_object.companyId,
        role=input_data_object.role,
        documents=input_data_object.documents
    )
    
    return input_data_model

# @app.post("/upsert")
# async def upsert(input_data_model: InputDataModel):
#     # ทำการเรียกใช้ฟังก์ชัน upsert_to_pinecone และส่ง InputDataModel ไป
#     upsert_to_pinecone(input_data_model)
    
#     # ทำการเรียกใช้ฟังก์ชัน insert_log_to_mongodb และส่ง log_data ไป
#     insert_log_to_mongodb(log_data)
    
#     return {"message": "upsert complete"}

upsert_to_pinecone(input_data_model) # อันนี้ใช้ได้ แต่พอเป็น API แล้วมันจะไม่ทำงาน ได้ไงว้าาาาา เดี๋ยวให้พี่ๆช่วยดู
insert_log_to_mongodb(log_data) # อันนี้ใช้ได้ แต่พอเป็น API แล้วมัน error 422 Unprocessable Entity

if __name__ == '__main__':
    uvicorn.run("main:app", port=ENV.PORT)
