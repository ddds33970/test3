# process/insert_to_pinecone.py
from pinecone import Pinecone
import settings as ENV
from user_input import *

def upsert_to_pinecone(input_data_model):
    api_key = ENV.PINECONE_API_KEY
    pc = Pinecone(api_key=api_key)
    index = pc.Index(ENV.PINECONE_INDEX_NAME)

    # ดึงข้อมูลจาก input_data_model
    fileId = input_data_model.fileId
    userId = input_data_model.userId
    companyId = input_data_model.companyId
    role = input_data_model.role
    documents = input_data_model.documents

    item_id = fileId + userId
    item_data = {
        "fileId": fileId,
        "userId": userId,
        "companyId": companyId,
        "role": role,
    }
    
    index.upsert(
        vectors=[
            {
                "id": item_id,
                "values": documents,
                "metadata": item_data
            }
        ]
    )
    print(f"Data loaded into Pinecone successfully.")
