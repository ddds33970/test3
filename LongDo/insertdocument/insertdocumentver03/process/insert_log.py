# process/insert_log.py

import datetime
import os
import pymongo
from dotenv import load_dotenv
load_dotenv()
from pymongo import MongoClient
from user_input import input_data


def insert_log_to_mongodb(log_data):
    
    client = MongoClient(os.getenv("MONGODB_URI"))
    db = client[os.getenv("MONGODB_DATABASE_NAME")]
    collection = db[os.getenv("MONGODB_COLLECTION_NAME")]
    
    log = {
        "timestamp": datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
        "action": f"add documents ({log_data['fileId']}) by {log_data['userId']} ({log_data['role']})"
    }
    
    collection.insert_one(log)
    print(f"Log inserted: {log}")
    print(f"insert log completed.")
    
log_data = {
    "fileId": input_data.fileId,
    "userId": input_data.userId,
    "role": input_data.role
}
