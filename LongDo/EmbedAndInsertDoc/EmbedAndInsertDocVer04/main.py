# EmbedAndInsertDocVer02/main.py

import os
from flask import Flask, request
from ver04.process.insert_to_pinecone import upsert_to_pinecone
from ver04.process.delete_from_pinecone import delete_namespace_pinecone
from dotenv import load_dotenv
import settings as ENV
from ver04.process.file_loader import CohereEmbeddings
from langchain_community.document_loaders import DirectoryLoader, UnstructuredWordDocumentLoader
from langchain.text_splitter import RecursiveCharacterTextSplitter

app = Flask(__name__)

@app.route('/upsert', methods=['POST'])
def main():
    upsert_to_pinecone(request)
    return {"message": "upsert complete"}

@app.route('/delete', methods=['DELETE'])
def delete():
    delete_namespace_pinecone(request)
    return {"message": "delete complete"}

if __name__ == '__main__':
    app.run(debug=True, port=ENV.PORT)