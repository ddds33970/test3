from langchain.text_splitter import RecursiveCharacterTextSplitter, CharacterTextSplitter
from langchain.document_loaders import TextLoader
from langchain.document_loaders import DirectoryLoader
from langchain.embeddings.openai import OpenAIEmbeddings
from langchain.document_loaders import PyPDFLoader
from langchain.document_loaders import PyMuPDFLoader, PyPDFLoader, TextLoader, UnstructuredWordDocumentLoader, DirectoryLoader
import pandas as pd
from langchain.embeddings import CohereEmbeddings
from langchain.vectorstores import Pinecone
import pinecone
import os
from langchain.embeddings.openai import OpenAIEmbeddings
from pinecone import Pinecone

loader = DirectoryLoader('/content/Test_file', glob="./*.docx", loader_cls=UnstructuredWordDocumentLoader)
documents = loader.load()
text_splitter = RecursiveCharacterTextSplitter(chunk_size=1000, chunk_overlap=200)
documents = text_splitter.split_documents(documents)

embeddings_data = CohereEmbeddings(cohere_api_key="SrXtKcDufDjvb4vxWjfz5eIeT6JBySHXmbV9gKt0", model='embeddings', truncate = None)

os.environ["OPENAI_API_KEY"] = "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"
embeddings = OpenAIEmbeddings()

api_key="0f130012-88dd-43c9-9333-93c51c094329"
pc = Pinecone(api_key=api_key)
index = pc.Index("user-id-001")

# First, check if our index already exists. If it doesn't, we create it
if index not in pinecone.list_indexes():
    # we create a new index
    pinecone.create_index(
      name=index,
      metric='cosine',
      dimension = 1536
)
    
userId = "userId09"
companyId = "companyId09"
role = "Private"

for i in range(len(documents)):

  page_content = documents[i].page_content
  file_names = documents[i].metadata['source'].split('/')[-1]

  item_id = f"{userId}_{file_names}_doc_{i}"
  item_data = {
    "file_names": file_names,
    "userId": userId,
    "companyId": companyId,
    "role": role,
    "documents": documents
  }

  index.upsert(
    vectors = [
        {
          "id": item_id,
          "values": embeddings,
          "metadata": item_data
        }
      ]
  )
  print(f"Data loaded into Pinecone successfully.")
  print(file_names, "\n" + f"page_content:", page_content, "\n\n\n")
