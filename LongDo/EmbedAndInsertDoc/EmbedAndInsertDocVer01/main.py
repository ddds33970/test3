# ver01/main.py

import os
from flask import Flask, request
from ver01.process.embeddingcode import embeddings_data_def
from ver01.process.insert_log import insert_log_to_mongodb
from ver01.process.insert_to_pinecone import upsert_to_pinecone
from dotenv import load_dotenv
import settings as ENV
from ver01.process.embeddingcode import CohereEmbeddings
from langchain_community.document_loaders import DirectoryLoader, UnstructuredWordDocumentLoader
from langchain.text_splitter import RecursiveCharacterTextSplitter

app = Flask(__name__)

@app.route('/upsert', methods=['POST'])
def main():
    embeddings_data_def()
    upsert_to_pinecone(request)
    # insert_log_to_mongodb(request)
    return {"message": "upsert complete"}

# @app.route('/test', methods=['GET'])
# def test():
    loader = DirectoryLoader('/Users/j.papontee/Library/CloudStorage/GoogleDrive-st32673@kn.ac.th/ไดรฟ์ของฉัน/PIM/Botnoi/LongDo/EmbedAndInsertDoc/EmbedAndInsertDocVer01/Test_file', glob="./*.docx", loader_cls=UnstructuredWordDocumentLoader)
    documents = loader.load()
    text_splitter = RecursiveCharacterTextSplitter(chunk_size=1000, chunk_overlap=200)
    documents = text_splitter.split_documents(documents)
    
    os.environ["OPENAI_API_KEY"] = os.getenv("OPENAI_API_KEY")
    embeddings_data = CohereEmbeddings(cohere_api_key=os.getenv("COHERE_API_KEY"), model='embed-multilingual-v3.0', truncate = None)
    
    supported_input_types = type(embeddings_data)
    print(supported_input_types)
    return {"message": "test complete"}


if __name__ == '__main__':
    app.run(debug=True, port=ENV.PORT)