# process/insert_log.py

import datetime
import os
import pymongo
from dotenv import load_dotenv
load_dotenv()
from pymongo import MongoClient
from ver01.user_input import process_json
from ver01.process.insert_to_pinecone import upsert_to_pinecone


def insert_log_to_mongodb(request):
    
    client = MongoClient(os.getenv("MONGODB_URI"))
    db = client[os.getenv("MONGODB_DATABASE_NAME")]
    collection = db[os.getenv("MONGODB_COLLECTION_NAME")]
    
    item_id = upsert_to_pinecone(request)
    
    log_data = process_json(request)
    
    log = {
        "timestamp": datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
        "action": f"add documents ({item_id}) by {log_data[0]} ({log_data[2]})"
        # log_data[0] = userId, log_data[1] = companyId, log_data[2] = role
    }
    
    collection.insert_one(log)
    print(f"insert log completed.")
    print(f"Log inserted: {log}")
