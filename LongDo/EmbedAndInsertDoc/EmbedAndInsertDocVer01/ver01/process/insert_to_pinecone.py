# ver01/process/insert_to_pinecone.py
from pinecone import Pinecone
import settings as ENV
from ver01.user_input import process_json
from ver01.process.embeddingcode import embeddings_data_def
from langchain.embeddings.openai import OpenAIEmbeddings

def upsert_to_pinecone(request):
    api_key = ENV.PINECONE_API_KEY
    pc = Pinecone(api_key=api_key)
    index = pc.Index(ENV.PINECONE_INDEX_NAME)
    
    # if index not in pc.list_indexes():
    #     # create a new index
    #     pc.create_index(
    #         name=index,
    #         metric='cosine',
    #         dimension = 1536
    # )
    
    embeddings_model, documents = embeddings_data_def()
    
    for i in range(len(documents)):

        page_content = documents[i].page_content
        fileId = documents[i].metadata['source'].split('/')[-1]
        
        userId, companyId, role= process_json(request)

        item_id = f"{userId}_{fileId}_doc_{i+1}"
        item_data = {
            "fileId": fileId,
            "userId": userId,
            "companyId": companyId,
            "role": role,
            "documents": page_content
        }
        
        embeddings_values = embeddings_model.embed_documents(documents[i].page_content)

        index.upsert(
            vectors=[
                {
                    "id": item_id,
                    "values": embeddings_values[0][:],
                    "metadata": item_data
                }
            ]
        )
        
        print(f"Data loaded into Pinecone successfully.")
        print(fileId, "\n" + f"page_content:", page_content, "\n\n\n")