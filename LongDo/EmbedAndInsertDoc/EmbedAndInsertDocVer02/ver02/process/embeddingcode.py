# ver02/process/embeddingcode.py

import os
from langchain_community.document_loaders import DirectoryLoader, UnstructuredWordDocumentLoader
from langchain.text_splitter import RecursiveCharacterTextSplitter
from langchain_community.embeddings import CohereEmbeddings
from langchain.embeddings.openai import OpenAIEmbeddings
# กลับไปใช้ OPENAI แทน
def embeddings_data_def():
    # แก้ path เป็นที่อยู่ของไฟล์
    loader = DirectoryLoader('/Users/j.papontee/Library/CloudStorage/GoogleDrive-st32673@kn.ac.th/ไดรฟ์ของฉัน/PIM/Botnoi/LongDo/EmbedAndInsertDoc/EmbedAndInsertDocVer02/Test_file', glob="./*.docx", loader_cls=UnstructuredWordDocumentLoader)
    documents = loader.load()
    text_splitter = RecursiveCharacterTextSplitter(chunk_size=1000, chunk_overlap=200)
    documents = text_splitter.split_documents(documents)
    
    os.environ["OPENAI_API_KEY"] = os.getenv("OPENAI_API_KEY")
    # embeddings_data = CohereEmbeddings(cohere_api_key=os.getenv("COHERE_API_KEY"), model='embed-multilingual-v3.0', truncate = None)
    embeddings_model = OpenAIEmbeddings()
    # embeddings = []
    # for document in documents:
    #     document_embeddings = embeddings_data.embed([document.page_content], input_type=[str])
    #     embeddings.append(document_embeddings)
        
    return embeddings_model, documents
