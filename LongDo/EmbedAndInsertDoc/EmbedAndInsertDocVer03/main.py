# EmbedAndInsertDocVer02/main.py

import os
from flask import Flask, request
from ver03.process.embeddingcode import embeddings_data_def
from ver03.process.insert_to_pinecone import upsert_to_pinecone
from ver03.process.delete_from_pinecone import delete_in_namespace_pinecone
from dotenv import load_dotenv
import settings as ENV
from ver03.process.embeddingcode import CohereEmbeddings
from langchain_community.document_loaders import DirectoryLoader, UnstructuredWordDocumentLoader
from langchain.text_splitter import RecursiveCharacterTextSplitter

app = Flask(__name__)

@app.route('/upsert', methods=['POST'])
def main():
    embeddings_data_def()
    upsert_to_pinecone(request)
    # insert_log_to_mongodb(request)
    return {"message": "upsert complete"}

@app.route('/delete', methods=['DELETE'])
def delete():
    delete_in_namespace_pinecone(request)
    return {"message": "delete complete"}

if __name__ == '__main__':
    app.run(debug=True, port=ENV.PORT)