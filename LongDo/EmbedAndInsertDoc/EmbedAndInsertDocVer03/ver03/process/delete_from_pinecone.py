# ver03/process/delete_from_pinecone.py
from pinecone import Pinecone
import settings as ENV
from ver03.user_input import process_json
from pymongo import MongoClient
import datetime
import os

def delete_in_namespace_pinecone(request):
    # Pinecone connect
    api_key = ENV.PINECONE_API_KEY
    pc = Pinecone(api_key=api_key)
    index = pc.Index(ENV.PINECONE_INDEX_NAME)
    
    # MongoDB connect
    client = MongoClient(os.getenv("MONGODB_URI"))
    db = client[os.getenv("MONGODB_DATABASE_NAME")]
    collection = db[os.getenv("MONGODB_COLLECTION_NAME")]
    
    # ดึงค่าที่ input มาจาก user ในรูปแบบ json จากฟังก์ชัน process_json ในไฟล์ ver03/user_input.py 
    userId, _, role, floder = process_json(request)
    
    index.delete(delete_all=True, namespace=floder)
    
    # กำหนดข้อมูลที่จะ insert ไปใน MongoDB
    log_delete_data_namespace = {
    "timestamp": datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
        "action": f"delete documents ({floder}) by {userId} ({role})"
    }
        
    # insert log ขึ้นใน MongoDB
    collection.insert_one(log_delete_data_namespace)
        
    print(f"delete floder: {floder} completed.\n")