# EmbedAndInsertDocVer05/main.py

from flask import Flask, request
from ver05.process.insert_to_pinecone import docx_upsert_to_pinecone, csv_upsert_to_pinecone
from ver05.process.delete_from_pinecone import delete_namespace_pinecone
import settings as ENV

app = Flask(__name__)

@app.route('/docxupsert', methods=['POST'])
def docx_upsert():
    docx_upsert_to_pinecone(request)
    return {"message": "upsert complete"}

@app.route('/csvupsert', methods=['POST'])
def csv_upsert():
    csv_upsert_to_pinecone(request)
    return {"message": "upsert complete"}

@app.route('/delete', methods=['DELETE'])
def delete():
    delete_namespace_pinecone(request)
    return {"message": "delete complete"}

if __name__ == '__main__':
    app.run(debug=True, port=ENV.PORT)