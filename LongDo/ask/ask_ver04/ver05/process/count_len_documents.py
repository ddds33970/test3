# ver05.process.count_len_documents.py
from ver05.process.file_loader import all_file_loader

def count_len_documents():
    # เรียกใช้ function all_file_loader จาก all_file_loader.py
    all_file_loader()
    # ดึงค่ามาจาก all_file_loader ใน ver05/process/all_file_loader.py
    _, documents = all_file_loader()
    list_len_docs = [len(documents[i].page_content) for i in range(0, len(documents))]
    sum_len_docs = sum(list_len_docs)
    print(f"Total length of documents: {sum_len_docs}")