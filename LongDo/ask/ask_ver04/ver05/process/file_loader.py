# ver05/process/embeddingcode.py

import os
from langchain_openai import OpenAIEmbeddings
from langchain_community.document_loaders import UnstructuredWordDocumentLoader
from langchain_community.document_loaders import CSVLoader, PyPDFLoader, TextLoader
from langchain.text_splitter import RecursiveCharacterTextSplitter


def all_file_loader(directory = "/Users/j.papontee/Library/CloudStorage/GoogleDrive-st32673@kn.ac.th/ไดรฟ์ของฉัน/PIM/Botnoi/LongDo/ask/ask_ver04/Test_file"):
    embeddings_model = OpenAIEmbeddings()
    documents = []
    
    for filename in os.listdir(directory):
        text_splitter = RecursiveCharacterTextSplitter(chunk_size=1000, chunk_overlap=200)
        if filename.endswith(".csv"):
            loader = CSVLoader(os.path.join(directory, filename))
            documents.extend(text_splitter.split_documents(loader.load()))
        if filename.endswith(".txt"):
            loader = TextLoader(os.path.join(directory, filename))
            documents.extend(text_splitter.split_documents(loader.load()))
        if filename.endswith(".pdf"):
            loader = PyPDFLoader(os.path.join(directory, filename))
            documents.extend(text_splitter.split_documents(loader.load()))
        if filename.endswith(".docx"):
            loader = UnstructuredWordDocumentLoader(os.path.join(directory, filename))
            documents.extend(text_splitter.split_documents(loader.load()))

    return embeddings_model, documents

# import os
# import glob
# from langchain_community.document_loaders import DirectoryLoader, UnstructuredWordDocumentLoader
# from langchain.text_splitter import RecursiveCharacterTextSplitter
# from langchain.embeddings.openai import OpenAIEmbeddings
# from langchain_community.document_loaders import CSVLoader
# from ver05.util.convert_file.txt_to_docx import convert_txt_to_docx
# from ver05.util.convert_file.pdf_to_docx import convert_pdf_to_docx
# from ver05.util.folder_path import folder_path

# def file_loader():
#     embeddings_model = OpenAIEmbeddings()
#     documents = []
#     # เรียกใช้ function convert_txt_to_docx จาก txt_to_docx.py
#     convert_txt_to_docx()
#     convert_pdf_to_docx()
#     # ทำการโหลดไฟล์ .docx
#     docx_files = DirectoryLoader(folder_path, glob="./*.docx", loader_cls=UnstructuredWordDocumentLoader).load()

#     text_splitter = RecursiveCharacterTextSplitter(chunk_size=1000, chunk_overlap=200)
#     docx_documents = text_splitter.split_documents(docx_files)
#     documents.extend(docx_documents)

#     # ทำการโหลดไฟล์ .csv
#     csv_files = glob.glob(os.path.join(folder_path, '*.csv'))

#     for csv_file in csv_files:
#         csv_loader = CSVLoader(file_path=csv_file)
#         csv_documents = csv_loader.load()

#         text_splitter = RecursiveCharacterTextSplitter(chunk_size=1000, chunk_overlap=200)
#         csv_documents = text_splitter.split_documents(csv_documents)
#         documents.extend(csv_documents)

#     return embeddings_model, documents

