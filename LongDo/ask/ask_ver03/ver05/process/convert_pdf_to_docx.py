# ver05/process/pdf_to_docx.py
import os
from pdf2docx import Converter

def convert_pdf_to_docx(pdf_path, docx_path):
    # สร้างอ็อบเจ็กต์ Converter
    cv = Converter(pdf_path)

    # แปลง PDF เป็น .docx
    cv.convert(docx_path, start=0, end=None)

    # ปิด Converter
    cv.close()