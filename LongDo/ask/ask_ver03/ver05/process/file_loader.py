# ver05/process/embeddingcode.py

import os
import glob
from docx import Document
from langchain_community.document_loaders import DirectoryLoader, UnstructuredWordDocumentLoader
from langchain.text_splitter import RecursiveCharacterTextSplitter
from langchain.embeddings.openai import OpenAIEmbeddings
from langchain_community.document_loaders import CSVLoader
from .convert_pdf_to_docx import convert_pdf_to_docx

def file_loader():
    
    documents = []
    embeddings_model = OpenAIEmbeddings()

    folder_path = "/Users/j.papontee/Library/CloudStorage/GoogleDrive-st32673@kn.ac.th/ไดรฟ์ของฉัน/PIM/Botnoi/LongDo/ask_ver03/Test_file"

    # หาไฟล์ .txt ทั้งหมดในโฟลเดอร์
    txt_files = [file for file in os.listdir(folder_path) if file.endswith('.txt')]

    # วนลูปเพื่อแปลงไฟล์ .txt เป็น .docx
    for txt_file in txt_files:
        # สร้างเส้นทางเต็มของไฟล์ .txt
        txt_file_path = os.path.join(folder_path, txt_file)

        # นำเสนอชื่อ .txt และสร้างชื่อ .docx ใหม่
        docx_file_name = os.path.splitext(txt_file)[0] + '.docx'
        docx_file_path = os.path.join(folder_path, docx_file_name)

        # อ่านข้อมูลจากไฟล์ .txt และเขียนลงในไฟล์ .docx
        with open(txt_file_path, 'r', encoding='utf-8') as txt_file:
            document = Document()
            document.add_paragraph(txt_file.read())
            document.save(docx_file_path)
    
    # ทำการโหลดไฟล์ .pdf
    pdf_files = [f for f in os.listdir(folder_path) if f.endswith('.pdf')]
    # สร้างที่เก็บข้อมูลที่ได้จาก pdf ไปเป็น docx
    docx_files_from_pdf = []
    # แปลง pdf เป็น docx """เงื่อนไข ถ้ามีไฟล์ชื่อเดียวกับ pdf ที่เป็น docx จะ save ทับข้อมูลเดิม ดังนั้นควรใช้คนละชื่อไฟล์"""
    for pdf_file in pdf_files:
        pdf_file_path = os.path.join(folder_path, pdf_file)
        docx_file_path = os.path.join(folder_path, os.path.splitext(pdf_file)[0] + '.docx')
        convert_pdf_to_docx(pdf_file_path, docx_file_path)
        docx_files_from_pdf.append(docx_file_path)
        
    # ทำการโหลดไฟล์ .docx
    docx_files = DirectoryLoader(folder_path, glob="./*.docx", loader_cls=UnstructuredWordDocumentLoader).load()

    text_splitter = RecursiveCharacterTextSplitter(chunk_size=1000, chunk_overlap=200)
    docx_documents = text_splitter.split_documents(docx_files)
    documents.extend(docx_documents)

    # ทำการโหลดไฟล์ .csv
    csv_files = glob.glob(os.path.join(folder_path, '*.csv'))

    for csv_file in csv_files:
        csv_loader = CSVLoader(file_path=csv_file)
        csv_documents = csv_loader.load()

        text_splitter = RecursiveCharacterTextSplitter(chunk_size=1000, chunk_overlap=200)
        csv_documents = text_splitter.split_documents(csv_documents)
        documents.extend(csv_documents)

    return embeddings_model, documents
