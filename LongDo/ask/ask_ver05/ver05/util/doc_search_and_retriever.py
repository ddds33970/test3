# ver05/util/doc_search_and_retriever.py
from langchain_community.vectorstores import Pinecone
from langchain_openai import OpenAIEmbeddings
import settings as ENV
from ver05.user_input import ask_chatbot_json

def doc_search_and_retriever(request):
    index_name = ENV.PINECONE_INDEX_NAME
    embeddings = OpenAIEmbeddings()

    _, _, folder = ask_chatbot_json(request)

    docsearch = Pinecone.from_existing_index(index_name, embeddings, namespace=folder)

    retriever_pinecone = docsearch.as_retriever(
        # search_type="similarity_score_threshold",
        # search_kwargs={'score_threshold': 0.9}
        
        search_kwargs={"k": 5}
        # namespace=folder
    )
    return retriever_pinecone

