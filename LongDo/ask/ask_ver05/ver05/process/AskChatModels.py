# ver05/process/AskChatModels.py

import json
from langchain_core.runnables import RunnableLambda, RunnablePassthrough
from operator import itemgetter
from pinecone import Pinecone
from langchain_core.output_parsers import StrOutputParser
from langchain_core.messages import get_buffer_string
from ver05.util.prompt_rephrase import CONDENSE_QUESTION_PROMPT
from ver05.util._combine_documents import _combine_documents
from ver05.util.prompt_template import ANSWER_PROMPT
from ver05.util.cohere_reRank import CompressionRetriever
from langchain_openai import ChatOpenAI
from ver05.util.memory import memory
from ver05.user_input import ask_chatbot_json
import settings as ENV

# Pinecone connect
api_key = ENV.PINECONE_API_KEY
pc = Pinecone(api_key=api_key)
index = pc.Index(ENV.PINECONE_INDEX_NAME)

llm_for_calling = ChatOpenAI(model="gpt-3.5-turbo-1106")

def AskChatModels(request):
    compression_retriever = CompressionRetriever(request)

    # First we add a step to load memory
    # This adds a "memory" key to the input object
    loaded_memory = RunnablePassthrough.assign(
        chat_history=RunnableLambda(memory.load_memory_variables) | itemgetter("history"),
    )
    # Now we calculate the standalone question
    standalone_question = {
        "standalone_question": {
            "question": lambda x: x["question"],
            "chat_history": lambda x: get_buffer_string(x["chat_history"]),
        }
        | CONDENSE_QUESTION_PROMPT
        | llm_for_calling
        | StrOutputParser(),
    }

    # Now we retrieve the documents
    retrieved_documents = {
        "docs": itemgetter("standalone_question") | compression_retriever,
        "question": lambda x: x["standalone_question"],
    }
    # Now we construct the inputs for the final prompt
    final_inputs = {
        "context": lambda x: _combine_documents(x["docs"]),
        "question": itemgetter("question"),
    }
    # And finally, we do the part that returns the answers
    answer = {
        "answer": final_inputs | ANSWER_PROMPT | llm_for_calling,
        "docs": itemgetter("docs"),
    }

    # And now we put it all together!
    final_chain = loaded_memory | standalone_question | retrieved_documents | answer
    
    return final_chain

def ask(request):
    json_data, _, _= ask_chatbot_json(request)
    final_chain = AskChatModels(request)
    
    question_dict = json.loads(f'{{"question": "{json_data["question"]}"}}')

    result = final_chain.invoke(question_dict)
    
    print(result)
    
    return result

# result = final_chain.invoke(question)