# ver05/process/print_docs_from_query.py
from ver05.util.doc_search_and_retriever import doc_search_and_retriever
from ver05.user_input import ask_chatbot_json

def variable_for_pretty_print_docs(request):
    retriever_pinecone = doc_search_and_retriever(request)
    question, _ = ask_chatbot_json(request)

    docs = retriever_pinecone.get_relevant_documents(question)
    pretty_print_docs(docs)
    return docs

def pretty_print_docs(docs):
    print(
        f"\n{'-' * 100}\n".join(
            [f"Document {i+1}:\n\n" + d.page_content for i, d in enumerate(docs)]
        )
    )
