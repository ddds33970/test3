from flask import Flask, jsonify, request, session, redirect, render_template, flash
from passlib.hash import pbkdf2_sha256
from bson.objectid import ObjectId
from bson import ObjectId
from werkzeug.utils import secure_filename
from dbconnector.mongo_connector import MongoConnector
#from app import user_db, upload_db, log_login_collection, log_signup_collection,role_user_collection,role_admin_collection
from repositories.user_repository import get_thai_time
#from process.upload_read import all_file_loader
import settings as ENV
import os

user_db,upload_db,log_db,role_db = MongoConnector.connect()
role_user_collection = role_db[ENV.USER_ROLE_COLLECTION]
role_admin_collection = role_db[ENV.ADMIN_ROLE_COLLECTION]
user_collection = user_db[ENV.USER_COLLECTION]
log_login_collection = log_db[ENV.USER_LOGIN_LOG]
log_signup_collection = log_db[ENV.USER_SIGNUP_LOG]

class User:
  
  #login หลังจากที่ user signup เข้ามาแล้ว
  def start_session(self, user):
    # เข้ารหัสรหัสผ่าน
    hashed_password = pbkdf2_sha256.encrypt(user['password'])
    # ลบรหัสผ่าน
    del user['password']
    session['logged_in'] = True
    session['user'] = user
    log_login_collection.insert_one({ "userId":os.urandom(16).hex(),
                                            "username": request.form.get('username'),
                                            "password": hashed_password,
                                            "errorMessage":"-",
                                            "status": int('1'),
                                            "datetime":str(get_thai_time())})
    return jsonify(user), 200
  
  #fuction register
  def signup(self):
    print(request.form)

    user = {
      "_id": ObjectId(),  
      "userId": os.urandom(16).hex(),
      "companyId": os.urandom(16).hex(),
      "username": request.form.get('username'),
      "password": request.form.get('password'),
      "role": request.form.get('role'),
      "startUpdate": str(get_thai_time()),
      "lastUpdate": str(get_thai_time()),
      "uploadQuota": int('0'),
      "queryQuota": int('0')  
    }
    

    user['password'] = pbkdf2_sha256.encrypt(user['password'])
    
    if user_db.users.find_one({ "username": user['username'] }):
      return jsonify({ "error": "username already in use" }), 400
    if user["role"] == "user":
      role_user_collection.insert_one({"displayName":request.form.get('username'),
                                      "datetime":str(get_thai_time()) })
    if user["role"] == "admin":
      role_admin_collection.insert_one({"displayName":request.form.get('username'),
                                      "datetime":str(get_thai_time()) })
    if user_db.users.insert_one(user):
      hashed_password = pbkdf2_sha256.encrypt(user['password'])
      log_signup_collection.insert_one({"userId":os.urandom(16).hex(),
                                            "username": request.form.get('username'),
                                            "password": hashed_password,
                                            "errorMessage":"-",
                                            "status": int('1'),
                                            "datetime":str(get_thai_time())})
    
      user["_id"] = str(user["_id"])#แปลง _id และ userId เป็น string เพื่อให้สามารถส่งออกเป็น JSON ได้
      return self.start_session(user)
    else:
      # ลงทะเบียนผู้ใช้ล้มเหลว
      hashed_password = pbkdf2_sha256.encrypt(user['password'])
      log_signup_collection.insert_one({"userId":os.urandom(16).hex(),
                                            "username": request.form.get('username'),
                                            "password": hashed_password,
                                            "errorMessage":"failed to register",
                                            "status": int('0'),
                                            "datetime":str(get_thai_time())})
      return jsonify({ "error": "Signup failed" }), 400
  
  #fuction logout
  def signout(self):
    session.clear()
    return redirect('/')
  
  #fuction login
  def login(self):

    user = user_db.users.find_one({
      "username": request.form.get('username')
    })
    if user:
      user["_id"] = str(user["_id"])
    else:
        return jsonify({ "error": "can't find your ID" }), 401

    if user and pbkdf2_sha256.verify(request.form.get('password'), user['password']):
      return self.start_session(user)
    
    hashed_password = pbkdf2_sha256.encrypt(user['password'])
    log_login_collection.users.insert_one({ "userId":os.urandom(16).hex(),
                                            "username": request.form.get('username'),
                                            "password": hashed_password,
                                            "errorMessage":"failed to login",
                                            "status": int('0'),
                                            "datetime":str(get_thai_time())})
    
    return jsonify({ "error": "login fail" }), 401
  
  '''#fuction upload file
  def upload(self):
    if 'attachment' in request.files:
        """file = request.files['attachment']#เหมาะกับรับแค่ไฟล์เดียว"""
        files = request.files.getlist("attachment")#เหมาะกับรับหลายๆไฟล์
        embeddings_model, documents = all_file_loader(files)
        if files:
            # บันทึกไฟล์ลงในฐานข้อมูล MongoDB
            filename = secure_filename(files.filename)
            file_type = files.content_type  # รับประเภทของไฟล์
            username = session['user']['username']
            upload_db.files.insert_one({"filename": filename,
                                        "type": file_type,
                                        "userUpload":username,  # ใช้ชื่อผู้ใช้ที่เข้าสู่ระบบ,
                                        "status":"Upload Success",
                                        "startUpdate":str(get_thai_time()),
                                        "latestUpdate":str(get_thai_time()),
                                        })
            # ส่งคืนการสำเร็จหรือเกิดข้อผิดพลาดกลับไปยังผู้ใช้
            flash('File successfully uploaded ' + files.filename + ' to the database!')
            return render_template('dashboard.html')
        else:
          flash('User not found', 'error')
          return render_template('dashboard.html')
    # ถ้าไม่มีไฟล์ถูกอัปโหลด ส่งข้อความผิดพลาดกลับไปยังผู้ใช้
    flash('No file uploaded', 'error')
    return render_template('dashboard.html')'''
  
