import settings as ENV
import pymongo 

class MongoConnector:
    # สร้าง Client เพื่อเชื่อมต่อ MongoDB โดยใช้ข้อมูลจากไฟล์ settings.py
    client = pymongo.MongoClient(ENV.MONGODB_URI)

    # การเข้าถึงฐานข้อมูล 
    user_db = client[ENV.MONGODB_DB_USER]
    upload_db = client[ENV.MONGODB_DB_FILE]
    log_db = client[ENV.MONGODB_DB_LOG]
    role_db = client[ENV.MONGODB_DB_ROLE]
    # การเข้าถึงคอลเล็กชัน 
    role_user_collection = role_db[ENV.USER_ROLE_COLLECTION]
    role_admin_collection = role_db[ENV.ADMIN_ROLE_COLLECTION]
    user_collection = user_db[ENV.USER_COLLECTION]
    log_login_collection = log_db[ENV.USER_LOGIN_LOG]
    log_signup_collection = log_db[ENV.USER_SIGNUP_LOG]

    @classmethod
    def connect(cls):
        return cls.user_db, cls.upload_db, cls.log_db, cls.role_db

    @classmethod
    def disconnect(cls):
        return cls.client.close()
