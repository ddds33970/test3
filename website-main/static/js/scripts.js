$("form[name=signup_form]").submit(function(e) {
  var $form = $(this);
  var $error = $form.find(".error");
  var data = $form.serialize();

  // แปลง ObjectId เป็นสตริงก่อนส่งข้อมูล
  data = data.replace(/_id=.*?&/, '');

  $.ajax({
    url: "/user/signup",
    type: "POST",
    data: data,
    dataType: "json",
    success: function(resp) {
      console.log(resp); // Log the response to the console
      window.location.href = "/dashboard/";
    },
    error: function(resp) {
      $error.text(resp.responseJSON.error).removeClass("error--hidden");
    }
  });

  e.preventDefault();
});

$("form[name=login_form]").submit(function(e) {
  var $form = $(this);
  var $spinner = $("#spinner");
  var $error = $form.find(".error");
  var data = $form.serialize();

  // แสดง spinner ขณะที่กำลังส่งข้อมูล
  $spinner.show();

  // แปลง ObjectId เป็นสตริงก่อนส่งข้อมูล
  data = data.replace(/_id=.*?&/, '');

  $.ajax({
    url: "/user/login",
    type: "POST",
    data: data,
    dataType: "json",
    success: function(resp) {
      console.log(resp); // Log the response to the console
      window.location.href = "/dashboard/";
    },
    error: function(resp) {
      $error.text(resp.responseJSON.error).removeClass("error--hidden");
    }
  });

  e.preventDefault();
});

$(document).ready(function () {
  $("form[name=upload_form]").submit(function (event) {
      event.preventDefault();
      var formData = $(this).serialize();
      var $spinner = $("#spinner");
      // แสดง spinner ขณะที่กำลังส่งข้อมูล
      $spinner.show();
      $.ajax({
          type: 'POST',
          url: '/user/upload',
          data: formData,
          success: function (response) {
              alert(response);
          },
          error: function (error) {
              console.log('Error:', error);
          }
      });
  });
});

// แสดงชื่อไฟล์ แสดงปุ่มที่ชัดเจน และเปลี่ยนการเรียกดู 
//ข้อความปุ่มเมื่อเลือกไฟล์นามสกุลที่ถูกต้อง
$(".browse-button input:file").change(function (){
  $("input[name='attachment']").each(function() {
    var fileName = $(this).val().split('/').pop().split('\\').pop();
    $(".filename").val(fileName);
    $(".browse-button-text").html('<i class="fa fa-refresh"></i> Change');
    $(".clear-button").show();
  });
});

//actions เมือกดปุ่ม
$('.clear-button').click(function(){
    $('.filename').val("");
    $('.clear-button').hide();
    $('.browse-button input:file').val("");
    $(".browse-button-text").html('<i class="fa fa-folder-open"></i> Browse'); 
});