from datetime import datetime
import pytz

def get_thai_time():
        # กำหนดโซนเวลาของกรุงเทพฯ
        thai_timezone = pytz.timezone('Asia/Bangkok')
        # รับค่าเวลาปัจจุบันที่ถูกต้องโดยใช้โซนเวลาของกรุงเทพฯ
        current_time = datetime.now(thai_timezone)
        # แปลงเวลาให้เป็น string ในรูปแบบที่ต้องการ
        thai_time = current_time.strftime("%d-%m-%Y %H:%M:%S")
        return thai_time 