from flask import Flask
from app import app
from user.models import User
from process.AskChatModels import Ask
from process.upload_read import Upload


@app.route('/user/signup', methods=['POST'])
def signup():
  return User().signup()

@app.route('/user/signout')
def signout():
  return User().signout()

@app.route('/user/login', methods=['POST'])
def login():
  return User().login()

@app.route('/user/upload', methods=['POST'])
def upload():
  return Upload().upload()

@app.route('/process/ask', methods=['POST'])
def ask():
    return Ask().ask()
