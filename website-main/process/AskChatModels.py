# ver05/process/AskChatModels.py

import json
from langchain_core.runnables import RunnableLambda, RunnablePassthrough
from operator import itemgetter
from pinecone import Pinecone
from langchain_core.output_parsers import StrOutputParser
from langchain_core.messages import get_buffer_string
from util.prompt_rephrase import CONDENSE_QUESTION_PROMPT
from util._combine_documents import _combine_documents
from util.prompt_template import ANSWER_PROMPT
from util.cohere_reRank import compression_retriever
from langchain_openai import ChatOpenAI
from util.memory import memory
import settings as ENV

# Pinecone connect
api_key = ENV.PINECONE_API_KEY
pc = Pinecone(api_key=api_key)
index = pc.Index(ENV.PINECONE_INDEX_NAME)

llm_for_calling = ChatOpenAI(model="gpt-3.5-turbo-1106")
# global variable
folder = None
question = None
# First we add a step to load memory
# This adds a "memory" key to the input object
loaded_memory = RunnablePassthrough.assign(
    chat_history=RunnableLambda(memory.load_memory_variables) | itemgetter("history"),
)
# Now we calculate the standalone question
standalone_question = {
    "standalone_question": {
        "question": lambda x: x["question"],
        "chat_history": lambda x: get_buffer_string(x["chat_history"]),
    }
    | CONDENSE_QUESTION_PROMPT
    | llm_for_calling
    | StrOutputParser(),
}

# Now we retrieve the documents
retrieved_documents = {
    "docs": itemgetter("standalone_question") | compression_retriever,
    "question": lambda x: x["standalone_question"],
}
# Now we construct the inputs for the final prompt
final_inputs = {
    "context": lambda x: _combine_documents(x["docs"]),
    "question": itemgetter("question"),
}
# And finally, we do the part that returns the answers
answer = {
    "answer": final_inputs | ANSWER_PROMPT | llm_for_calling,
    "docs": itemgetter("docs"),
}

# And now we put it all together!
final_chain = loaded_memory | standalone_question | retrieved_documents | answer
class Ask:
    @staticmethod
    def ask_chatbot_json(request):
        # เรียกใช้ global variable
        global folder, question_dict
        # รับ JSON จาก Postman
        json_data = request.get_json()

        # ตรวจสอบว่ามีข้อมูลที่ต้องการหรือไม่
        if not json_data:
            return 'ไม่พบข้อมูล JSON', 400

        try:
            # ตรวจสอบความสมบูรณ์ของข้อมูล JSON
            required_keys = ['question', 'folder']
            for key in required_keys:
                if key not in json_data:
                    return f'ไม่พบคีย์ {key} ในข้อมูล JSON', 400
            
            # กำหนดค่าให้กับ global variable
            folder = json_data['folder']
            question = json_data['question']

            return json_data, question, folder

        except KeyError as e:
            # หากไม่พบ key ที่ต้องการใน JSON
            return f'ไม่พบ key {e} ใน JSON', 400
        
    def ask(self):
        json_data, _, _ = Ask.ask_chatbot_json()  # Fixed by adding the class name

        question_dict = json.loads(f'{{"question": "{json_data["question"]}"}}')

        result = final_chain.invoke(question_dict)
        
        print(result)

        return result

    # result = final_chain.invoke(question)