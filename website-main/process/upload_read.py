
import os
from flask import Flask, request, session, render_template, flash
from langchain_openai import OpenAIEmbeddings
from langchain_community.document_loaders import UnstructuredWordDocumentLoader
from langchain_community.document_loaders import CSVLoader, PyPDFLoader, TextLoader
from langchain.text_splitter import RecursiveCharacterTextSplitter
from repositories.user_repository import get_thai_time
from werkzeug.utils import secure_filename
from dbconnector.mongo_connector import MongoConnector

upload_db = MongoConnector.connect()

class Upload:
    @staticmethod
    def all_file_loader(files):
        embeddings_model = OpenAIEmbeddings()
        documents = []

        for file in files:
            filename = file.filename
            file_extension = os.path.splitext(filename)[1]

            text_splitter = RecursiveCharacterTextSplitter(chunk_size=1000, chunk_overlap=200)

            if file_extension == ".csv":
                try:
                    loader = CSVLoader(file)
                    documents.extend(text_splitter.split_documents(loader.load()))
                except Exception as e:
                    flash(f"Error loading CSV file: {filename}. Error: {str(e)}", 'error')  
            elif file_extension == ".txt":
                loader = TextLoader(file)
                documents.extend(text_splitter.split_documents(loader.load()))
            elif file_extension == ".pdf":
                loader = PyPDFLoader(file)
                documents.extend(text_splitter.split_documents(loader.load()))
            elif file_extension == ".docx":
                loader = UnstructuredWordDocumentLoader(file)
                documents.extend(text_splitter.split_documents(loader.load()))      
        Upload.upload(embeddings_model, documents)

        return embeddings_model, documents

        # Function upload file
    def upload(self):
        if 'attachment' in request.files:
            files = request.files.getlist("attachment")
            embeddings_model, documents = Upload.all_file_loader(files) 
            if files:
                filename = secure_filename(files.filename)
                file_type = files.content_type
                username = session['user']['username']
                upload_db.files.insert_one({"filename": filename,
                                            "type": file_type,
                                            "userUpload": username,
                                            "status": "Upload Success",
                                            "startUpdate": str(get_thai_time()),
                                            "latestUpdate": str(get_thai_time()),
                                            })
                flash('File successfully uploaded ' + files.filename + ' to the database!')
                return render_template('dashboard.html')
            else:
                flash('User not found', 'error')
                return render_template('dashboard.html')
        flash('No file uploaded', 'error')
        return render_template('dashboard.html')
    
    """def all_file_loader(directory = "/Users/j.papontee/Library/CloudStorage/GoogleDrive-st32673@kn.ac.th/ไดรฟ์ของฉัน/PIM/Botnoi/LongDo/ask/ask_ver04/Test_file"):
        embeddings_model = OpenAIEmbeddings()
        documents = []
        
        for filename in os.listdir(directory):
            text_splitter = RecursiveCharacterTextSplitter(chunk_size=1000, chunk_overlap=200)
            if filename.endswith(".csv"):
                loader = CSVLoader(os.path.join(directory, filename))
                documents.extend(text_splitter.split_documents(loader.load()))
            if filename.endswith(".txt"):
                loader = TextLoader(os.path.join(directory, filename))
                documents.extend(text_splitter.split_documents(loader.load()))
            if filename.endswith(".pdf"):
                loader = PyPDFLoader(os.path.join(directory, filename))
                documents.extend(text_splitter.split_documents(loader.load()))
            if filename.endswith(".docx"):
                loader = UnstructuredWordDocumentLoader(os.path.join(directory, filename))
                documents.extend(text_splitter.split_documents(loader.load()))

        return embeddings_model, documents"""


