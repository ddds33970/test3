import os
from os import getenv
from os.path import join, dirname
from dotenv import load_dotenv


# กำหนดตำแหน่งของไฟล์ .env
dotenv_path = join(dirname(__file__), '.env')

# โหลดค่าไฟล์ .env
load_dotenv()


# ดึงค่าที่ต้องใช้จากไฟล์ .env และกำหนดให้เป็นตัวแปร
HOST = str(getenv("HOST"))
PORT = str(getenv("PORT"))
MONGODB_URI = str(getenv("MONGODB_URI"))
MONGODB_DB_USER = str(getenv("MONGODB_DB_USER"))
MONGODB_DB_FILE = str(getenv("MONGODB_DB_FILE"))
MONGODB_DB_LOG = str(getenv("MONGODB_DB_LOG"))
MONGODB_DB_ROLE = str(getenv("MONGODB_DB_ROLE"))
USER_ROLE_COLLECTION =str(getenv("USER_ROLE_COLLECTION"))
ADMIN_ROLE_COLLECTION =str(getenv("ADMIN_ROLE_COLLECTION"))
USER_COLLECTION = str(getenv("USER_COLLECTION"))
USER_LOGIN_LOG = str(getenv("USER_LOGIN_LOG"))
USER_SIGNUP_LOG = str(getenv("USER_SIGNUP_LOG"))
PINECONE_API_KEY = str(getenv("PINECONE_API_KEY"))
PINECONE_INDEX_NAME = str(getenv("PINECONE_INDEX_NAME"))
OPENAI_API_KEY = str(getenv("OPENAI_API_KEY"))
COHERE_API_KEY = str(getenv("COHERE_API_KEY"))