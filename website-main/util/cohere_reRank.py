# ver05/util/cohere_reRank.py
# https://api.python.langchain.com/en/latest/retrievers/langchain.retrievers.document_compressors.cohere_rerank.CohereRerank.html
from langchain.retrievers.document_compressors import CohereRerank
from langchain.retrievers import ContextualCompressionRetriever
from util.doc_search_and_retriever import retriever_pinecone


compressor = CohereRerank(model="rerank-multilingual-v2.0",
                            top_n=2)

compression_retriever = ContextualCompressionRetriever(
    base_compressor=compressor, base_retriever=retriever_pinecone
)

# https://txt.cohere.com/introducing-embed-v3/
