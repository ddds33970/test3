# ver05/util/doc_search.py
from langchain_community.vectorstores import Pinecone
from langchain_openai import OpenAIEmbeddings
import settings as ENV
#from ver05.user_input import folder

index_name = ENV.PINECONE_INDEX_NAME
embeddings = OpenAIEmbeddings()

docsearch = Pinecone.from_existing_index(index_name, embeddings)

retriever_pinecone = docsearch.as_retriever(
    search_type="similarity_score_threshold",
    search_kwargs={'score_threshold': 0.9},
    namespace="user"
)

