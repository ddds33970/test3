from flask import Flask, render_template, session, redirect, redirect
from functools import wraps
from util.JSON import MongoEngineJSONEncoder
import settings as ENV

app = Flask(__name__)
app.secret_key = b'\xcc\x94\x80Z\xbfp\x85\xed\x031l\xbc'
app.json_encoder = MongoEngineJSONEncoder


    
# Decorators function (f) ที่modify logged_in ใน login_required
def login_required(f):
  @wraps(f)
  def wrap(*args, **kwargs):
    if 'logged_in' in session:
      return f(*args, **kwargs)
    else:
      return redirect('/')
  
  return wrap


# Routes
from controller import routes
@app.route('/')
def home():
  return render_template('home.html')


@app.route('/dashboard/')
@login_required
def dashboard():
  return render_template('dashboard.html')

if __name__ == '__main__':
  app.run(debug=True, port=ENV.PORT, host=ENV.HOST)
