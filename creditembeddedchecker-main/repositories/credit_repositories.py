from dbconnector.mongo_connector import MongoConnector

logs = MongoConnector().connect().logs
users = MongoConnector().connect().users

class CreditRepositories:

    @staticmethod
    def insert_log(data):
        return logs.find_one(data)
    
    @staticmethod
    def find_user(user_id):
        return users.find_one({'user_id'  : user_id }, {'_id' : False})

    @staticmethod
    def update_credits(user_id , credits):
        return users.update_one({'user_id' : user_id } , { "$set" : { 'credits' : credits }})