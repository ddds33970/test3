from pydantic import BaseModel
from typing import Optional

class DocumentsObject(BaseModel):
    userId: str
    companyId: str
    role: str
    documents: list