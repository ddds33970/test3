# repositories
from repositories.credit_repositories import CreditRepositories

# utils
from utils.credit_utils import check_credit

# model
from model.format_model import Model

from datetime import datetime

credit_repo = CreditRepositories()


class CreditCheckerService:

    @classmethod
    def check_upload_credit(cls, user_id: str, documnets: list):
        count = check_credit(documnets)
        update_status = cls.update_quota(user_id, count)

        if not update_status:
            return "Out of Credit"

        else:
            return ""


    @staticmethod
    def insert_messsage(user_id, message, channel, count, selectdate):
        
        current_time = datetime.utcnow()
        Model.format_message(user_id, message, channel, count, selectdate, current_time)
        return credit_repo.insert_log
    

    @staticmethod
    def update_quota(user_id, count):
        user_data = credit_repo.find_user(user_id)
        credits = user_data['credits']
        if credits:
            if credits > 0:
                credits = int(credits) - int(count)
                if credits >= 0:
                    credit_repo.update_credits(user_id, credits)
                    status = True
                else:
                    status = False
            else:
                status = False
        else:
            return False
        return status

    