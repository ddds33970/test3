import os

import settings as ENV

def check_credit(documents: list): 
    return sum(len(doc["page_content"]) for doc in documents)
