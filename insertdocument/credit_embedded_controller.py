
from fastapi import APIRouter, HTTPException
# from fastapi_jwt_auth import AuthJWT
# from fastapi.responses import JSONResponse
from typing import Optional, List

# Service
# from services.pdf_services import PdfServices

# Repository

from input import *

credit_embedded_app = APIRouter()
# pdf_service = PdfServices()

@credit_embedded_app.post("/credit_embedded_checker")
async def credit_embedded_checker(docs_model: DocumentsObject):
    print("/credit_embedded_checker")
    try:    
        print("documents : ", docs_model.documents)
        
    except Exception as e:
        print(
                "Controller: credit_embedded_control:" + str(e),
                type(e).__name__,          # TypeError
                __file__,                  # /tmp/example.py
                e.__traceback__.tb_lineno  # 2
            )
        raise HTTPException(status_code=401, detail="Could not validate credentials", headers={"WWW-Authenticate": "Bearer"},)
