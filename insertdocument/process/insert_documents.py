from pinecone import Pinecone

# Set your Pinecone API key and create a Pinecone client
api_key = "b6e007c9-2d3d-4fbb-b9ef-9ca14862119e"  # Replace with your actual Pinecone API key
pinecone = Pinecone(api_key=api_key)

# Specify the index you want to insert into
index_name = "test-api"  # Replace with your actual index name

# Define the vectors and their corresponding unique identifiers (document IDs)
data = [
    {"id": "doc1", "vector": [0.1, 0.2, 0.3]},#เพิ่มคลาส role ให้
    {"id": "doc2", "vector": [0.4, 0.5, 0.6]},
    {"id": "doc3", "vector": [0.7, 0.8, 0.9]},
    {"id": "doc4", "vector": [1.0, 1.1, 1.2]},
    {"id": "doc5", "vector": [1.3, 1.4, 1.5]}
]

# Insert the vectors into the specified index
response = pinecone.create_index(index_name, if_exists='fail')  # Only needed if the index doesn't exist yet
response = pinecone.upsert(index_name, data)

print(response)