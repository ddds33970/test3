# from pydantic import BaseModel
# from typing import Optional

# class DocumentsObject(BaseModel):
#     userId: str
#     companyId: str
#     role: str
#     documents: list
    
class InputData:
    def __init__(self, userId, companyId, role, documents):
        self.userId = userId
        self.companyId = companyId
        self.role = role
        self.documents = documents