# # Library
# from fastapi import FastAPI, Request, Form, Body, UploadFile, File, Depends
# # from fastapi.responses import JSONResponse
# # from fastapi_jwt_auth import AuthJWT
# # from fastapi_jwt_auth.exceptions import AuthJWTException
# from fastapi.middleware.cors import CORSMiddleware
# import uvicorn
# # from datetime import timedelta

# # Controller
# import credit_embedded_controller

# # Setting
# import settings as ENV

# app = FastAPI()

# # @AuthJWT.load_config
# # def get_config():
# #     return Settings()

# # @app.exception_handler(AuthJWTException)
# # def authjwt_exception_handler(request: Request, exc: AuthJWTException):
# #     return JSONResponse(
# #         status_code=exc.status_code,
# #         content={"detail": exc.message}
# #     )

# app.add_middleware(
#     CORSMiddleware,
#     allow_origins=["*"],
#     allow_credentials=True,
#     allow_methods=["*"],
#     allow_headers=["*"]
# )

# @app.get("/")
# async def index():
#     return 'creditEmbeddedChecker API Service'

# app.include_router(credit_embedded_controller.credit_embedded_app)

# if __name__ == '__main__':
#     uvicorn.run("main:app", port=ENV.PORT)

from input import InputData
from fastapi import FastAPI
from process.insert_logs import router

app = FastAPI()

app.include_router(router)

def get_user_from_mongo(userId):
    # ดึงข้อมูล user จาก mongo
    # เดี๋ยวมาทำ
    pass
# รับค่า input จาก input.py
input_data = InputData(userId="example_user", companyId="example_company", role="example_role", documents=["doc1", "doc2"])

# ดึงข้อมูล user จาก MongoDB
user_data = get_user_from_mongo(input_data.userId)

