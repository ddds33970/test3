import streamlit as st
# from util.askChat_reRank import ask_reRank
# from util.askChat_no_reRank import ask_no_reRank
# from util.connector.docker_connector import send_chatHistory_to_docker
# from util.memory import memory
# ส่วน chat
# def st_chatVector(username, userId, companyId, role):
def st_chatGraphs(username):
    
    st.title("Graphs Bot")
    
    reset_button_key = "reset_button"
    reset_button = st.button("Reset Chat",key=reset_button_key)
    if reset_button:
        # memory.clear()
        st.session_state.messages = []

    # Initialize chat history
    if "messages" not in st.session_state:
        st.session_state.messages = []

    # Display chat messages from history on app rerun
    for message in st.session_state.messages:
        with st.chat_message(message["role"]):
            st.markdown(message["content"])

    # React to user input
    if question := st.chat_input("What is up?"):
        # Display user message in chat message container
        st.chat_message("user").markdown(question)
        # Add user message to chat history
        st.session_state.messages.append({"role": "user", "content": question})
        
        response = f"Echo: คร้าบบบบบบ"
        
        # Display assistant response in chat message container
        with st.chat_message("assistant"):
            st.markdown(response)
        # Add assistant response to chat history
        st.session_state.messages.append({"role": "assistant", "content": response})
    
    
