from langchain_core.runnables import RunnableLambda, RunnablePassthrough
from operator import itemgetter
from pinecone import Pinecone
from langchain_openai import ChatOpenAI
from langchain_core.output_parsers import StrOutputParser
from langchain_core.messages import get_buffer_string
from util.prompt_rephrase import CONDENSE_QUESTION_PROMPT
from util.prompt_retrieved import RETRIEVED_CONDENSE_QUESTION_PROMPT
from util._combine_documents import _combine_and_remove_redundancy_documents
from util.prompt_answer import ANSWER_PROMPT
from util.cohere_reRank import CompressionRetriever
from util.memory import memory
from langchain_google_genai import ChatGoogleGenerativeAI
import os
from util.prompt_question import FINAL_QUESTION_PROMPT

GOOGLE_API_KEY = os.getenv("GEMINI_API_KEY")

llm_gimini = ChatGoogleGenerativeAI(model="gemini-pro", convert_system_message_to_human=True,) # google_api_key="AI--A") # bun
# ไม่มี call back for count token เหมือน OpenAI

# ฟังก์ชันที่จะใช้ในส่วนของ "Chat"
llm_for_calling = ChatOpenAI(temperature=0, model="gpt-3.5-turbo-1106")

def AskChatModels_main_chain_reRank(username):
    compression_retriever = CompressionRetriever(username)
    # First we add a step to load memory
    # This adds a "memory" key to the input object
    loaded_memory = RunnablePassthrough.assign(
        chat_history=RunnableLambda(memory.load_memory_variables) | itemgetter("history"),
        language= itemgetter("language") # ดึงจาก invoke
    )
    # Now we calculate the standalone question
    standalone_question = {
        "standalone_question": {
            "question": itemgetter("question"),
            "chat_history": lambda x: get_buffer_string(x["chat_history"]), # ใช้ lambda สำหรับจะนำแต่ละค่าไปเข้า fn อื่นๆต่อ
            "language" : itemgetter("language")
        }
        | CONDENSE_QUESTION_PROMPT
        | llm_gimini #llm_for_calling#
        | StrOutputParser(),
        "question_original" : itemgetter("question"), # ดึง key "question" จาก chain ก่อนหน้า (chain assign)
        "language" : itemgetter("language"),
        "chat_history" : itemgetter("chat_history"),
    }

    standalone_question_search = {
        "standalone_question_search": {
            "question": itemgetter("question_original"), # ดึง key "question_original" จาก dictionary "standalone_question"
            "chat_history": lambda x: get_buffer_string(x["chat_history"]),
        }
        | RETRIEVED_CONDENSE_QUESTION_PROMPT
        | llm_for_calling# llm_gimini #
        | StrOutputParser(),
        # chain ต่อไปไม่ได้ใช้  question_original เลยลบออก
        "language" : itemgetter("language"),
        "standalone_question" : itemgetter("standalone_question"),
    }

    standalone_final = {
        "standalone_final": {
            "question_reqphrase": itemgetter("standalone_question"), # ดึง key "question_original" จาก dictionary "standalone_question"
            "retrieved_keyword": itemgetter("standalone_question_search"),
            "language" : itemgetter("language"),
        }
        | FINAL_QUESTION_PROMPT
        | llm_for_calling # llm_gimini #
        | StrOutputParser(),
        # chain ต่อไปไม่ได้ใช้  question_original เลยลบออก
        "language" : itemgetter("language"),
        "standalone_question" : itemgetter("standalone_question"),
        "standalone_question_search" : itemgetter("standalone_question_search"),
    }

    # Now we retrieve the documents
    retrieved_documents = {
        "question_to_search_doc": itemgetter("standalone_question_search"),
        "docs1": itemgetter("standalone_question") | compression_retriever, # เอา
        "docs2" : itemgetter("standalone_question_search") | compression_retriever, # คือเอาคำรีสำหรับ search ไป retreival
        "docs3" : itemgetter("standalone_final")  | compression_retriever,
        "language" : itemgetter("language"),
        "standalone_final" : itemgetter("standalone_final") ,
    }

    final_inputs = {
        "context1": lambda x: _combine_and_remove_redundancy_documents(
            *[doc for doc in x["docs1"]],  # Pass all elements from "docs1"
            *[doc for doc in x["docs2"]],  # Pass all elements from "docs2"
            *[doc for doc in x["docs3"]]   # Pass all elements from "docs3"
        ),
        "question":  itemgetter("standalone_final"), # ใช้ qusetion ใหม่ + history เก่า + keyword เป็น final prompt
        "language" : itemgetter("language"),
    }
    # d = loaded_memory | standalone_question | standalone_question_search | standalone_final | retrieved_documents | final_inputs
    # # And finally, we do the part that returns the answers
    answer = {
        "answer": final_inputs | ANSWER_PROMPT | llm_gimini, #llm_for_calling
        "docs1": itemgetter("docs1"),
        "docs2": itemgetter("docs2"),
        "docs3": itemgetter("docs3"),
    }
    # And now we put it all together!
    final_chain = loaded_memory | standalone_question | standalone_question_search | standalone_final | retrieved_documents | answer
    return final_chain

def ask_reRank(question, username, language="thai"):
    
    final_chain = AskChatModels_main_chain_reRank(username)
    
    question_dict = {
        "question": question,
        "language": language,
    }

    result = final_chain.invoke(question_dict)
    
    return result