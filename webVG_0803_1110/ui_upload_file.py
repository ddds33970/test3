import streamlit as st
import tempfile
from pathlib import Path
from util.pinecone.upsert import file_loader
# from util.pinecone.delete_from_stCheckbox import pinecone_delete_from_checkbox
# from util.delete_in_local import delete_file_duplicate
# from util.mongodb.mongo_latest_upload import mongo_query_all_latest_upload, mongo_delete_selected_latest_upload, mongo_find_one_latest_upload


def st_upload_file_Vector(name_new):
    # แสดงส่วน "Upload File"
    st.header("Upload File")
    # filenames = mongo_query_all_latest_upload(name_new)
    # if filenames:
    #     with st.form("Files you've uploaded"):
    #         # filenames = mongo_query_all_latest_upload(name_new)
    #         selected_files = [st.checkbox(filename) for filename in filenames]
            
    #         delete_file_button = st.form_submit_button("Delete This File")
    #         if delete_file_button:
    #             for filename, selected in zip(filenames, selected_files):
    #                 if selected:
    #                     # print("/nfilename : ", filename)
    #                     # print("selected : ", selected)
    #                     # ตรงนี้เดี๋ยวเรียกใช้ฟังก์ชัน ลบไฟล์ที่เลือกใน Pinecone และ MongoDB collection latest_upload
    #                     # delete file from Pinecone and MongoDB
    #                     pinecone_delete_from_checkbox(name_new, filename)
    #                     mongo_delete_selected_latest_upload(name_new, filename)
    #                 else:
    #                     pass
    #         else:
    #             pass
    # else:
    #     pass
            
    # แสดงส่วนอัปโหลดไฟล์
    with st.form("Upload File", clear_on_submit=True):
        uploaded_files = st.file_uploader("Please select file", type=['.pdf', '.docx', '.txt', '.csv'], accept_multiple_files=True)
        if uploaded_files is not None:
            with tempfile.TemporaryDirectory() as tmpdirname:
                temp_dir = Path(tmpdirname)
                uploaded_filename_list = []
                for uploaded_file in uploaded_files:
                    st.write("Filename:", uploaded_file.name)
                    with open(temp_dir / uploaded_file.name, "wb") as f:
                        f.write(uploaded_file.getbuffer())
                    uploaded_filename_list.append(uploaded_file.name)

                button_upload = st.form_submit_button("Upload")
                if button_upload:
                    for uploaded_filename in uploaded_filename_list:
                        file_loader(name_new, uploaded_filename, temp_dir)
                        # filename_from_mongo = mongo_find_one_latest_upload(name_new, uploaded_filename)
                        # if uploaded_filename == filename_from_mongo:
                            # st.warning(f"{uploaded_filename} has already been uploaded. \n\n If you want to upload it, please delete the old file first.", icon="⚠️")
                            # delete_file_duplicate(uploaded_filename, temp_dir)
                        # else:
                        #     file_loader(name_new, uploaded_filename, temp_dir)

            uploaded_files = None



def st_upload_file_Graphs(name_new):
    # แสดงส่วน "Upload File"
    st.header("Upload File")
    # filenames = mongo_query_all_latest_upload(name_new)
    # if filenames:
    #     with st.form("Files you've uploaded"):
    #         # filenames = mongo_query_all_latest_upload(name_new)
    #         selected_files = [st.checkbox(filename) for filename in filenames]
            
    #         delete_file_button = st.form_submit_button("Delete This File")
    #         if delete_file_button:
    #             for filename, selected in zip(filenames, selected_files):
    #                 if selected:
    #                     # print("/nfilename : ", filename)
    #                     # print("selected : ", selected)
    #                     # ตรงนี้เดี๋ยวเรียกใช้ฟังก์ชัน ลบไฟล์ที่เลือกใน Pinecone และ MongoDB collection latest_upload
    #                     # delete file from Pinecone and MongoDB
    #                     pinecone_delete_from_checkbox(name_new, filename)
    #                     mongo_delete_selected_latest_upload(name_new, filename)
    #                 else:
    #                     pass
    #         else:
    #             pass
    # else:
    #     pass
            
    # แสดงส่วนอัปโหลดไฟล์
    with st.form("Upload File", clear_on_submit=True):
        uploaded_files = st.file_uploader("Please select file", type=['.pdf', '.docx', '.txt', '.csv'], accept_multiple_files=True)
        # if uploaded_files is not None:
        #     with tempfile.TemporaryDirectory() as tmpdirname:
        #         temp_dir = Path(tmpdirname)
        #         uploaded_filename_list = []
        #         for uploaded_file in uploaded_files:
        #             st.write("Filename:", uploaded_file.name)
        #             with open(temp_dir / uploaded_file.name, "wb") as f:
        #                 f.write(uploaded_file.getbuffer())
        #             uploaded_filename_list.append(uploaded_file.name)

        button_upload = st.form_submit_button("Upload")
        #         if button_upload:
        #             for uploaded_filename in uploaded_filename_list:
        #                 file_loader(name_new, uploaded_filename, temp_dir)
        #                 # filename_from_mongo = mongo_find_one_latest_upload(name_new, uploaded_filename)
        #                 # if uploaded_filename == filename_from_mongo:
        #                     # st.warning(f"{uploaded_filename} has already been uploaded. \n\n If you want to upload it, please delete the old file first.", icon="⚠️")
        #                     # delete_file_duplicate(uploaded_filename, temp_dir)
        #                 # else:
        #                 #     file_loader(name_new, uploaded_filename, temp_dir)

        #     uploaded_files = None