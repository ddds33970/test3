import streamlit as st
from util.askChat_reRank import ask_reRank
from util.askChat_no_reRank import ask_no_reRank
# from util.connector.docker_connector import send_chatHistory_to_docker
from util.memory import memory
# ส่วน chat
# def st_chatVector(username, userId, companyId, role):
def st_chatVector(username):
    
    st.title("Vector Bot")
    
    on = st.toggle('"re-rank" feature')

    # clear_memory_button = st.button("Clear memory")
    # if clear_memory_button:
    #     memory.clear()
    # else:
    #     pass
    
    reset_button_key = "reset_button"
    reset_button = st.button("Reset Chat",key=reset_button_key)
    if reset_button:
        memory.clear()
        st.session_state.messages = []

    # Initialize chat history
    if "messages" not in st.session_state:
        st.session_state.messages = []

    # Display chat messages from history on app rerun
    for message in st.session_state.messages:
        with st.chat_message(message["role"]):
            st.markdown(message["content"])

    # React to user input
    if question := st.chat_input("What is up?"):
        # Display user message in chat message container
        st.chat_message("user").markdown(question)
        # Add user message to chat history
        st.session_state.messages.append({"role": "user", "content": question})
        
        if on:
            result = ask_reRank(question, username)
        else:
            result = ask_no_reRank(question, username)

        response = f"Echo: {result['answer'].content}"
        
        memory.save_context({"question": question}, {"answer": result['answer'].content}) # save memory

        latestMemory = str(memory.load_memory_variables({})['history'][:])

        # ส่งข้อมูลไปยัง docker
        # send_chatHistory_to_docker(userId, companyId, role, question, result['answer'].content, latestMemory)
        
        # print(memory.load_memory_variables({}))
        # Display assistant response in chat message container
        with st.chat_message("assistant"):
            st.markdown(response)
        # Add assistant response to chat history
        st.session_state.messages.append({"role": "assistant", "content": response})
    
    
