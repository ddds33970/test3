import streamlit as st
from ui_chatVector import st_chatVector
from ui_delete_file import st_delete_namespace_Graphs, st_delete_namespace_Vector
from ui_upload_file import st_upload_file_Graphs, st_upload_file_Vector
from ui_chatGraphs import st_chatGraphs

option_menu = st.sidebar.selectbox('Selected Page:', ['vector', 'graphs'])

username = "PP"
userId = "123"
companyId ="BN"
role = "admin"

if option_menu == 'vector':
    st_upload_file_Vector(username)
    st_delete_namespace_Vector(username)
    st_chatVector(username)
elif option_menu == 'graphs':
    st_upload_file_Graphs(username)
    st_delete_namespace_Graphs(username)
    st_chatGraphs(username)